
	<div class="notice">
		<div class="wrap">
			<h3 class="notice-title"><a href="http://castour.ru/novosti">Объявления</a></h3>
			<div class="notice-wrap">
			<?php

			   $args = array('post_type' => 'post', 'posts_per_page' => '3');
			   $category_posts = new WP_Query($args);

			   if($category_posts->have_posts()) : 
				  while($category_posts->have_posts()) : 
					 $category_posts->the_post();
			?>
				<div class="notice-block">
					<div class="notice__title"><?php the_title();?><br /><?php the_time('d/m/Y') ?> </div>
					<i><?php the_excerpt();?></i>
				</div>
				
			<?php endwhile; endif;// End the loop. Whew. ?>
			</div>
		</div>
	</div>
	<?php include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>