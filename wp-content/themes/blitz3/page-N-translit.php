<?php
/**
 * Template Name: Транслит
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>


<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="wrap">
		<div class="top-text">
			<?php while ( have_posts() ) : the_post(); ?>

	<h2 class="top-text__title"><?php the_title();?></h2>
	

	
	<?php the_excerpt();?>
	<div id="ig" class="translate">
        <div class="content">
          
            <div class="enterFields">
                <div class="input-group">
                    <input type="text" class="form-control inSurname" placeholder="Фамилия">
                </div>
                <div class="input-group">
                    <input type="text" class="form-control inName" placeholder="Имя">
                </div>
                <div class="input-group">
                    <input type="text" class="form-control inSecName" placeholder=" ">
                </div>
                <div>
                    <button class="enterBtn btn-default">Транслитерировать</button>
                </div>
            </div>
            <div class="pass">
                <img src="<?php bloginfo('template_url'); ?>/img-N/pass_img.png">
                <div class="result">
                    <div class="surname">ФАМИЛИЯ /<br>surname</div>
                    <div class="tr-name">ИМЯ /<br>name</div> 
                    <div class="together">р&lt;rusFamiliia&lt;&lt;Imia&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;<br>XXXXXXXXXXRUSXXXXXXXXXXXXXXXXXXXXXXXX&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;14</div>
                </div>
            </div>

            <div class="rulBtn">новые правила транслитерации</div>
            <div class="rules">
                <div class="wrapper">
                    <div class="closeblock"></div>
                </div>
                <img src="<?php bloginfo('template_url'); ?>/img-N/rules.png">
                <div class="title">новые правила транслитерации</div>
                <div class="close"></div>
            </div>
           
        </div>
	</div>
    <p id="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	
	<?php the_content();?>
	
<?php endwhile; ?>
		</div>
	</div>

<script src="<?php bloginfo('template_url'); ?>/js/translit.js"></script>
<?php// include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php //include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>