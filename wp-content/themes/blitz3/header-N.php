<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
get_header();
return;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<title></title>
		<link rel="stylesheet" href="/wp-content/themes/blitz3/styles-N.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	
		
		<?php wp_head(); ?>
		
	</head>
	<body class="p_id_<?php echo get_the_ID() ?> post_type_<?php echo get_post_type();?> <?php if (is_single()) {echo "single_page";}?>">
	<div class="centercolumn2">
		<div class="centercolumn">
		<!--div id="topmess">В связи со сбоем в МГТС звонить по телефонам: 8 (495) 227-13-15 или 8 (929) 919-49-46</div-->
			<div id="top">
				<?php
				$logotype = get_option('templatesquare_logo_type');
				$logoimage = get_option('templatesquare_logo_image'); 
				$sitename = get_option('templatesquare_site_name');
				$tagline = get_option('templatesquare_tagline');
				if($logoimage == ""){ $logoimage = get_bloginfo('stylesheet_directory') . "/images/logo.gif"; }
				?>
				<?php if($logotype == 'textlogo'){ ?>
					<?php if($sitename =="" && $tagline ==""){?>
					<div id="logo">
						<div class="pad-logo">
							<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
							<span class="desc"><?php bloginfo('description'); ?></span>
						</div>
					</div>
					<?php }else{ ?>
					<div id="logo">
						<div class="pad-logo">
							<h1><a href="<?php echo get_option('home'); ?>/"><?php echo $sitename; ?></a></h1>
							<span class="desc"><?php echo $tagline; ?></span>
						</div>
					</div>
					<?php } ?>
				<?php }else{ ?>
					<div id="logo">
						<a style="float:left; "href="<?php echo get_option('home'); ?>/" title="<?php _e('Home','blitz'); ?>"><img src="<?php echo $logoimage; ?>" alt="" height="60" /></a>
						<span style="width:330px;color: #333333; position:relative;top: 30px; left:20px;font-style: italic;font-size: 12px;font-family: 'Open Sans', sans-serif; "><?php bloginfo('description'); ?></span>
					</div>
				<?php } ?>
				
				<div id="topcontact"><small><sup>+7 (495)</sup></small><big><b>608-66-62</b></big><b><small></small></b><small> <sup>+7 (495)</sup></small><b><big>608-66-64</big></b></div>
				<div id="topcontact_soc"><a href="https://www.facebook.com/castour.ru" target="_blank"><img class="" src="http://castour.ru/wp-content/uploads/2014/10/04-02.jpg" alt="04_02" width="29" height="27"></a> <a href="http://vk.com/castour_ru" target="_blank"><img class="" src="http://castour.ru/wp-content/uploads/2014/10/04-041.jpg" alt="04_04" width="auto" height="27"></a></div>
				<div class="clr"></div>
			</div><!-- end top -->
			
			<div id="topmenu">
				
				<?php if ( function_exists( 'wp_nav_menu' ) ) {?>
					<?php 
						wp_nav_menu( array(
							'container'       => 'ul', 
							'menu_class'      => 'menu', 
							'menu_id'         => 'nav',
							'depth'           => 0,
							'theme_location' => 'mainmenu',
							'walker' => new description_walker(),
						
							)
						);
					  ?>
				<?php } ?>
			</div><!-- end topmenu -->
		</div>
	</div>
	<?php if ( is_home() || is_front_page() || is_page( 'kontakty' )) {} else {?>
  
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
	<?php } 	?>	
			