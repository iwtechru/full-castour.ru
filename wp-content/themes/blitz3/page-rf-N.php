<?php
/**
 * Template Name: РФ паспорт N
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>
<div class="wrap">
		<div class="top-text">
			<?php include_once (TEMPLATEPATH . '/page-rf-N-content-block.php'); ?>
		</div>
	</div>

<?php include_once (TEMPLATEPATH . '/page-rf-N-part-1.php'); ?>
			<div class="form-wrap">
				<div class="form-col form-col-rf pass">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_1.png" alt="">
						Причина
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="new" name="type_p" id="new" checked="checked" class="styled"><label for="new">Получение / замена паспорта</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="lost" name="type_p" id="lost" class="styled"><label for="lost">Утрата / хищение паспорта</label>
						</div>						
					</div>
				</div>
				
				<div class="form-col form-col-rf country">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_3.png" alt="">
						Место <br />регистрации
					</div>
					<div class="labels">
					
						<div class="labels-wrap"><input type="radio" value="moscow" name="region" id="moscow" checked="checked" class="styled"><label for="moscow">Москва</label></div>
						<div class="labels-wrap"><input type="radio" value="obl" name="region" id="obl" class="styled"><label for="obl">Московская область</label></div>
						<div class="labels-wrap"><input type="radio" value="other" name="region" id="other" class="styled"><label for="other">Другие регионы РФ</label></div>
					</div>
				</div>
			</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-2.php'); ?>

		<div class="reload" id="output">		
		<?php require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		$loop = new WP_Query('page_id=1973');
		while ( $loop->have_posts() ) : $loop->the_post();	 ?>
				
		<?php 
			
		$select_price = "newmoscow";
			?>
		<?php include_once (TEMPLATEPATH . '/filter-variants-rf-N.php'); ?>	
	<?php endwhile; ?>
		</div>	
<?php include_once (TEMPLATEPATH . '/page-rf-N-part-3.php'); ?>
	<div class="big-notice big-notice_nob">
		<?php include_once (TEMPLATEPATH . '/page-zagran-N-cross-url-block.php'); ?>
	</div>

	<div class="tabs-wrap">
		<ul class="tabs rfpassp">
			<li class="tabs-current">В случае достижения 14-летнего возраста</li>
			<li>В случае достижения возраста 20 или 45 лет</li>
			<li>В случае изменения внешности, ошибки в данных, непригодности</li>
			<li>В случае изменения ФИО, сведений о дате и месте рождения </li>
		</ul>
		<div class="tabs-box visible">
			<ul class="tabs-item">
				<li class="tabs__list">	Свидетельство о рождении</li>
				<li class="tabs__list">4 фото размером 35×45 мм</li>
				<li class="tabs__list">Документы, свидетельствующие о принадлежности к гражданству РФ</li>
			</ul>
		</div>
		<div class="tabs-box">
			<ul class="tabs-item">
				<li class="tabs__list">Паспорт гражданина РФ, подлежащий замене</li>
				<li class="tabs__list">4 фото размером 35×45 мм</li>
				<li class="tabs__list">Документы, необходимые для проставления обязательных отметок в паспорте</li>
			</ul>
		</div>
		<div class="tabs-box">
			<ul class="tabs-item">
				<li class="tabs__list">Паспорт гражданина РФ, подлежащий замене</li>
				<li class="tabs__list">4 фото размером 35×45 мм</li>
				<li class="tabs__list">Документы, необходимые для проставления обязательных отметок в паспорте</li>
				<li class="tabs__list">Свидетельство о рождении</li>
			</ul>
		</div>	
		<div class="tabs-box">
			<ul class="tabs-item">
				<li class="tabs__list">Паспорт гражданина РФ, подлежащий замене</li>
				<li class="tabs__list">4 фото размером 35×45 мм</li>
				<li class="tabs__list">Документы, необходимые для проставления обязательных отметок в паспорте</li>
				<li class="tabs__list">Свидетельство о рождении Свидетельство о расторжении брака</li>
				<li class="tabs__list">Свидетельство о регистрации брака</li>
				<li class="tabs__list">Повторное свидетельство о рождении</li>
				<li class="tabs__list">Свидетельство о перемене имени</li>
			</ul>
		</div>
		<script>
	$('ul.tabs').delegate('li:not(.tabs-current)', 'click', function() {
		$(this).addClass('tabs-current').siblings().removeClass('tabs-current').parents('div.tabs-wrap').find('div.tabs-box').eq($(this).index()).fadeIn(150).siblings('div.tabs-box').hide();
	});
		</script>
	</div>	
	
<?php// include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php //include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>