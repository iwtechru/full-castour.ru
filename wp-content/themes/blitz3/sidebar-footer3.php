<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

?>
<div class="widget-area s3">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3') ) : ?><?php endif; ?>
</div>