<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
?>  

		</div><!-- end centercolumn -->
	</div><!-- end top container -->
	
	<div id="bottom_container">
		<div class="centercolumn">
			<div id="bottombox_container">
				<div class="bottombox nomargin">
					  <noindex><?php get_sidebar('footer1'); ?></noindex>
				</div><!-- end bottombox -->
				<div class="bottombox">
					  <?php get_sidebar('footer2'); ?>
				</div><!-- end bottombox -->
				<div class="bottombox">
					  <?php get_sidebar('footer3'); ?>
				</div><!-- end bottombox -->
			</div><!-- end bottombox container -->
			<div class="clr"></div>
		</div><!-- end centercolumn -->
	</div><!-- end bottom container -->
	<div id="footer_container">
		<div class="centercolumn">
			<div id="foot">
		<?php $foot= stripslashes(get_option('templatesquare_footer'))?>
					<?php if($foot==""){?>
					<?php _e('Copyright', 'templatesquare'); ?> &copy;
					<?php global $wpdb;
					$post_datetimes = $wpdb->get_results("SELECT YEAR(min(post_date_gmt)) AS firstyear, YEAR(max(post_date_gmt)) AS lastyear FROM $wpdb->posts WHERE post_date_gmt > 1970");
					if ($post_datetimes) {
						$firstpost_year = $post_datetimes[0]->firstyear;
						$lastpost_year = $post_datetimes[0]->lastyear;
		
						$copyright = $firstpost_year;
						if($firstpost_year != $lastpost_year) {
							$copyright .= '-'. $lastpost_year;
						}
						$copyright .= ' ';
		
						echo $copyright;
						bloginfo('name');
					}
				?>. <?php _e('All rights reserved.', 'templatesquare'); ?>
				
					<?php }else{?>
					<?php echo $foot; ?>
					<?php } ?>
			</div><!-- end foot -->
		</div><!-- end centercolumn -->
	</div><!-- end bottom container -->
<?php wp_footer(); ?>
<?php $google = stripslashes(get_option('templatesquare_google'));?>
<?php if($google==""){?>
<?php }else{?>
<?php echo $google; ?>
<?php } ?>




<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=14561236&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/14561236/3_0_6C7886FF_4C5866FF_1_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:14561236,lang:'ru'});return false}catch(e){}"/></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter14561236 = new Ya.Metrika({id:14561236,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/14561236" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter14577979 = new Ya.Metrika({id:14577979, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/14577979" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55222089-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 983488895;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983488895/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55222089-1', 'auto');
  ga('send', 'pageview');

</script>



</body>
</html>