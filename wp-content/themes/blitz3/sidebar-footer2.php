<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

?>
<div class="widget-area s2">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : ?><?php endif; ?>
</div>