
	<div class="map">
		<div class="map-wrap">
			<div class="map-info">
				<div class="map-info__inner">
					<div class="map-info__title">Адрес офиса CASTOUR:</div>
					<div class="map-info__b">
						ул. Трубная, д. 32, стр. 4, офис 3
						<span class="map-info__bs">(м. Сухаревская, Цветной бульвар или Трубная)</span>
					</div>
					<div class="map-info__title">График работы</div>
					<div class="map-info__b">
						Понедельник-пятница:
						<span class="map-info__bs">с 10:00 до 20:00</span>
					</div>
					<div class="map-info__b">
						Суббота:
						<span class="map-info__bs">с 10:00 до 17:00</span>
					</div>
					<div class="map-info__b">
						Воскресенье:
						<span class="map-info__bs">выходной</span>
					</div>
					<div class="map-info__title">Телефон</div>
					<div class="map-info__b">
						+7 (495) 608-66-62<br />
						+7 (495) 608-66-64
					</div>
					<div class="map-info__title">Электронная почта</div>
					<div class="map-info__b">
						info@castour.ru
					</div>
					<i class="map-info__dot map-info__dot_1"></i>
					<i class="map-info__dot map-info__dot_2"></i>
					<i class="map-info__dot map-info__dot_3"></i>
					<i class="map-info__dot map-info__dot_4"></i>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=9D6sB-gR-dbkjqLteEXJYvU2HbOLCRQe"></script>
	</div>
