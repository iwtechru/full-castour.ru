
	$( document ).ready(function() {
		setup();
		
		$( ".datepicker_expected" ).datepicker({ 
			dateFormat: "dd/mm/y",
			minDate: new Date('2013-04-22'), 
			changeYear: true, 
			yearRange : '2013:+2', 
		});
	});
	
<!--
  window.a = "";
  var Dateberegningsdato;
  var DateControl;
  var antallrader = 8;	// количество строк по парам заезд-выезд в таблице (из паспорта)
  var antallraderPass = 8;//antallrader * 2; // количество строк в произвольной таблице из паспорта
  var rows;
  var fraDato = new Array();
  var tilDato = new Array();
  var TxtPassport = new Array();
  var dager = new Array();
  var hist = new Array();
  var histdager = new Array();
  var modeCronology = true; // какая таблица активна с въезд-выезд или плюсик-минусик
  var messageStartDate='Новый регламент вступил в силу 18 октября 2013 года, следовательно, калькулятор не может быть использован для поездок, совершенных ранее 22 апреля 2013 (18.10.2013 минус 180 дней). Для поездок, совершенных до 22 апреля 2013 года, действует старое правило «3 месяца в течение полугода».';//6&apos;;00&quot
  var tabid = 1; // подсчёт количество полей для того чтобы формировать уникальный норме для каждого генерируемого поля
  var validateDate = function () { // функция валидации даты въезда-выезда
      var str = this.value;
      var i = 0;
      var isValidChar = true;
      while (i < str.length) {
          switch (str.charAt(i)) {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
          case '/':
              break;
          default:
              isValidChar = false;
              alert("Только цифры и знак «/»");
              str = "";
          }
          i++;
      }
      if (isValidChar) {
          if (str.charAt(2) != "/" && str.length > 2) str = str.substr(0, 2) + "/" + str.substr(2, 6);
          if (str.charAt(5) != "/" && str.length > 5) str = str.substr(0, 5) + "/" + str.substr(5, 6);
		  
      }

      if (str.length ==8) {
          if (str != DateToString(StringToDate(str))) {
              alert("Дата " + str + " некорректна.");
              str="";
          }
		  if(daysBetween( StringToDate('22/04/13'), StringToDate(str))<0) {
		  alert(messageStartDate);
		  str="";}
          
      }
        else {alert("Введите дату " + str + " в формате ддммгг или дд/мм/гг");}

      this.value = str;
  }
  
  var validateDatePass = function () { // функция валидации даты для паспорта
      var str = this.value;
      var i = 1;
      var firstchar;
      if (str.charAt(0) != "+" && str.charAt(0) != "-" && str.length > 0) {
          alert("Сначала введите «+» или «-»");
          str = "";
      } else {
          while (i < str.length) {
              switch (str.charAt(i)) {
              case '0':
              case '1':
              case '2':
              case '3':
              case '4':
              case '5':
              case '6':
              case '7':
              case '8':
              case '9':
              case '/':
                  break;
              default:
                  isValidChar = false;
                  alert("Только цифры и знак «/»");
                  str = "";
              }
              i++;
          }
          if (isValidChar) {
          if (str.charAt(3) != "/" && str.length > 3) str = str.substr(0, 3) + "/" + str.substr(3, 7);
          if (str.charAt(6) != "/" && str.length > 6) str = str.substr(0, 6) + "/" + str.substr(6, 8);
			}
         
      }
      this.value = str;
  }
  
  var changeArrow = function () { // функция изменения стрелочки
  	if (this.getAttribute('way') == 'true') {
	  	this.innerHTML = '&rarr;выезд';
	  	this.setAttribute('way', 'false');
  	} else {
	  	this.innerHTML = '&larr;въезд';
	  	this.setAttribute('way', 'true'); 	
  	}
  }




  
  function append(str) {
      append(s, "BLACK");
  }

  function append(str, fstr) {
      var x = document.getElementById("result");
      var newDiv = document.createElement("div");
      switch (fstr) {
      case 'green':
          newDiv.innerHTML = "<div style=color:green>" + str + "</div>\n";
          break;
      case 'red':
          newDiv.innerHTML = "<div style=color:red>" + str + "</div>\n";
          break;
      case 'black':
          newDiv.innerHTML = "<div style=color:green>" + str + "</div>\n";
          break;
      default:
          newDiv.innerHTML = "<div style=color:black>" + str + "</div>\n";
      }
      x.appendChild(newDiv);
  }

  function settingDate(t) {
      var x = document.getElementById('TxtField');
      var str = x.value;
              var i = 0;
              var isValidChar = true;
              while (i < str.length) {
                  switch (str.charAt(i)) {
                  case '0':
                  case '1':
                  case '2':
                  case '3':
                  case '4':
                  case '5':
                  case '6':
                  case '7':
                  case '8':
                  case '9':
                  case '/':
                      break;
                  default:
                      isValidChar = false;
                      alert("Только цифры и знак «/»");
                      str = "";
                  }
                  i++;
              }
			  
              if (isValidChar) {
                  if (str.charAt(2) != "/" && str.length > 2) str = str.substr(0, 2) + "/" + str.substr(2, 6);
                  if (str.charAt(5) != "/" && str.length > 5) str = str.substr(0, 5) + "/" + str.substr(5, 6);
              }
              if (str.length ==8) {
                  if (str != DateToString(StringToDate(str))) {
                      alert("Дата " + str + " некорректна.");
                      str="";
                  }
		
              }
                 else {alert("Введите дату " + str + " в формате ддммгг или дд/мм/гг");}

      x.value = str;
  }

  function cleanresult() {
      var x = document.getElementById('result');
      x.innerHTML = '';
      return false;
  }

  function cleanForm() {
      document.getElementById('TxtField').value = DateToString(new Date());
      cleanresult();
      fraDato = new Array();
      tilDato = new Array();

      for (var i = 0; i < antallrader; i++) {
          var x = document.getElementById("fra" + i);
          x.value = '';
          var x = document.getElementById("til" + i);
          x.value = '';
          var x = document.getElementById("dager" + i);
          x.value = '';
      }

      for (var i = 0; i < antallraderPass; i++) {
          var x = document.getElementById("TxtPassport" + i);
          x.value = '';
      }

  }

  function cleanMain() {
      cleanresult();
      fraDato = new Array();
      tilDato = new Array();

      for (var i = 0; i < antallrader; i++) {
          var x = document.getElementById("fra" + i);
          x.value = '';
          var x = document.getElementById("til" + i);
          x.value = '';
          var x = document.getElementById("dager" + i);
          x.value = '';
      }
  }

  function isValidDate(sjekkdato) {
      if (sjekkdato == "" || isNaN(sjekkdato)) {
          return false;
      } else {
          return true;
      }

  }

  function isValidChar(c) {
      isValidChar = true;
      switch (c) {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      case '/':
          break;
      default:
          isValidChar = false;
          alert("Только цифры и знак «/»");
      }

      return isValidChar;
  }

  function DateToString(dato) {
      var txt;
      var dd = dato.getDate();
      var mm = dato.getMonth() + 1;
      var yyyy = dato.getFullYear();
      if (parseInt(dd) < 10) {
          dd = '0' + dd
      }
      if (parseInt(mm) < 10) {
          mm = '0' + mm
      }
      txt = "" + dd + "/" + mm + "/" + yyyy % 100;

      return txt;
  }

  function StringToDate(str) {
      var dd = str.substring(0, 2);
      var mm = str.substring(3, 5);
      var yyyy = "20" + str.substring(6, 10);
      var tt = "06";

      var d = new Date(yyyy, mm - 1, dd, tt);
      return d;
  }

  function DagerSiden(dato, dager) {
      var timestamp;
      timestamp = dato.getTime() - (1000 * 60 * 60 * 24 * dager);
      var newDate = new Date(timestamp);
      return newDate;
  }

  function daysBetween(fra, til) {
      var dager;
      if (fra == null || til == null) return -1;
      if (til.getTime() + (1000 * 60 * 60 * 2) < fra.getTime()) {
          dager = -1;
      } else {
          dager = (til.getTime() + (1000 * 60 * 60 * 2) - fra.getTime()) / (1000 * 60 * 60 * 24);
          dager++;
      }
      return Math.floor(dager);
  }


  function setup() {
      var fra = new Array();
      var til = new Array();


      for (var i = 0; i < antallrader; i++) {
      	addToFromDate(i);
      }

      for (var i = 0; i < antallraderPass; i++) {
      	addTxtPassport(i);
      }
      document.getElementById("passportstays_wrap").style.display = "none";
      document.getElementById('TxtField').value = DateToString(new Date()); // устанавливаем начальную дату
  }
  
  /*
   * функция добавления нового поля в произвольное добавление дат с плюсиками и минусиками
   * подсчёт количества полей в переменной antallraderPass
   */
  function addTxtPassport(count) {
	  // если не передали счётчик, то увеличиваем количество полей берём из глобальный переменной
  	if (!count && count !== 0) {
  		count = antallraderPass;
  		antallraderPass++
  	}

	TxtPassport[count] = document.createElement("input");
	TxtPassport[count].setAttribute("type", "text");
	TxtPassport[count].setAttribute("name", "TxtPassport" + count);
	TxtPassport[count].setAttribute("tabindex", tabid);
	TxtPassport[count].setAttribute("id", "TxtPassport" + count);
	TxtPassport[count].setAttribute("class", "random_date");
	TxtPassport[count].style.width = 70;
	TxtPassport[count].style.margin = 2;
	//TxtPassport[count].onchange = validateDatePass;
	TxtPassport[count].onchange = validateDate;

    var foo = document.getElementById("passportstays");
    foo.appendChild(TxtPassport[count]);
    
    
	  var spn = document.createElement("span");
	  spn.setAttribute("id", "TxtPassportWay" + count);
	  spn.setAttribute("way", "true");
	  spn.setAttribute("class", "passport_way");
	spn.onclick = changeArrow;
	  spn.innerHTML = '&larr;въезд';
	  var foo = document.getElementById("passportstays");
	  foo.appendChild(spn);

    var br = document.createElement("br");
    var foo = document.getElementById("passportstays");
    foo.appendChild(br);
    tabid++;
    
	  $( "#TxtPassport" + count ).datepicker({ 
	  	dateFormat: "dd/mm/y", 
	  	minDate: new Date('2013-04-22'), 
	  	changeYear: true, 
	  	yearRange : '2013:+2',
	  });
  }
  
  /*
   * функция добавления новых полей в таблицу дат въезд-выезд
   * подсчёт количества полей в переменной antallrader
   */
  function addToFromDate(count) {
  	// если не передали счётчик, то увеличиваем количество полей берём из глобальный переменной
  	if (!count && count !== 0) {
  		count = antallrader;
  		antallrader++
  	}
  	
  		var datepickerClass="datepicker"+count;
  		
	  var fra = document.createElement("input");
	  fra.setAttribute("type", "text");
	  fra.setAttribute("name", "text" + count);
	  fra.setAttribute("tabindex", tabid);
	  fra.setAttribute("id", "fra" + count);
	  fra.setAttribute("class", datepickerClass+"_fra from_date");
	
	
	  fra.style.width = 60;
	  fra.style.margin = 2;
	  fra.onchange = validateDate;
	  var foo = document.getElementById("stays");
	  foo.appendChild(fra);
	  
	  // добавляем стрелку по другому (напрямую не через тег) перестают работать календари
	  var spn = document.createElement("span");
	  spn.innerHTML += '—';
	  var foo = document.getElementById("stays");
	  foo.appendChild(spn);
	  
	  tabid++;
	
	  var til = document.createElement("input");
	  til.setAttribute("type", "text");
	  til.setAttribute("name", "text" + count);
	  til.setAttribute("tabindex", tabid);
	  til.setAttribute("id", "til" + count);
	  til.setAttribute("class", datepickerClass+"_til to_date");
	  til.style.width = 60;
	  til.style.margin = 2;
	  til.onchange = validateDate;
	  var foo = document.getElementById("stays");
	  foo.appendChild(til);
	  tabid++;
	
	  dager[count] = document.createElement("input");
	  dager[count].setAttribute("type", "text");
	  dager[count].setAttribute("name", "dager" + count);
	  dager[count].setAttribute("class", "all_days");
	  dager[count].disabled=true;
	
	  dager[count].setAttribute("id", "dager" + count);
	  dager[count].style.width = 30;
	  dager[count].style.margin = 2;
	  var foo = document.getElementById("stays");
	  foo.appendChild(dager[count]);
	
	  var br = document.createElement("br");
	  var foo = document.getElementById("stays");
	  foo.appendChild(br);	  

	  // запускаем работу календаря в созданных полях
	  $( "."+datepickerClass+"_fra" ).datepicker({ 
	  	dateFormat: "dd/mm/y", 
	  	minDate: new Date('2013-04-22'), 
	  	changeYear: true, 
	  	yearRange : '2013:+2',
	  	onClose: function( selectedDate ) {
	        $( "."+datepickerClass+"_til" ).datepicker( "option", "minDate", selectedDate );
            $("."+datepickerClass+"_til").datepicker("option", 'defaultDate', selectedDate);
	    } 
	  });
	  $( "."+datepickerClass+"_til" ).datepicker({ 
	  	dateFormat: "dd/mm/y", 
	  	minDate: new Date('2013-04-22'), 
	  	changeYear: true, 
	  	yearRange : '2013:+2',
	  	onClose: function( selectedDate ) {
        	$( "."+datepickerClass+"_fra" ).datepicker( "option", "maxDate", selectedDate );
            $("."+datepickerClass+"_fra").datepicker("option", 'defaultDate', selectedDate);
		}
	  });
  }

  function fncCalculate() {
      cleanresult();

      var x = document.getElementById("result");
      window.a = "";

	  // если активна таблица с плюсик-минусик
      if (!modeCronology) {
          cleanMain();
          InsertDates();
          sjekkdatoer();
          sortPassport();
          ordneDatoer();
          fncPassport();
          // переключить бегунок обратно на По поездкам
          fncPassport(true);
          document.getElementById('not_random').checked = true;
          document.getElementById('random').checked = false;
      }
      sjekkdatoer();
	  if(sortCron())  cleanresult();sjekkdatoer();
      if (feedback()) {
          if (leggInndager()) {
              if (document.getElementById('altVisa').checked) {
                  BeregnVisa();
              } else {
                  BeregnControl();
              }
          }
      }

  }

  /*
   * меняем таблицу с рандомного заполнения на по поездкам и наоборот
   * можно передать аргумент который откроет по true и false то или другое
   */
  function fncPassport(argument) {
  	if (argument === false && modeCronology != true) {
	  	document.getElementById("stays_wrap").style.display = "none";
		document.getElementById("passportstays_wrap").style.display = "block";
		modeCronology = false;
  	} else if (argument === true && modeCronology != false) {
	  	document.getElementById("stays_wrap").style.display = "block";
		document.getElementById("passportstays_wrap").style.display = "none";
		modeCronology = true;
  	} else {
	  	if (modeCronology) {
	    	document.getElementById("stays_wrap").style.display = "none";
			document.getElementById("passportstays_wrap").style.display = "block";
			modeCronology = false;
		} else {
	    	document.getElementById("stays_wrap").style.display = "block";
			document.getElementById("passportstays_wrap").style.display = "none";
			modeCronology = true;
		}
	}
  }

  function InsertDates() {
      var i = 0;
      var str;
      var inn = 0;
      var ut = 0;

      while (i < antallraderPass) { // TODO тут надо проверить чётное ли количестов въездов-выездов получается или нет
          var x = document.getElementById("TxtPassport" + i);
          var way = document.getElementById("TxtPassportWay" + i);

		  if (x.value) {
	          if (way.getAttribute('way') == 'true') {	// это въезд
	              fraDato[inn] = StringToDate(x.value);
	              z = document.getElementById("fra" + inn);
	              z.value = x.value;
	              inn++;
	          } else {
	              tilDato[ut] = StringToDate(x.value);
	              z = document.getElementById("til" + ut);
	              z.value = x.value;
	              ut++;
	          }
          }
          i++;
      }
  }

  function sjekkdatoer() {
      var x; //=document.getElementById("fra"+2);
      var i = 0;
      validRows = 0;

      Dateberegningsdato = StringToDate(document.getElementById('TxtField').value);
      DateControl = StringToDate(document.getElementById('TxtField').value);

      while (i < antallrader) {

          x = document.getElementById("fra" + i);
          y = document.getElementById("til" + i);
          z = document.getElementById("dager" + i);
          if (x.value != "") {

              fraDato[i] = StringToDate(document.getElementById('fra' + i).value);
          }
          if (y.value != "") {
              tilDato[i] = StringToDate(y.value);
          }

          if (x.value != "" && y.value != "") {
              z.value = daysBetween(fraDato[i], tilDato[i]);
              validRows++;
          }
          i++;
      }

  }

  function sortPassport() {
      var isSorted = false;
      var x;
      var y;
      var i = 1;
      var tmpDate;

      while (!isSorted) {
          isSorted = true;
          i = 1;
          while (i < antallrader && !isNaN(fraDato[i])) {
              if (daysBetween(fraDato[i - 1], fraDato[i]) < 0) {
                  tmpDate = fraDato[i - 1];
                  fraDato[i - 1] = fraDato[i];
                  fraDato[i] = tmpDate;
                  isSorted = false;
              }
              i++;
          }
      }
      isSorted = false;

      while (!isSorted) {
          isSorted = true;
          i = 1;
          while (i < antallrader & !isNaN(tilDato[i])) {
              if (daysBetween(tilDato[i - 1], tilDato[i]) < 0) {
                  tmpDate = tilDato[i - 1];
                  tilDato[i - 1] = tilDato[i];
                  tilDato[i] = tmpDate;
                  isSorted = false;
              }
              i++;
          }
      }
      i = 0;

  }

  function ordneDatoer() {
      i = 0;

      while (i < Math.max(fraDato.length, tilDato.length)) {
          if (i + 1 < fraDato.length && i < tilDato.length && tilDato[i] != "" && fraDato[i + 1] != "") {
              if (daysBetween(tilDato[i], fraDato[i + 1]) < 0) {
                  tilDato.splice(i, 0, "");
              }
          }
          if (i < fraDato.length && i < tilDato.length && fraDato[i] != "" && tilDato[i] != "") {
              if (daysBetween(fraDato[i], tilDato[i]) < 0) {
                  fraDato.splice(i, 0, "");
              }
          }
          i++;
      }

      i = 0;
      while (i < fraDato.length) {
          var x = document.getElementById("fra" + i);
          if (fraDato[i] == "") {
              x.value = "";
          } else {
              x.value = DateToString(fraDato[i]);
          }
          i++;
      }
      i = 0;
      while (i < tilDato.length) {
          var x = document.getElementById("til" + i);
          if (tilDato[i] == "") {
              x.value = "";
          } else {
              x.value = DateToString(tilDato[i]);
          }
          i++;
      }
  }
  function sortCron() {
      var isSorted = false;
      var x;
      var y;
      var i = 1;
      var tmpDate;
	  skalSorteres=false;
			
   
           while (i < fraDato.length && !skalSorteres) {
            if (daysBetween(fraDato[i - 1], fraDato[i]) > 1||(daysBetween(fraDato[i - 1], fraDato[i]) ==1 && daysBetween(tilDato[i - 1], tilDato[i])>0)||isNaN(fraDato[i])) {i++;}
              else{
                  skalSorteres=confirm('Даты ваших поездок указаны не в хронологическом порядке. Нажмите ОК, чтобы расположить даты по хронологии или Cancel, чтобы отредактировать их.');
              }
         }
  
	  
	  while (!isSorted&&skalSorteres) {
          isSorted = true;
          i = 1;
          while (i < fraDato.length) {
            if (daysBetween(fraDato[i - 1], fraDato[i]) > 1||(daysBetween(fraDato[i - 1], fraDato[i]) ==1 && daysBetween(tilDato[i - 1], tilDato[i])>0)||isNaN(fraDato[i])) {}
              else{
                  tmpDate = fraDato[i - 1];
                  fraDato[i - 1] = fraDato[i];
                  fraDato[i] = tmpDate;
                  tmpDate = tilDato[i - 1];
                  tilDato[i - 1] = tilDato[i];
                  tilDato[i] = tmpDate;
                  isSorted = false;
              }
              i++;
          }
      }
      i = 0;
      while (i < fraDato.length&&skalSorteres) {
          var x = document.getElementById("fra" + i);
          var y = document.getElementById("til" + i);
		  var z = document.getElementById("dager" + i);
          if (isNaN(fraDato[i]) == true) {
              x.value = "";
		  } else {
              x.value = DateToString(fraDato[i]);
          }
          if (isNaN(tilDato[i]) == true) {
              y.value = "";
          } else {
              y.value = DateToString(tilDato[i]);
          }
		  z.value="";
          i++;
      }
	  return skalSorteres;
	  }


  function leggInndager() {
      var i = 0;
      var totalt = 0;
      var diffDager;
      var lovligOppholdRad = true;
      var lovligOpphold = true;
      var row = 0;
      rows = daysBetween(fraDato[0], DateControl) + 180;
      x = document.getElementById("fra" + 0);
      var d = StringToDate(x.value);

      while (i < rows) {
          if (daysBetween(fraDato[row], d) > 0 && daysBetween(d, tilDato[row]) > 0) {
              hist[i] = 1;
          } else if (daysBetween(DateControl, d) >= 1) {
              hist[i] = 1;
          } else {
              hist[i] = 0;
          }
 
          if (daysBetween(d, tilDato[row]) <= 1) {
              row++;
          }
          d = DagerSiden(d, -1);
          i++;
      }
	  
      i = 0;
      while (i < rows) {
          if (i == 0) {
              histdager[i] = 1;
          } else if (i < 180) {
              histdager[i] = histdager[i - 1] + hist[i];
          } else {
              histdager[i] = histdager[i - 1] + hist[i] - hist[i - 180];
          }
          i++;
      }

      i = 0;
      row = 0;

      while (row < validRows) {
          i = daysBetween(fraDato[0], fraDato[row]) - 1;
          while (i <= (daysBetween(fraDato[0], tilDato[row]) - 1) && lovligOppholdRad) {
              if (histdager[i] > 90) {
                  lovligOppholdRad = false;
                  append("Дни пребывания в 180-дневный период " + DateToString(DagerSiden(tilDato[row], 179)) + " – " + DateToString(tilDato[row]) + ": " + histdager[(daysBetween(fraDato[0], tilDato[row]) - 1)] + " дней", "red");
                  append("Превышен допустимый срок пребывания в период " + DateToString(DagerSiden(fraDato[0], -i)) + " – " + DateToString(tilDato[row]) + " (" + daysBetween(DagerSiden(fraDato[0], -i), tilDato[row]) + " дней)", "red");

                  lovligOpphold = false;
              }
              i++;
          }
          row++;
          lovligOppholdRad = true;
      }
      return lovligOpphold;
      return true;
  }

  function feedback() {
      var returverdi = true;
      var i = 0;
      var x;
      var y;
      var z;

      while (isValidDate(fraDato[i]) || isValidDate(tilDato[i])) {
      	// проверяем не указана ли дата до установленного законом периода
			if(daysBetween(StringToDate('22/04/13'), fraDato[i]) < 0 && isValidDate(fraDato[i])) {
				  alert(messageStartDate); returverdi = (returverdi && false);}
			if(daysBetween(StringToDate('22/04/13'), tilDato[i]) < 0 && isValidDate(tilDato[i])) {
				  alert(messageStartDate);alert('ff');returverdi = (returverdi && false);}
	  

				 // проверяем заполненность и валидность дат
          x = document.getElementById("fra" + i);
          y = document.getElementById("til" + i);
          if (!isValidDate(fraDato[i])) {
              append("Для даты выезда " + y.value + " не указана дата въезда");
              returverdi = (returverdi && false);
          }

          if (!isValidDate(tilDato[i])) {
              append("Для даты въезда " + x.value + " не указана дата выезда");
              returverdi = (returverdi && false);
          }

          if (isValidDate(fraDato[i]) && isValidDate(tilDato[i])) {
              if (daysBetween(fraDato[i], tilDato[i]) < 0) {
                  append("Дата въезда " + x.value + " позже даты выезда " + y.value);
                  returverdi = (returverdi && false);
              }
          }

          if (isValidDate(tilDato[i])) {
              if (daysBetween(tilDato[i], DateControl) < 0) {
                  append("Дата выезда " + y.value + " позже Предполагаемой даты въезда/Контрольной даты  " + document.getElementById('TxtField').value);
                  returverdi = (returverdi && false);
              }
          }

          if (i > 0) {
              if (isValidDate(fraDato[i + 1]) && isValidDate(tilDato[i])) {
                  if (daysBetween(tilDato[i], fraDato[i + 1]) < 0) {
                      z = document.getElementById("fra" + (i + 1));
                      append("Дата въезда " + z.value + " раньше даты выезда в предыдущую поездку " + y.value);
                      returverdi = (returverdi && false);
                  }
              }
          }
          i++;
      } //end while                                                                                                                                                 
      if (returverdi == false) append("Расчет невозможен" + "\n");
      return returverdi;
  }

  function BeregnVisa() {
      var visumkangisdager = 0;
      var Dateberegningsdato = StringToDate(document.getElementById('TxtField').value);
      var tekst;
      var x = document.getElementById("til" + (validRows - 1));
      var y = document.getElementById("TxtField");

      append("Начало 90-дневного периода: " + DateToString(DagerSiden(Dateberegningsdato, 89)));
      append("Начало 180-дневного периода: " + DateToString(DagerSiden(Dateberegningsdato, 179)));

      visumkangisdager = Beregn2(Dateberegningsdato);
      if (visumkangisdager == 0) {
          append("Допустимо пребывание на срок до " + visumkangisdager + " дней");
      } else if (visumkangisdager > 0) {
          append("Допустимо пребывание на срок до " + visumkangisdager + " дней", "green");

      }
      if (visumkangisdager == 0) {
          if (x.value == y.value) {
              Dateberegningsdato = DagerSiden(Dateberegningsdato, -1);
          }
          Dateberegningsdato = Beregn3(Dateberegningsdato);
          visumkangisdager = Beregn2(Dateberegningsdato);
          append("С " + DateToString(Dateberegningsdato) + " пребывание допустимо на " + visumkangisdager + " дней", "BLACK");

      }

  }

  function BeregnControl() {
      var Dateberegningsdato;
      var visumkangisdager = 0;
      var Dateberegningsdato = StringToDate(document.getElementById('TxtField').value);
      append("Нет нарушений в указанный период.");

      visumkangisdager = Beregn2(Dateberegningsdato);

      if (visumkangisdager == 0) {
          if (DateToString(tilDato[validRows - 1]) == DateToString(DateControl)) {
              append("С " + DateToString(Dateberegningsdato) + " пребывание допустимо на " + visumkangisdager + " дней", "BLACK");
          } else {
              f
              append("Пребывание недопустимо.", "RED");
          }
      } else if (visumkangisdager > 0) {
          if (validRows != 0) {
              if (DateToString(tilDato[validRows - 1]) == DateToString(DateControl)) visumkangisdager++;
          }
          append("Пребывание возможно до " + DateToString(DagerSiden(DateControl, -visumkangisdager + 1)), "GREEN");
          append("Не забудьте проверить количество дней пребывания, указанные в визе.", "green");
      }

      if (visumkangisdager == 0) {
          if (DateToString(tilDato[validRows - 1]) == DateToString(DateControl)) {
              Dateberegningsdato = DagerSiden(Dateberegningsdato, -1);
          }

          Dateberegningsdato = Beregn3(Dateberegningsdato);
          visumkangisdager = Beregn2(Dateberegningsdato);
          append("С " + DateToString(Dateberegningsdato) + " пребывание допустимо на " + visumkangisdager + " дней", "BLACK");
      }
  }

  function Beregn2(dato) {
      var d;
      var i;
      var totalt = 0;
      var visumKanGisDager = 0;
      if (validRows == 0) {
          visumKanGisDager = 90;
      } else {
          i = daysBetween(fraDato[0], dato) - 1;
          while (histdager[i] <= 90 && i < rows) {
              visumKanGisDager++;
              i++;
			  //alert("i:" + i + "histdager"+ histdager[i]);
          }
          i = daysBetween(fraDato[0], dato) - 1;

          if (DateToString(tilDato[validRows - 1]) == DateToString(dato)) {
              visumKanGisDager=visumKanGisDager-1;
          }
      }
      return visumKanGisDager;
  }

  function Beregn3(d) {
      i = daysBetween(fraDato[0], d) - 1;
      hist[i] = 1;
      if (i < 180) {
          histdager[i] = histdager[i - 1] + hist[i];
      } else {
          histdager[i] = histdager[i - 1] + hist[i] - hist[i - 180];
      }

      while (histdager[i] > 90) {
          hist[i] = 0;
          if (i < 180) {
              histdager[i] = histdager[i - 1] + hist[i];
          } //variant ikke mulig
          else {
              histdager[i] = histdager[i - 1] + hist[i] - hist[i - 180];
          }
          i++;
          d = DagerSiden(d, -1);
          hist[i] = 1;
          if (i < 180) {
              histdager[i] = histdager[i - 1] + hist[i];
          } else {
              histdager[i] = histdager[i - 1] + hist[i] - hist[i - 180];
          }
      }
      while (i < rows - 1) {
          i++;
          hist[i] = 1;
          if (i < 180) {
              histdager[i] = histdager[i - 1] + hist[i];
          } else {
              histdager[i] = histdager[i - 1] + hist[i] - hist[i - 180];
          }
      }
      return d;
  }




//-->