<?php
/**
 * Template Name: Portfolio
 *
 * A custom page template for Portfolio page.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
	<div id="maincontent">
		<div id="content">
		<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>

			
			
			<?php if ( ! have_posts() ) : ?>
			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'templatesquare' ); ?></h1>
				<div class="entry-content">
				<p>
				<?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'templatesquare' ); ?>
				</p>
				<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
			<?php endif; ?>
			<?php wp_reset_query();?>
			
			<?php include_once (TEMPLATEPATH . '/title.php'); ?>
			<?php $values = get_post_custom_values("category-include"); $cat=$values[0];  ?>
			<?php global $more;	$more = 0;?>
			<?php $catinclude = 'portfoliocat='.$cat;?>
			<?php query_posts('&' . $catinclude .' &paged='.$paged.'&showposts=3'); ?>
			
			<div id="gallery">
			<?php 
			if ( have_posts() ) while ( have_posts() ) : the_post(); 
			?>
			
			
			<?php
				$custom = get_post_custom($post->ID);
				$cf_thumb = $custom["thumb"][0];
				$cf_lightbox = $custom["lightbox"][0];
				
				if($cf_thumb!=""){
					$cf_thumb = "<img src='" . $cf_thumb . "' alt=''  width='280' height='124' />";
				}
			?>
			
			<div class="pf_container">
			<?php if($cf_lightbox!=""){ ?>
				<div class="pf_image">
				<a class="image" href="<?php echo $cf_lightbox;?>" rel="prettyPhoto[mixed]" title="<?php the_title();?>"><?php if($cf_thumb!=""){ echo $cf_thumb; }else{ the_post_thumbnail( 'portfolio-post-thumbnail' );} ?></a>
				</div>
			<?php }else{ ?>
				<div class="pf_image">
				<a href="<?php the_permalink() ?>" title="<?php _e('Permanent Link to', 'templatesquare');?> <?php the_title_attribute(); ?>" ><?php if($cf_thumb!=""){ echo $cf_thumb; }else{ the_post_thumbnail( 'portfolio-post-thumbnail' );} ?></a>
				</div>
				<?php } ?>
				<div class="pf_desc">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'templatesquare');?> <?php the_title_attribute(); ?>" ><?php the_title(); ?></a></h2>
				<p><?php $excerpt = get_the_excerpt(); echo ts_string_limit_words($excerpt,25).'... ';?></p>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'templatesquare');?> <?php the_title_attribute(); ?>" class="morelink2"><?php _e('&raquo; Details', 'templatesquare'); ?></a>
				</div>
			</div><!-- end pf_container -->
			<div class="clr"></div><!-- clear float -->
			<?php endwhile; ?>
			</div>
			<?php /* Display navigation to next/previous pages when applicable */ ?>
			<?php if (  $wp_query->max_num_pages > 1 ) : ?>
						 <?php if(function_exists('wp_pagenavi')) { ?>
							 <?php wp_pagenavi(); ?>
						 <?php }else{ ?>
							<div id="nav-below" class="navigation">
								<div class="nav-previous"><?php next_posts_link( __( '<span class="prev"><span class="meta-nav">&laquo;</span> Older posts</span>', 'templatesquare' ) ); ?></div>
								<div class="nav-next"><?php previous_posts_link( __( '<span class="next">Newer posts <span class="meta-nav">&raquo;</span></span>', 'templatesquare' ) ); ?></div>
							</div><!-- #nav-below -->
						<?php }?>
			<?php endif; ?>
			<?php wp_reset_query();?>
		</div><!-- end content -->
		
		
		<div id="sideright">
			<?php get_sidebar('page'); ?>
		</div><!-- end sideright -->
		<div class="clr"></div><!-- clear float -->
	</div><!-- end maincontent -->
<?php get_footer(); ?>
