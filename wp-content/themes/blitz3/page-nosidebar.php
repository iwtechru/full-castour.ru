<?php
/**
 * Template Name: No sidebar
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
<div id="maincontent">
	<div id="content" class="contentfull">
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>

		<?php if(!is_front_page()){?><?php include_once (TEMPLATEPATH . '/title.php'); ?><?php }?>
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php if(!is_front_page()){post_class();} ?>>
			<div class="entry-content">
				<?php the_content(); ?>
				
			</div><!-- .entry-content -->
		</div><!-- #post-## -->
		
	
		
		<?php endwhile; ?>
	</div><!-- end content -->
	<div class="clr"></div><!-- clear float -->
</div><!-- end maincontent -->
<?php get_footer(); ?>
