<?php
/**
 * Template Name: Загран паспорт N старого образца.
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>
<div class="wrap">
		<div class="top-text">
			<?php include_once (TEMPLATEPATH . '/page-zagran-N-content-block.php'); ?>
		</div>
	</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-1.php'); ?>
			<div class="form-wrap">
				<div class="form-col pass">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_1.png" alt="">
						Тип загранпаспорта
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="bio" name="type_p" id="bio"  class="styled"><label for="bio">Биометрический</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="old" name="type_p" id="old" checked="checked" class="styled"><label for="old">Старого образца</label>
						</div>						
					</div>
				</div>
				<div class="form-col high">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_2.png" alt="">
						Возраст получателя
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="14" name="age" id="14" checked="checked" class="styled"><label for="14">до 14 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="14_17" name="age" id="14_17" class="styled"><label for="14_17">от 14 до 17 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="18" name="age" id="18" class="styled"><label for="18">18 лет и старше</label>
						</div>	
					</div>
				</div>
				<div class="form-col country">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_3.png" alt="">
						Место регистрации
					</div>
					<div class="labels">
					
						<div class="labels-wrap"><input type="radio" value="moscow" name="region" id="moscow" checked="checked" class="styled"><label for="moscow">Москва</label></div>
						<div class="labels-wrap"><input type="radio" value="moscow_obl" name="region" id="moscow_obl" class="styled"><label for="moscow_obl">Московская область</label></div>
						<div class="labels-wrap"><input type="radio" value="others" name="region" id="others" class="styled"><label for="others">Другие регионы РФ</label></div>
					</div>
				</div>
			</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-2.php'); ?>
		<div class="reload" id="output">		
		<?php require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		$loop = new WP_Query('page_id=1694');
		while ( $loop->have_posts() ) : $loop->the_post();	 ?>
				
		<?php 
			
		$select_price = "old14moscow";
		$select_doc = "old14";		?>
		<?php include_once (TEMPLATEPATH . '/filter-variants-N.php'); ?>	
	<?php endwhile; ?>
		</div>	
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-3.php'); ?>
<div class="pass_block">
	<ul class="pass_anc_link_doc old">
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Анкета-на-загранпаспорт-старого-образца-для-лиц-старше-18-лет-скачать.doc">Анкета на загранпаспорт старого образца для лиц <strong>старше 18 лет</strong> (скачать)</a></li>
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Анкета-на-загранпаспорт-старого-образца-для-лиц-до-18-лет-скачать.doc">Анкета на загранпаспорт старого образца для лиц <strong>до 18 лет</strong> (скачать)</a></li>
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Образец-заполнения-анкеты-для-лиц-старше-18-лет-скачать.doc">Образец заполнения анкеты для лиц <strong>старше 18 лет </strong>(скачать)</a></li>
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Образец-заполнения-анкеты-для-лиц-до-18-лет-скачать.doc">Образец заполнения анкеты для лиц <strong>до 18 лет</strong> (скачать)</a></li>
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Приложение-к-анкете-на-загранпаспорт-старого-образца-смена-Ф.И.О..doc">Приложение к анкете на загранпаспорт старого образца смена Ф.И.О.(скачать)</a></li>
		<li><a href="http://castour.ru/wp-content/uploads/2014/07/Приложение-к-анкете-на-загранпаспорт-старого-образца-трудовая-деятельность.doc">Приложение к анкете на загранпаспорт старого образца трудовая деятельность (скачать)</a></li>
	</ul>
	<ul class="pass_other_link">
		<li class="pencil"><a href="http://castour.ru/zagranpasporta/zapolnenie-ankety" target="_blank">Помощь в правильном заполнении анкеты (от 500 рублей)</a></li>
		<li class="question">Проверить готовность: <a href="http://www.fmsmoscow.ru/docs/ipassport/passport_test.html" target="_blank">Москва</a>, <a href="http://www.fmsmo.ru/internet-priemnaya/pgzp" target="_blank">Московская область</a></li>
	</ul>
</div>
	<div class="big-notice big-notice_nob">
		<?php include_once (TEMPLATEPATH . '/page-zagran-N-cross-url-block.php'); ?>
	</div>
	<div class="tabs-wrap">
		<ul class="tabs">
			<li class="tabs-current">Обратите внимание</li>
			<li>Возможные причины отказа</li>
			<li>Преимущества «Кастур</li>
		</ul>
		<div class="tabs-box visible">
			<ul class="tabs-item">
				<li class="tabs__list">Для молодых людей в возрасте от 18 до 27 лет необходимо определить воинскую обязанность. Если гражданин прошел военную службу, он может представить, либо военный билет, либо нужна справка из военкомата о том, что он не призван.</li>
				<li class="tabs__list">Если срок действия заграничного паспорта еще не истек, но подается заявление на новый загранпаспорт, то действующий паспорт аннулируется только при получении нового (пока оформляется новый паспорт старым можно пользоваться).</li>
				<li class="tabs__list">Для оформления заграничного паспорта нового поколения необходимы любые 2 фотографии для документов (на анкеты).  На сам паспорт фотографирует инспектор в специальной кабинке при подаче документов.</li>
				<li class="tabs__list">Вписывание ребенка в заграничный паспорт теперь носит лишь информационный характер. Каждый гражданин РФ выезжающий за рубеж должен иметь отдельный заграничный паспорт от рождения.</li>
				<li class="tabs__list">На сегодняшний день ребенку до 18 лет можно оформить загранпаспорт как старого так и нового образца.</li>
			</ul>
		</div>
		<div class="tabs-box">
			<ul class="tabs-item">
				<li class="tabs__list">имел допуск к сведениям «совершенно секретным» или «сведениям особой важности».</li>
				<li class="tabs__list">подлежит призыву на срочную военную или альтернативную гражданскую службу.</li>
				<li class="tabs__list">задержан по подозрению в совершении преступления или привлечен в качестве обвиняемого.</li>
				<li class="tabs__list">осужден и отбывает наказание, в том числе условное осуждение.</li>
				<li class="tabs__list">уклоняется от исполнения обязательств, наложенных на него судом.</li>
				<li class="tabs__list">предоставил ложные сведения при оформлении документов (ограничение на выдачу действует до предоставления полной и достоверной информации, но не более одного месяца).</li>
			</ul>
		</div>
		<div class="tabs-box">
			Загранпаспорт старого образца выдается сроком на 5 лет, но оформить его часто получается быстрее. Сделать это без очередей, проволочек, с минимумом затрат времени и сил можно через компанию Кастур. Мы обладаем существенными преимуществами, а именно:<br /><br />

<b>Работаем быстро и профессионально.</b> С первого звонка вы сможете получить исчерпывающую консультацию по оформлению загранпаспорта старого образца в Москве, Московской области или других регионах России. Мы знаем все тонкости процедуры подачи документов и получения паспорта.
<br /><br />
<b>Держим невысокие цены.</b> Благодаря оптимизации бизнес-процессов и обширной клиентуре, мы можем удерживать стоимость услуг чуть ниже среднерыночной.
<br /><br />
<b>Работаем полностью в правовом поле.</b> Все оформляемые нами документы на 100% легальны; мы все делаем официальными методами.
<br /><br />


Обратившись к нам, можете быть уверены, что загранпаспорт старого образца будет оформлен быстро, недорого и официально. Кастур - надежный помощник в сложном деле!

		</div>
		<script>
	$('ul.tabs').delegate('li:not(.tabs-current)', 'click', function() {
		$(this).addClass('tabs-current').siblings().removeClass('tabs-current').parents('div.tabs-wrap').find('div.tabs-box').eq($(this).index()).fadeIn(150).siblings('div.tabs-box').hide();
	});
		</script>
	</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>
<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>