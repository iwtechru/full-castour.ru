<div class="blue-form-wrap">
	<div class="blue-form">
			<div class="wrap">
				<div class="blue-form__cols">
					<div class="blue-form__right">
						<div class="blue-form__cols-title">или закажите звонок:</div>
						<div class="blue-form__wrap">
						
							<?php echo do_shortcode('[contact-form-7 id="1847" title="Позвоните сейчас или закажите звонок"]'); ?>
						</div>
					</div>
					<div class="blue-form__left">
						<div class="blue-form__cols-title">Позвоните сейчас</div>
						<ul class="blue-form__left-tel">
							<li class="blue-form__left-list">+7 (495) 608-66-62</li>
							<li class="blue-form__left-list">+7 (495) 608-66-64</li>
							<li class="blue-form__left-list">+7 (495) 227-13-15</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
</div>

