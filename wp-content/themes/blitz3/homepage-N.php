<?php
/**
 * Template Name: Главная N
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>


<div class="yellow-wrap">
		<div class="yellow">
		<div class="passport-top">
			Профессиональная помощь в оформлении
			<div class="passport-top__text">
				Паспорта Российской Федерации, заграничного паспорта, легальной временной<br> 
				регистрации в Москве и Московской области.
			</div>
		</div>
		<div class="wrap">
			<div class="passport">
				<div class="passport-block passport-block_text passport-block_text_1">
					<div class="passport-inner passport-inner_1">
						<div class="passport-inner__title"><a href="/zagranpasporta" class="passport-inner__link">Оформление <br>загранпаспорта РФ</a></div>
						<ul class="passport-item">
							<li class="passport__list"><a href="/zagranpasporta/srochnoe-oformlenie-zagranpasporta" class="passport__list-link">Срочное (от 2-х дней)</a></li>
							<li class="passport__list"><a href="/zagranpasporta/oformlenie-zagranpasporta-dlya-inogorodnix-zhitelej-regionov-rf" class="passport__list-link">Для жителей регионов РФ</a></li>
							<li class="passport__list"><a href="/zagranpasporta/oformlenie-zagranpasporta-dlya-rebenka" class="passport__list-link">Для детей</a></li>
							<li class="passport__list"><a href="/zagranpasporta/oformlenie-zagranpasporta-starogo-obrazca" class="passport__list-link">Старого образца</a></li>
							<li class="passport__list"><a href="/zagranpasporta/oformlenie-zagranpasporta-novogo-obrazca" class="passport__list-link">Биометрический</a></li>
							<li class="passport__list"><a href="/zagranpasporta/oformlenie-zagranpasporta-dlya-zhitelej-kryma" class="passport__list-link">Для крымчан</a></li>
						
							<li class="passport__list"><a href="/zagranpasporta/zamena-zagranpasporta" class="passport__list-link">Замена</a></li>
						</ul>
					</div>
					
				</div>
				<div class="passport-block passport-block_img"><img src="/wp-content/themes/blitz3/img-N/passport.png" alt=""></div>
				<div class="passport-block passport-block_text">
					<div class="passport-inner passport-inner_3">
						<div class="passport-inner__title"><a href="/zagranpasporta/zapolnenie-ankety" class="passport-inner__link">Заполнение анкет <br>на загранпаспорт</a></div>
						<ul class="passport-item">
							<li class="passport__list"><a href="/zagranpasporta/zapolnenie-ankety" class="passport__list-link">Необходимые документы</a></li>
							<li class="passport__list"><a href="/zagranpasporta/zapolnenie-ankety" class="passport__list-link">Стоимость</a></li>
						</ul>
					</div>
					<div class="passport-inner passport-inner_4">
						<div class="passport-inner__title"><a href="/pasport-rf" class="passport-inner__link">Полезное</a></div>
						<ul class="passport-item">
							<li class="passport__list"><a href="/calculator" class="passport__list-link">Визовый калькулятор</a></li>
							<li class="passport__list"><a href="/translit" class="passport__list-link">Транлитерация</a></li>
							
							
						</ul>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>	
	<div class="stats">
		<div class="wrap">
			<div class="stats-wrap">
				<div class="stats-block stats-block_1">
					<img src="/wp-content/themes/blitz3/img-N/stats_1.png" alt="" class="stats__img">
					<div class="stats__info">
						<div class="stats__info-title">5 лет</div>
						Успешной работы
					</div>
				</div>
				<div class="stats-block stats-block_2">
					<img src="/wp-content/themes/blitz3/img-N/stats_2.png" alt="" class="stats__img">
					<div class="stats__info">
						<div class="stats__info-title" id="clients">7752</div>
						Довольных клиентов
					
					</div>
				</div>
				<div class="stats-block stats-block_3">
					<img src="/wp-content/themes/blitz3/img-N/stats_3.png" alt="" class="stats__img">
					<div class="stats__info">
						<div class="stats__info-title">100%</div>
						Законно и легально
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wwdo">
		<div class="wrap">
			<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content();?>
			<?php endwhile; ?>
		</div>
	</div>

	<div class="blue-form-wrap">
		<div class="blue-form">
			<div class="wrap">
				<h3 class="blue-form__title">Появились вопросы?</h3>
				<div class="blue-form__call-now">Позвоните прямо сейчас</div>
				<div class="blue-form__tel">+ 7 (495) 608-66-62</div>
				<div class="blue-form__text">или оставьте заявку и мы с вами свяжемся в ближайшее время</div>
				
					<?php echo do_shortcode('[contact-form-7 id="1846" title="Появились вопросы"]'); ?>
				
			</div>
		</div>
	</div>	
	<?php include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<script>
	var start = Math.round(new Date().getTime()/10000000)-132710;
	document.getElementById("clients").innerHTML = start;
</script>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>