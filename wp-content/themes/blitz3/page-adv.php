<?php
/**
 * Template Name: Объявления
 *
 * A custom page template for news page.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
 ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>


<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="wrap">
		<div class="top-text">
		<?php query_posts('cat=31&showposts=20'); ?>
			<?php while ( have_posts() ) : the_post(); ?>

	<h3 class="top-text__title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'templatesquare' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
	<span class="postdate"><?php the_time('d/m/Y') ?></span>
	<?php the_excerpt();?>
	
	
<?php endwhile; ?>

<?php if (  $wp_query->max_num_pages > 1 ) : ?>
						 <?php if(function_exists('wp_pagenavi')) { ?>
							 <?php wp_pagenavi(); ?>
						 <?php }else{ ?>
							<div id="nav-below" class="navigation">
								<div class="nav-previous"><?php next_posts_link( __( '<span class="prev"><span class="meta-nav">&laquo;</span> Older posts</span>', 'templatesquare' ) ); ?></div>
								<div class="nav-next"><?php previous_posts_link( __( '<span class="next">Newer posts <span class="meta-nav">&raquo;</span></span>', 'templatesquare' ) ); ?></div>
							</div><!-- #nav-below -->
						<?php }?>
			<?php endif; ?>
			<?php wp_reset_query();?>
		</div>
	</div>


<?php include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>