<?php
header('Content-Type: text/html; charset=utf-8');


if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

// Получаем данные из формы
$type_p = $_POST['type_p'];
$age = $_POST['age'];
$region = $_POST['region'];

 /* // определяем имя и путь к файлу с кэшем 
 $cachetime = 25920000; 

 // проверяем кэш на пригодность по времени,  
 // если время еще не вышло, то подключаем имеющийся файл. 
 if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)){ 
 include($cachefile); 
 exit; 
 */
 // определяем имя и путь к файлу с кэшем 
$cachefile = 'cache/'.$type_p.$age.$region.'.php'; 

 //  подключаем имеющийся файл если есть кешированный
 if (file_exists($cachefile)){ 
 include($cachefile); 
 exit; 
 }   

 // если время уже прошло, даем загрузиться новому HTML 

	if (!isset($type_p) && !isset($age) && !isset($region))
{
	/* 
	Здесь можно вывести сообщение о необходимости выбрать параметры фильтра, либо оставить пустым.      
	*/
	}
	else 
	{ 
		require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		$loop = new WP_Query('page_id=1694');
		while ( $loop->have_posts() ) : $loop->the_post();
		$select_price = "$type_p$age$region";
		$select_doc = "$type_p$age";
		
?>
		<script type="text/javascript">
		
		$(function(){
            $(".reload-table").hide();
			$(".reload-table").fadeTo(500, 1);
			$(".hidden-doc").hide();
			$(".hidden-doc").fadeTo(500, 1);

        });
		</script>
		<div class="hidden-table">
			<div class="reload-table">
			<?php echo types_render_field($select_price, array());?>
			</div>
		</div>
		<div class="reload-notice">*Сроки указаны в рабочих днях</div>
		<div class="cols">
			<div class="col-left">
				<div class="hidden-doc">
					<h3 class="sub-title">Необходимые документы:</h3>
					<?php echo types_render_field($select_doc, array());?>
				</div>
				
			</div>
			<div class="col-right">
				<div class="big-notice">
					<?php echo types_render_field("gos_poshlina", array());?>
				</div>
			</div>
		</div>
		<div class="cols">
			<div class="col-left">
				
				<?php echo types_render_field("in_price", array());?>
			</div>	
					

			
			<div class="col-right">
				<div class="big-notice">
					<?php echo types_render_field("vyezd", array());?> 
				</div>
			</div>
		</div>
<?php 
	endwhile;
	}
	// создаем новый кэш страницы 
	 $fp = fopen($cachefile, 'w'); 
	 fwrite($fp, ob_get_contents()); 
	 fclose($fp); 
	 ob_end_flush();
	 }
?> 