<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', 'templatesquare' ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'templatesquare' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<?php
	/* Start the Loop.
	 *
	 * In Twenty Ten we use the same loop in multiple contexts.
	 * It is broken into three main parts: when we're displaying
	 * posts that are in the gallery category, when we're displaying
	 * posts in the asides category, and finally all other posts.
	 *
	 * Additionally, we sometimes check for whether we are on an
	 * archive page, a search page, etc., allowing for small differences
	 * in the loop on each template without actually duplicating
	 * the rest of the loop that is shared.
	 *
	 * Without further ado, the loop:
	 */ ?>
<?php while ( have_posts() ) : the_post(); ?>
<?php /* How to display all other posts. */ ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'templatesquare' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

			<?php if(!is_search()){?><div class="entry-meta">
				 
			</div><!-- .entry-meta --><?php } ?>

			<?php if (is_search() ) : // Only display excerpts for archives and search. ?>
				<div class="entry-summary">
					<?php $excerpt = get_the_excerpt(); echo ts_string_limit_words($excerpt,25).'...';?>
				</div><!-- .entry-summary -->
			<?php else : ?>
			<div class="entry-content">
				<?php
					$custom = get_post_custom($post->ID);
					$cf_thumb = $custom["thumb"][0];
				?>
				<?php if(has_post_thumbnail( $the_ID) || $cf_thumb!=""){ ?>
						<?php 
							if($cf_thumb!=""){
								echo "<img src='" . $cf_thumb . "' alt=''  style='width:110px;height:110px;' class='alignleft border' />";
							}else{
								the_post_thumbnail( 'blog-post-thumbnail', array('class' => 'alignleft border', 'alt' =>'', 'title' =>'') );
							}
						?>
				<?php } ?>
				
				<?php// $excerpt = get_the_excerpt('');?>
				<p><?php the_content();?><br />
			</p>
				
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'templatesquare' ), 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->
	<?php endif; ?>

		</div><!-- #post-## -->

		<?php// comments_template( '', true ); ?>


<?php endwhile; // End the loop. Whew. ?>


<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
			 <?php if(function_exists('wp_pagenavi')) { ?>
				 <?php wp_pagenavi(); ?>
			 <?php }else{ ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="prev"><span class="meta-nav">&laquo;</span>Далее...</span>', 'templatesquare' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( '<span class="next">Newer posts <span class="meta-nav">&raquo;</span></span>', 'templatesquare' ) ); ?></div>
				</div><!-- #nav-below -->
			<?php }?>
<?php endif; ?>
<?php wp_reset_query();?>
