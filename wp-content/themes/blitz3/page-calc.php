<?php
/**
 * Template Name: Калькулятор
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
//get_header(); 
?>
<?php
///include_once (TEMPLATEPATH . '/header-N.php'); 
get_header();
?>
<link href="<?php bloginfo('template_url'); ?>/style-calc.css" type="text/css"  data-template-style="true"  rel="stylesheet" />

<script src="<?php bloginfo('template_url'); ?>/js/calc.js" type="text/javascript"></script>

<div class="wrap">
    <div class="top-text">
        <?php while (have_posts()) : the_post(); ?>

            <h2 class="top-text__title"><?php the_title(); ?></h2>

            <?php the_excerpt(); ?>


        <?php endwhile; ?>


        <form class="calculator_form"> 
            <style type="text/css">
                .fl{float:left;}
                .fr{float:right;}
                .cl{clear:both;}
                .datepicker_expected {

                }
                .pcal {
                    padding-top:0;
                /*    margin-top:*/
                }
                .pinp {
                    margin-top:0;
                    padding-top: 10;

                }

                input[type=checkbox].css-checkbox {
                    position:absolute; z-index:-1000; left:-1000px; overflow: hidden; clip: rect(0 0 0 0); height:1px; width:1px; margin:-1px; padding:0; border:0;
                }

                input[type=checkbox].css-checkbox + label.css-label {
                    padding-left:32px;
                    height:27px; 
                    display:inline-block;
                    line-height:27px;
                    background-repeat:no-repeat;
                    background-position: 0 0;
                    font-size:27px;
                    vertical-align:middle;
                    cursor:pointer;

                }

                input[type=checkbox].css-checkbox:checked + label.css-label {
                    background-position: 0 -27px;
                }
                label.css-label {
                    background-image:url(http://csscheckbox.com/checkboxes/u/csscheckbox_b1f47f7ee609c6e164b59a2a606b9d2f.png);
                    -webkit-touch-callout: none;
                    -webkit-user-select: none;
                    -khtml-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                }

                .gts {



                }
            </style>
            <script language="javascript">

                function switch1() {

                    fncPassport(true);
                    $('.as1').css('background', 'none').css('font-weight', 'bold');
                    $('.as2').css('background', '#e7e7e7').css('font-weight', 'normal');

                }

                function switch2() {

                    fncPassport(false);
                    $('.as1').css('background', '#e7e7e7').css('font-weight', 'normal');
                    $('.as2').css('background', 'none').css('font-weight', 'bold');

                }

                $(document).ready(function () {
                    $('mode_control').hide();
                });
            </script>
            <div class="fl">
                <div style="background:#e7e7e7; padding:5px; padding-top:10px; padding-left:10px; margin-top:0;width:360px">
                    Предполагаемая дата въезда/Контрольная дата:</br></br>
                    <div class="fl pcal" style="margin:0;"><a nohref  onclick="settingDate('TxtField')"><img src="<?php bloginfo('template_url'); ?>/img-calc/cal.png" /></a></div> 
                    <div class="fl" style="margin:0;padding-top:5px;padding-left:5px"><input type="text" name="TxtField" id="TxtField" maxlength="8" class="datepicker_expected ll-skin-melon" onchange="settingDate('TxtField')" style="border: 0; box-shadow:none;border-radius:0;background:white;font-weight:bold;font-style:italic;" /></div>
                    <div class="cl"></div>
                </div>
                <div class="checkboxes" style="margin: 15px 0px">
                    <div class="fl">
                        <input id="checkboxG1" class="css-checkbox" onClick="$('#altVisa').click();
                                $('#checkboxG2').prop('checked', false);" type="checkbox" />
                        <label for="checkboxG1" class="css-label">планирование</label>
                    </div>
                    <div class="fl" style="padding-left: 18px">
                        <input id="checkboxG2" class="css-checkbox" onClick="$('#altControl').click();
                                $('#checkboxG1').prop('checked', false);" type="checkbox" />
                        <label for="checkboxG2" class="css-label">контроль</label>
                    </div>
                    <div class="cl"></div>
                </div>
                <div class="calc_input fl">
                    <div id="passport_rows" style="float: left;background:#ffc785; width:375px">
                        <div>
                            <div class="fl as1" style="width:50%; height:25px;font-weight:bold;text-align:center;padding-top:5px;">
                                <a href="javascript://" onClick="switch1()" style="color:black;text-decoration:none;">по поездкам</a>
                            </div>
                            <div class="fl as2" style="width:50%; height:25px; background:#e7e7e7;text-align:center;padding-top:5px;">
                                <a href="javascript://" onClick="switch2()" style="color:black;text-decoration:none;">произвольно</a>
                            </div>
                            <div class="cl"></div>
                        </div>
                        <div style="padding:10px;">
                            <div id="stays_wrap">
                                &nbsp;&nbsp;&nbsp;&nbsp;Предыдущие поездки<br/>
                                <span class="label_in" style="background:none;border:none;box-shadow:none;">дата въезда</span><span class="label_out" style="background:none;border:none;box-shadow:none;">дата выезда</span> 
                                <br />
                                <span class="stays" id="stays"></span><input type="image" src="<?php bloginfo('template_url'); ?>/img-calc/addbtn.png" class="calc_btn big calculator" onclick="addToFromDate();
                                        return false;" style="width:auto;height:auto;background:none;" />
                            </div>
                            <div id="passportstays_wrap">
                                <span class="label_in_passport" style="border:none">Все даты предыдущих поездок</span>
                                <br />
                                <span style="width: 160px;" id="passportstays"></span><input type="image" src="<?php bloginfo('template_url'); ?>/img-calc/addbtn.png" class="calc_btn big calculator" onclick="addTxtPassport();
                                        return false;" style="width:auto;height:auto;background:none;">
                            </div>
                        </div>
                        </label>
                    </div>
                </div>
                <div class="cl"></div>
                <div style="padding-top:15px;">
                    <input id="cmdCalculate" type="image" src="<?php bloginfo('template_url'); ?>/img-calc/calc.png" class="calc_btn big calculator" style="width:auto;height:auto;background:none;" onclick="fncCalculate();
                            switch1();
                            return false;" />
                    <input id="cmdReset" type="image" src="<?php bloginfo('template_url'); ?>/img-calc/drop.png" class="calc_btn big clean" style="width:auto;height:auto;background:none;" onclick="cleanForm();
                            return false;" />
                </div>
            </div>
            <div class="calc_body fl">

                <div class="result" id="result" style="background: url('<?php bloginfo('template_url'); ?>/img-calc/resbox.png') no-repeat; width:320px;height:256px;padding:10px;">&nbsp;</div>

                <div class="result_text" >Инструкция по использованию калькулятора - <a href="wp-content/uploads/2014/12/rukovodstvo-polzovatelya-po-shengenskomu-kalkulyatoru.pdf" target="_blank" >скачать здесь</a>
                </div> 

            </div>
            <div> 
                <div class="mode_control"><input type="radio" class="" name="group1" value="altVisa" id="altVisa" checked="" /> <label for="altVisa" class="">Планирование</label>
                    <br />
                    <input type="radio" class="" name="group1" value="altControl" id="altControl" /> <label for="altControl" class="">Контроль</label></div>
                <div class="mode_control second"><input type="radio" class="" name="view" value="week" id="not_random" checked="" onclick="fncPassport(true);" /> <label for="not_random" class="">По поездкам</label>
                    <br />
                    <input type="radio" class="" name="view" value="month" id="random" onclick="fncPassport(false);" /> <label for="random" class="">Произвольно</label></div>
            </div>
            <div style="clear: both; height: 1px;"></div>

        </form>

        <p><?php the_content(); ?></p>

    </div>
</div>


<?php //include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php // include_once (TEMPLATEPATH . '/page-zagran-N-map.php');   ?>

<?php get_footer(); ?>
<?php
//include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>