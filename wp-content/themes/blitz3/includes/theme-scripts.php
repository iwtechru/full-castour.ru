<?php
function my_script() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_bloginfo('template_url').'/js/jquery.min-1.7.2.js', false, '');
		wp_enqueue_script('jquery');
		//wp_enqueue_script('prettyphoto', get_bloginfo('template_url').'/js/jquery.prettyPhoto.js', array('jquery'), '2.5.6');
		//wp_enqueue_script('cufon-yui', get_bloginfo('template_url').'/js/cufon-yui.js', array('jquery'), '1.0.9');
		//wp_enqueue_script('trebuchet', get_bloginfo('template_url').'/js/Trebuchet_MS_400-Trebuchet_MS_700-Trebuchet_MS_italic_400.font.js', array('jquery'));
		//wp_enqueue_script('cycle', get_bloginfo('template_url').'/js/jquery.cycle.all.min.js', array('jquery'));
		wp_enqueue_script('dropdownmenu', get_bloginfo('template_url').'/js/dropdown.js', array('jquery'));

	}
}
add_action('init', 'my_script');
?>