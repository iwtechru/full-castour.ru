<?php
function blitz_widgets_init() {
	register_sidebar(array(
		'name'          => 'Сквозной сайдбар стр. Сверху',
		'id'         	=> 'page-sidebar-top',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar( array(
		'name' 			=> __( 'Сайдбар в записи', 'templatesquare' ),
		'id' 			=> 'post-sidebar',
		'description' 	=> __( 'Located at the right side of archives, single and search.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Сайдбар обычной страницы',
		'id'         	=> 'page-sidebar',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	register_sidebar(array(
		'name'          => 'Сайдбар Вопросов и ответов',
		'id'         	=> 'page-archive',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	register_sidebar(array(
		'name'          => 'Сайдбар главной страницы',
		'id'         	=> 'page-sidebar-home',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));


	
	//Вид на жительство
	register_sidebar(array(
		'name'          => 'Вид на жительство',
		'id'         	=> 'page-vid',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	//Разрешение на временное проживание 
	register_sidebar(array(
		'name'          => 'Разрешение на временное проживание',
		'id'         	=> 'page-vr-pr',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	//Загранпаспорт
	register_sidebar(array(
		'name'          => 'Загранпаспорт',
		'id'         	=> 'page-zagr',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	//РФ паспорт
	register_sidebar(array(
		'name'          => 'РФ паспорт',
		'id'         	=> 'page-rf',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	//Разрешение на работу
	register_sidebar(array(
		'name'          => 'Разрешение на работу',
		'id'         	=> 'page-razr-na-rab',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	//Гражданство РФ
	register_sidebar(array(
		'name'          => 'Гражданство РФ',
		'id'         	=> 'page-gr-rf',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	//Продолжаем
	register_sidebar(array(
		'name'          => 'Подвал: колонка 1',
		'id'         	=> 'footer1',
		'description'   => __( 'Located in the footer area of this theme.', 'templatesquare' ),
		'before_widget' => '<ul class="prefooter-block><li id="%1$s" class="widget-container %2$s prefooter__list">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Подвал: колонка 2',
		'id'         	=> 'footer2',
		'description'   => __( 'Located in the footer area of this theme.', 'templatesquare' ),
		'before_widget' => '<ul class="prefooter-block><li id="%1$s" class="widget-container %2$s prefooter__list">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Подвал: колонка 3',
		'id'         	=> 'footer3',
		'description'   => __( 'Located in the footer area of this theme.', 'templatesquare' ),
		'before_widget' => '<ul class="prefooter-block><li id="%1$s" class="widget-container %2$s prefooter__list">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	register_sidebar(array(
		'name'          => 'Сквозной сайдбар стр. Снизу',
		'id'         	=> 'page-sidebar-bottom',
		'description'   => __( 'Located at the right side of page templates.', 'templatesquare' ),
		'before_widget' => '<ul><li id="%1$s" class="widget-container %2$s">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
	
	register_sidebar(array(
		'name'          => 'Подвал: колонка 4',
		'id'         	=> 'footer4',
		'description'   => __( 'Located in the footer area of this theme.', 'templatesquare' ),
		'before_widget' => '<ul class="prefooter-block><li id="%1$s" class="widget-container %2$s prefooter__list">',
		'after_widget' 	=> '</li></ul>',
		'before_title' 	=> '<h2 class="widget-title">',
		'after_title' 	=> '</h2>',
	));
}
/** Register sidebars by running blitz_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'blitz_widgets_init' );
?>