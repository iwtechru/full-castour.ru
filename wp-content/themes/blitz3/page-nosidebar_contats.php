<?php
/**
 * Template Name: Без сайдбара с контактами 
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>


<div id="maincontent">
	<div id="content" class="contentfull">
		<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>

		<?php if(!is_front_page()){?><?php include_once (TEMPLATEPATH . '/title.php'); ?><?php }?>
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php if(!is_front_page()){post_class();} ?>>
			<div class="entry-content">
				<?php the_content(); ?>
				
			</div><!-- .entry-content -->
		</div><!-- #post-## -->
		

		
		<?php endwhile; ?>
		
		<? $loop = new WP_Query('page_id=1694');
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
		<script type="text/javascript">
	
			$(document).ready(function(){
			$("#dont_call").click(function(){
				$(".hidden-tel").slideToggle("slow");
				return false;
			});
		});
		</script>
		<div class="cols contact">
			<div class="col-left">
				<?php echo types_render_field("contacts", array());?>
			</div>
			<div class="col-right">
				<div class="mapcas">
					<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
					<div id="ymaps-map-id_136196192441591033636" style="width: 349px; height: 245px;"> <script type="text/javascript">// <![CDATA[
					function fid_136196192441591033636(ymaps) {var map = new ymaps.Map("ymaps-map-id_136196192441591033636", {center: [37.62567410052483, 55.77035836567188], zoom: 15, type: "yandex#map"});map.controls.add("zoomControl").add("mapTools").add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));map.geoObjects.add(new ymaps.Placemark([37.626232, 55.771894], {balloonContent: "КасТур"}, {preset: "twirl#redDotIcon"}));};
					// ]]></script>
					<script charset="utf-8" type="text/javascript" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=zrWmQyh_2kAKK4TvjLuqEeLP2FUQ2_Pl&amp;width=349&amp;height=245"></script>
					<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
					<!--a href="">Карта в большом размере</a-->
					</div>
				</div>
			</div>
			<div class="bottom-form">
			<?php echo types_render_field("we_are_call", array());?>
			</div>
		</div>
		<?php endwhile; ?> 
		
	</div><!-- end content -->
</div><!-- end maincontent -->
<?php get_footer(); ?>
