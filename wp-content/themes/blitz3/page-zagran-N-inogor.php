<?php
/**
 * Template Name: Загран паспорт N Иногор.
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>
<div class="wrap">
		<div class="top-text">
			<?php include_once (TEMPLATEPATH . '/page-zagran-N-content-block.php'); ?>
		</div>
	</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-1.php'); ?>
			<div class="form-wrap">
				<div class="form-col pass">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_1.png" alt="">
						Тип загранпаспорта
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="bio" name="type_p" id="bio" checked="checked" class="styled"><label for="bio">Биометрический</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="old" name="type_p" id="old" class="styled"><label for="old">Старого образца</label>
						</div>						
					</div>
				</div>
				<div class="form-col high">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_2.png" alt="">
						Возраст получателя
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="14" name="age" id="14" checked="checked" class="styled"><label for="14">до 14 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="14_17" name="age" id="14_17" class="styled"><label for="14_17">от 14 до 17 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="18" name="age" id="18" class="styled"><label for="18">18 лет и старше</label>
						</div>	
					</div>
				</div>
				<div class="form-col country">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_3.png" alt="">
						Место регистрации
					</div>
					<div class="labels">
					
						<div class="labels-wrap"><input type="radio" value="moscow" name="region" id="moscow"  class="styled"><label for="moscow">Москва</label></div>
						<div class="labels-wrap"><input type="radio" value="moscow_obl" name="region" id="moscow_obl" class="styled"><label for="moscow_obl">Московская область</label></div>
						<div class="labels-wrap"><input type="radio" value="others" name="region" id="others" class="styled" checked="checked"><label for="others">Другие регионы РФ</label></div>
					</div>
				</div>
			</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-2.php'); ?>
		<div class="reload" id="output">		
		<?php require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		$loop = new WP_Query('page_id=1694');
		while ( $loop->have_posts() ) : $loop->the_post();	 ?>
				
		<?php 
			
		$select_price = "bio14others";
		$select_doc = "bio14";		?>
		<?php include_once (TEMPLATEPATH . '/filter-variants-N.php'); ?>	
	<?php endwhile; ?>
		</div>	
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-3.php'); ?>
	<div class="big-notice big-notice_nob">
		<?php include_once (TEMPLATEPATH . '/page-zagran-N-cross-url-block.php'); ?>
	</div>
	<div class="tabs-wrap">
		<ul class="tabs">
			<li class="tabs-current">Обратите внимание</li>
			<li>Возможные причины отказа</li>
			<li>Преимущества «Кастур</li>
		</ul>
		<div class="tabs-box visible">
			<ul class="tabs-item">
				<li class="tabs__list">Для молодых людей в возрасте от 18 до 27 лет необходимо определить воинскую обязанность. Если гражданин прошел военную службу, он может представить, либо военный билет, либо нужна справка из военкомата о том, что он не призван.</li>
				<li class="tabs__list">Если срок действия заграничного паспорта еще не истек, но подается заявление на новый загранпаспорт, то действующий паспорт аннулируется только при получении нового (пока оформляется новый паспорт старым можно пользоваться).</li>
				<li class="tabs__list">Для оформления заграничного паспорта нового поколения необходимы любые 2 фотографии для документов (на анкеты).  На сам паспорт фотографирует инспектор в специальной кабинке при подаче документов.</li>
				<li class="tabs__list">Вписывание ребенка в заграничный паспорт теперь носит лишь информационный характер. Каждый гражданин РФ выезжающий за рубеж должен иметь отдельный заграничный паспорт от рождения.</li>
				<li class="tabs__list">На сегодняшний день ребенку до 18 лет можно оформить загранпаспорт как старого так и нового образца.</li>
			</ul>
		</div>
		<div class="tabs-box">
			<ul class="tabs-item">
				<li class="tabs__list">имел допуск к сведениям «совершенно секретным» или «сведениям особой важности».</li>
				<li class="tabs__list">подлежит призыву на срочную военную или альтернативную гражданскую службу.</li>
				<li class="tabs__list">задержан по подозрению в совершении преступления или привлечен в качестве обвиняемого.</li>
				<li class="tabs__list">осужден и отбывает наказание, в том числе условное осуждение.</li>
				<li class="tabs__list">уклоняется от исполнения обязательств, наложенных на него судом.</li>
				<li class="tabs__list">предоставил ложные сведения при оформлении документов (ограничение на выдачу действует до предоставления полной и достоверной информации, но не более одного месяца).</li>
			</ul>
		</div>
		<div class="tabs-box">
			<ul class="tabs-item">
Оформить загранпаспорт без регистрации в Москве в настоящее время довольно проблематично – главным образом из-за того, что срок его изготовления будет довольно длительным (в некоторых случаях это занимает более 4-5 месяцев). Чтобы получить загран без прописки, рекомендуем обратится в нашу компанию Кастур. Фирм, оказывающих подобного рода услуги в Москве достаточно много, но мы имеем существенные преимущества:
<ul>
	<li><b>Высокий профессионализм</b>. Обладая огромным опытом работы и занимаясь оформлением загранпаспортов иногородним свыше 5 лет, мы можем решить практически любую проблему наших клиентов.</li>
	<li><b>Сжатые сроки</b>. Мы знаем все тонкости законодательства и процедуры, поэтому ошибки на этапе оформления и подачи документов исключены, а значит заграничный паспорт клиент получит максимально быстро, даже если имеет только временную регистрацию в Москве.</li>
	<li><b>Официально на 100%</b>. Мы работает в строгом соответствии с законодательством РФ, то есть при оформлении паспорта проходим все необходимые процедуры.</li>
	<li><b>Недорого</b>. Цены на наши услуги являются конкурентными, хотя качество их оказания одно из лучших в столице. Оформление загранпаспорта без регистрации и прописки стоит у нас от 12 000 рублей (для лиц старше 18 лет при изготовлении в течении 20-23 дней). Очевидно, что чем быстрее делается паспорт для иногородних, тем дороже будет стоить услуга.</li>
</ul>
Обратившись в компанию Кастур, вы можете быть уверены в том, что ваша проблема будет решена в максимально короткие сроки при минимуме затрат. Мы надежный помощник в сложном деле, что доказали годами работы и только положительными отзывами.
			</ul>
		</div>
		<script>
	$('ul.tabs').delegate('li:not(.tabs-current)', 'click', function() {
		$(this).addClass('tabs-current').siblings().removeClass('tabs-current').parents('div.tabs-wrap').find('div.tabs-box').eq($(this).index()).fadeIn(150).siblings('div.tabs-box').hide();
	});
		</script>
	</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>