<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 * We filter the output of wp_title() a bit -- see
	 * twentyten_filter_wp_title() in functions.php.
	 */
	wp_title( '|', true, 'right' ); 

	?></title>

<link href="<?php bloginfo('template_url'); ?>/slider.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href="<?php bloginfo('template_url'); ?>/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php 
$favicon = get_option('templatesquare_favicon');
if($favicon =="" ){
?>
<link rel="shortcut icon" href="http://castour.ru/icon.png" />
<?php }else{?>
<link rel="shortcut icon" href="http://castour.ru/icon.png" />
<?php }?>
<script type="text/javascript">$(document).ready(function(){$('.selectBlock').sSelect();});</script> 
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<!-- ////////////////////////////////// -->
<!-- //      Javascript Files        // -->
<!-- ////////////////////////////////// 
<script type="text/javascript">
	 Cufon.replace('h1') ('h2') ('h3') ('h4') ('h5') ('h6') ('#topmenu ul li') ('.tagline') ('.desc') (' #logo h1 a') ('#sideright ul li li a', {
	 hover: true
})
</script>-->
<script language="JavaScript">
<!--

function  changeCss() {
            var w =  screen.width;
            var h =  screen.height;
            if (w <=  '1024')
                {
                         $('.cycle').css('margin', '0 2%');
                }
            else
                  {
                         if (w < '1440' )
                                {
                                         $('.cycle').css('margin', '0 18%');
                                }
                            else
                                  {
										 if (w >= '1440' ) 
											{
													$('.cycle').css('margin', '0 22%');
											}
                                 };
                };
           }; 
window.onload  = changeCss;
//-->
</script>
 
<?php $time = get_option('templatesquare_slider_timeout'); ?>
<script type="text/javascript">
jQuery(document).ready(function($) {

		function onBefore() {
			 $('.slidetext').cycle({
			 fx: 'fade',
			 timeout: 1000, 
			 speed: 700, 
			pause:   700,
			autostop: 1
			 });
		}

		function onAfter() {
			 $('.slidetext').cycle('pause');
		}
				
		/* for cycle */
		$('#slideshow').cycle({
		timeout: 2000,  // milliseconds between slide transitions (0 to disable auto advance)
		fx:      'scrollLeft', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
		pause:   1,	  // true to enable "pause on hover"
		pauseOnPagerHover: 0, // true to pause when hovering over pager link
		before:   onBefore,

		prev: '#prev',
		next: '#next',
		
		});
		
		/* for widget cycle */
		$('.boxslideshow').cycle({
		timeout: 10000,  // milliseconds between slide transitions (0 to disable auto advance)
		fx:      'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...            
		pause:   0,	  // true to enable "pause on hover"
		pauseOnPagerHover: 0 // true to pause when hovering over pager link
		});
		
		/* for portfolio prettyPhoto */
		$("#gallery a[rel^='prettyPhoto']").prettyPhoto({theme:'dark_rounded'});
		
});



</script>
</head>

<body <?php body_class(); ?>>

<?php if(is_front_page()){
$con = "top_container";
}else{
$con = "top_container_inner";
}
?>
	<div id="<?php echo $con; ?>">
		<div class="centercolumn">
			<div id="top">
				<?php
				$logotype = get_option('templatesquare_logo_type');
				$logoimage = get_option('templatesquare_logo_image'); 
				$sitename = get_option('templatesquare_site_name');
				$tagline = get_option('templatesquare_tagline');
				if($logoimage == ""){ $logoimage = get_bloginfo('stylesheet_directory') . "/images/logo.gif"; }
				?>
				<?php if($logotype == 'textlogo'){ ?>
					<?php if($sitename =="" && $tagline ==""){?>
					<div id="logo">
						<div class="pad-logo">
							<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
							<span class="desc"><?php bloginfo('description'); ?></span>
						</div>
					</div>
					<?php }else{ ?>
					<div id="logo">
						<div class="pad-logo">
							<h1><a href="<?php echo get_option('home'); ?>/"><?php echo $sitename; ?></a></h1>
							<span class="desc"><?php echo $tagline; ?></span>
						</div>
					</div>
					<?php } ?>
				<?php }else{ ?>
					<div id="logo">
						<a style="float:left; "href="<?php echo get_option('home'); ?>/" title="<?php _e('Home','blitz'); ?>"><img src="<?php echo $logoimage; ?>" alt="" height="60" /></a>
						<span style="width:330px;color: #333333; position:relative;top: 30px; left:20px;font-style: italic;font-size: 12px;font-family: 'Open Sans', sans-serif; "><?php bloginfo('description'); ?></span>
					</div>
				<?php } ?>
				
				<div id="topcontact"><small><sup>+7 (495)</sup></small><big><b>608-66-62</b></big><b><small></small></b><small> <sup>+7 (495)</sup></small><b><big>608-66-64</big></b></div>
				
				<div class="clr"></div>
			</div><!-- end top -->
			<div id="topmenu">
				
				<?php if ( function_exists( 'wp_nav_menu' ) ) {?>
					<?php 
						wp_nav_menu( array(
							'container'       => 'ul', 
							'menu_class'      => 'menu', 
							'menu_id'         => 'nav',
							'depth'           => 0,
							'theme_location' => 'mainmenu',
							'walker' => new description_walker(),
						
							)
						);
					  ?>
				<?php } ?>
			</div><!-- end topmenu -->
		</div>

			<?php if(is_front_page()){?>
			<div id="slidecontainer">
				 <?php  include_once (TEMPLATEPATH . '/slider.php'); ?>
			</div><!-- end slidecontainer -->
			<?php } ?>
<div class="centercolumn">