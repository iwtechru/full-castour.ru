<div id="slide">
	<div id="slideshow">  
		<?php
			query_posts("post_type=slider&post_status=publish&posts_per_page=-1");
			while ( have_posts() ) : the_post();
		?>
		
	  <div class="cycle">

		<div class="slideimg">			
			<?php
			the_post_thumbnail('slider', array('title' => ''));
			?>
		</div>
<div class="slidetext">
			<span></span><span><h1><?php the_title(); ?></h1><?php the_content();?></span> 
		</div>
	  </div><!-- end of cycle -->
	  
	  <?php endwhile;?>
	  <?php wp_reset_query();?>
	</div><!-- end slideshow -->

</div>
