<?php
header('Content-Type: html; charset=utf-8');
if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {

// Получаем данные из формы
$type_p = $_POST['type_p'];
$age = $_POST['age'];
$region = $_POST['region'];

if (!isset($type_p) && !isset($age) && !isset($region))
{
	/* 
	Здесь можно вывести сообщение о необходимости выбрать параметры фильтра, либо оставить пустым.      
	*/
	}
	else 
	{ 
		require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		// 1. Старого образца,  ребенок до 14 лет , Москва
		if ($type_p=='old' && $age=='14' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1255 );
		}

		// 2.  Старого образца,  ребенок до 14 лет , Московская область
		elseif ($type_p=='old' && $age=='14' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1257 );
		}

		// 3.  Старого образца, ребенок до 14 лет ,  регионы РФ 
		elseif ($type_p=='old' && $age=='14' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1259 );
		}

		// 4.  Биометрический , ребенок до 14 лет , Москва 
		elseif ($type_p=='bio' && $age=='14' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1261 );
		}

		// 5.  Биометрический,  ребенок до 14 лет , Московская область
		elseif ($type_p=='bio' && $age=='14' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1263 );
		}

		// 6.  Биометрический, ребенок до 14 лет ,  регионы РФ 
		elseif ($type_p=='bio' && $age=='14' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1265 );
		}

		// 7.  Старого образца, от 14 до 17 лет , Москва 
		elseif ($type_p=='old' && $age=='14_17' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1267 );
		}

		// 8.  Старого образца, от 14 до 17 лет , Московская область 
		elseif ($type_p=='old' && $age=='14_17' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1269 );
		}

		// 9.  Старого образца, от 14 до 17 лет , регионы РФ
		elseif ($type_p=='old' && $age=='14_17' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1271 );
		}

		// 10.  Биометрический , от 14 до 17 лет , Москва 
		elseif ($type_p=='bio' && $age=='14_17' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1273 );
		}

		// 11.  Биометрический, от 14 до 17 лет , Московская область 
		elseif ($type_p=='bio' && $age=='14_17' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1275 );
		}

		// 12.  Биометрический, от 14 до 17 лет , регионы РФ
		elseif ($type_p=='bio' && $age=='14_17' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1277 );
		}

		// 13.  Старого образца, 18 лет и старше, Москва 
		elseif ($type_p=='old' && $age=='18' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1279 );
		}

		// 14.  Старого образца, 18 лет и старше, Московская область 
		elseif ($type_p=='old' && $age=='18' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1281 );
		}

		// 15.  Старого образца, 18 лет и старше, регионы РФ
		elseif ($type_p=='old' && $age=='18' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1283 );
		}

		// 16.  Биометрический , 18 лет и старше , Москва 
		elseif ($type_p=='bio' && $age=='18' && $region=='moscow')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1285 );
		}

		// 17.  Биометрический, 18 лет и старше, Московская область 
		elseif ($type_p=='bio' && $age=='18' && $region=='moscow_obl')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1287 );
		}

		// 18.  Биометрический, 18 лет и старше, регионы РФ
		elseif ($type_p=='bio' && $age=='18' && $region=='others')
		{
		$args = array( 'post_type' => 'zpassport', 'p' => 1289 );
		}
  
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
?>
<div style="padding:0px 0px 0px 0px">
	<?php  $myExcerpt = get_the_excerpt();
  $tags = array("<p>", "</p>");
  $myExcerpt = str_replace($tags, "", $myExcerpt);
  echo $myExcerpt; ?> 
</div>
<!--<div class="selectBoxMainBottom"></div>-->
<div style="width:600px;height:auto;background:#fff;padding:30px 0px 10px 0px;margin-left:0px;border-top:#d78841 solid 1px;"> 
<?		
		the_content(); 
		endwhile; 
	}
}
?>
</div>