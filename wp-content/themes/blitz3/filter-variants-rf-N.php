<div class="gray-bg">
	<div class="wrap">
		<div class="hidden-table">
			<div class="reload-table" style="display: block; opacity: 1;">
				<?php echo types_render_field($select_price, array());?>
			</div>
		</div>
		<div class="reload-notice">*Сроки указаны в рабочих днях</div>
	</div>
</div>
<div class="orange-call-wrap">
	<div class="orange-call">Звоните <span class="orange-call__big">+7 (495) 608-66-62</span></div>
</div>
<div class="cols">
	
	<div class="col-left">
	
			Паспорт гражданина Российской Федерации — основной документ, удостоверяющий личность гражданина РФ на территории РФ. Все граждане России в возрасте от 14 лет обязаны иметь паспорт. Выдача паспортов производится Федеральной миграционной службой по месту жительства, по месту пребывания или по месту обращения гражданина.
<br />
			Обратившись в компанию «Кастур», вы можете быть уверены, что получение и обмен внутригражданского паспорта будет сделан легально и максимально профессионально. Мы оказываем помощь и в случаях, когда получение паспорта Р.Ф. нужно срочно.
	</div>
	<div class="col-right">
		<div class="hidden-doc" style="display: block; opacity: 1;">
			<h3 class="sub-title">Что включено в стоимость:</h3>
			<ul class="content-item">
				<li>Ксерокопия документов</li>
				<li>Сдача документов без очереди</li>
				<li>Получение документов без очереди</li>
				<li>Отслеживание готовности</li>
			</ul>
		</div>
	</div>
</div>