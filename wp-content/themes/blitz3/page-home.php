<?php
/**
 * Template Name: Главная
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
	<div id="maincontent">
		<div id="content">

			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'templatesquare' ), 'after' => '</div>' ) ); ?>
					
				</div><!-- .entry-content -->
			</div><!-- #post-## -->
			
			<?php endwhile; ?>
		</div><!-- end content -->

			<div id="sideright" style="background:none;color:#4e4e4e;">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-sidebar-home') ) : endif; 

				?>
			</div><!-- end sideright -->

		<div class="clr"></div><!-- clear float -->
	</div><!-- end maincontent -->
<?php get_footer(); ?>
