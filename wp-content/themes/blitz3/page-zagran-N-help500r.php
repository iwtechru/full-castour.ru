<?php
/**
 * Template Name: Загран паспорт N Помощь в заполнении
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>
<div class="wrap">
		<div class="top-text">
			<?php while ( have_posts() ) : the_post(); ?>

	<h2 class="top-text__title"><?php the_title();?></h2>
	

	<?php the_content();?>
<?php endwhile; ?>
		</div>
	</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

	

	
	
<div class="blue-form-wrap">
	<div class="blue-form">
			<div class="wrap">
				<div class="blue-form__cols">
					<div class="blue-form__right">
						<div class="blue-form__cols-title">или закажите звонок:</div>
						<div class="blue-form__wrap">
							<?php echo do_shortcode('[contact-form-7 id="1847" title="Позвоните сейчас или закажите звонок"]'); ?>
						</div>
					</div>
					<div class="blue-form__left">
						<div class="blue-form__cols-title">Позвоните сейчас</div>
						<ul class="blue-form__left-tel">
							<li class="blue-form__left-list">+7 (495) 608-66-62</li>
							<li class="blue-form__left-list">+7 (495) 608-66-64</li>
							<li class="blue-form__left-list">+7 (495) 227-13-15</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
</div>
	<div class="map">
		<div class="map-wrap">
			<div class="map-info">
				<div class="map-info__inner">
					<div class="map-info__title">Адрес офиса CASTOUR:</div>
					<div class="map-info__b">
						ул. Трубная, д. 32, стр. 4, офис 3
						<span class="map-info__bs">(м. Сухаревская, Цветной бульвар или Трубная)</span>
					</div>
					<div class="map-info__title">График работы</div>
					<div class="map-info__b">
						Понедельник-пятница:
						<span class="map-info__bs">с 10:00 до 20:00</span>
					</div>
					<div class="map-info__b">
						Суббота:
						<span class="map-info__bs">с 10:00 до 17:00</span>
					</div>
					<div class="map-info__b">
						Воскресенье:
						<span class="map-info__bs">выходной</span>
					</div>
					<div class="map-info__title">Телефон</div>
					<div class="map-info__b">
						+7 (495) 608-66-62
					</div>
					<i class="map-info__dot map-info__dot_1"></i>
					<i class="map-info__dot map-info__dot_2"></i>
					<i class="map-info__dot map-info__dot_3"></i>
					<i class="map-info__dot map-info__dot_4"></i>
				</div>
			</div>
		</div>
		<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=4hEx62oYGwtu0KnJLmY-JBc5tZW_HFZS"></script>
	</div>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>