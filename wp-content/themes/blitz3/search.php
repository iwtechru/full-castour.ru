<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
	<div id="maincontent">
		<div id="content">
			<?php if ( function_exists('yoast_breadcrumb')) {
			yoast_breadcrumb('<div id="breadcrumbs">','</div>');
			} ?>

			<h1 class="pagetitle"><?php printf( __( 'Search Results for: %s', 'templatesquare' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			<div id="searchresult">
				<?php if ( have_posts() ) : ?>
				
				<?php
				/* Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called loop-search.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'search' );
				?>
				<?php else : ?>
				<div id="post-0" class="post no-results not-found">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'templatesquare' ); ?></h2>
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'templatesquare' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-0 -->
				<?php endif; ?>
			</div>
		</div><!-- end content -->
		
		<div id="sideright">
			<?php get_sidebar(); ?>
		</div><!-- end sideright -->
		<div class="clr"></div><!-- clear float -->
	</div><!-- end maincontent -->
<?php get_footer(); ?>
