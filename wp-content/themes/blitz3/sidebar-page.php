<?php
/**
 * @package WordPress
 * @subpackage Vivee
 * @since Vivee 3.0
 */

?>
<div class="widget-area">
<? if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-sidebar-top') ) : endif;

//Вид на жительство
if (is_page('108') ) {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-vid') ) : endif;
}

//Разрешение на временное проживание 
elseif ( is_page('23') ) {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-vr-pr') ) : endif;
} 

//Загранпаспорт 
elseif ( is_page('1246') ) {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-zagr') ) : endif;
} 
//РФ паспорт  
elseif ( is_page('1296') ) {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-rf') ) : endif;
}

//Разрешение на работу
elseif ( is_page('26') ) {
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-razr-na-rab') ) : endif;
} 


else { }

if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('page-sidebar-bottom') ) : endif; ?>

</div>