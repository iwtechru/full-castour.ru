<?php //if ( is_user_logged_in() ) { ?>
<?php
/**
 * Template Name: Загранпаспорт
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
	<div id="maincontent">
		<div id="content" class="contentfull">
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display'))
				{
					bcn_display();
				}?>
			</div>
			
			
			
			
			<div class="wrap" id="wrap"> 
			
				<?php while ( have_posts() ) : the_post(); ?>
					<div class="head">
						<h2><?php the_title(); ?></h2>
						<a target="_blank" href="http://castour.ru/zagranpasporta/zapolnenie-ankety" class="head-link">Только заполнить анкету - от 500р.</a>
					</div>
					<?php the_content(); ?> 
				<?php endwhile; ?>

				<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
				<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/output.js"></script> 
				<script type="text/javascript">
				$(document).ready(function(){
					$('input:radio').click(function () {         
					$("form:first").trigger("submit"); 
				   });
				}); 
				</script>

				<div class="form">
					<h3 class="form__title">Расчет стоимости загранпаспорта</h3>
					<form id="iForm" action="<?php echo get_template_directory_uri(); ?>/filter_zagran.php" method="post">
						<div class="form-wrap">
							<div class="form-col pass">
								<img src="http://castour.ru/wp-content/themes/blitz-silver/i/pass.jpg" alt="">
								<div class="labels">
									<b>Тип загранпаспорта</b>
									
									<input type="radio" value="bio" name="type_p" id="bio" checked="checked"><label for="bio">Биометрический</label><br>
	<input type="radio" value="old" name="type_p" id="old" ><label for="old">Старого образца</label><br>							
								</div>
							</div>
							<div class="form-col high">
								<img src="http://castour.ru/wp-content/themes/blitz-silver/i/high.jpg" alt="">
								<div class="labels">
									<b>Возраст получателя</b>
									<input type="radio" value="14" name="age" id="14"checked="checked" ><label for="14">до 14 лет</label>
									<br>
									<input type="radio" value="14_17" name="age" id="14_17"><label for="14_17">от 14 до 17 лет</label><br>
									<input type="radio" value="18" name="age" id="18" ><label for="18">18 лет и старше</label>
									
								</div>
							</div>
							<div class="form-col country">
								<img src="http://castour.ru/wp-content/themes/blitz-silver/i/country.png" alt="">
								<div class="labels">
									<b>Место регистрации</b>
									<input type="radio" value="moscow" name="region" id="moscow" checked="checked"><label for="moscow" >Москва</label><br>
									<input type="radio" value="moscow_obl" name="region" id="moscow_obl"><label for="moscow_obl">Московская область</label><br>
									<input type="radio" value="others" name="region" id="others"><label for="others">Другие регионы РФ</label>
								</div>
							</div>
						</div>
						<h4 class="form__subtitle">Стоимость изготовления паспорта (меняется в зависимости от срочности):</h4>
					</form>
				</div>
				<? $loop = new WP_Query('page_id=1694');
					while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="reload" id="output">
				<div class="hidden-table">
					<div class="reload-table">
					<?php echo types_render_field("bio14moscow", array());?>
					</div>
				</div>
					<div class="reload-notice">* - Сроки указаны в рабочих днях</div>
					<div class="cols">
						<div class="col-left">
						<h3 class="sub-title">Необходимые документы:</h3>
						<?php echo types_render_field("bio14", array());?>
							

						</div>
						<div class="col-right">
							<div class="big-notice">
								<?php echo types_render_field("gos_poshlina", array());?>
							</div>
						</div>
					</div>
			
					
					
					<div class="cols">
						<div class="col-left">
							
							<?php echo types_render_field("in_price", array());?>
						</div>	
							

						
						<div class="col-right">
							<div class="big-notice">
								<?php echo types_render_field("vyezd", array());?> 
							</div>
						</div>
					</div>
					
					
					
				</div>

				<?php echo types_render_field("content_zagran_bottom", array());?>
				

				<script type="text/javascript">
				$(document).click( function(event){
						if( $(event.target).closest("#slide_on_click").length ) 
						return;
						$("#slide_on_click").slideUp("slow");
						event.stopPropagation();
					});
					$('#slide_on_off').click( function() {
							$(this).siblings("#slide_on_click").slideToggle("slow");
							return false;
						});
				</script>
<script type="text/javascript">				
				$(document).click( function(event){
						if( $(event.target).closest("#slide_on_click_stop").length ) 
						return;
						$("#slide_on_click_stop").slideUp("slow");
						event.stopPropagation();
					});
					$('#slide_on_off_stop').click( function() {
							$(this).siblings("#slide_on_click_stop").slideToggle("slow");
							return false;
						});	

						
				</script> 
<script type="text/javascript">
			
				
				
	$(document).ready(function(){
    $("#dont_call").click(function(){
        $(".hidden-tel").slideToggle("slow");
        return false;
    });
});
</script>
				<div class="cols contact">
					<div class="col-left">
						<?php echo types_render_field("contacts", array());?>
					</div>
					<div class="col-right">
						<div class="mapcas">
							<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
							<div id="ymaps-map-id_136196192441591033636" style="width: 349px; height: 245px;"> <script type="text/javascript">// <![CDATA[
							function fid_136196192441591033636(ymaps) {var map = new ymaps.Map("ymaps-map-id_136196192441591033636", {center: [37.62567410052483, 55.77035836567188], zoom: 15, type: "yandex#map"});map.controls.add("zoomControl").add("mapTools").add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));map.geoObjects.add(new ymaps.Placemark([37.626232, 55.771894], {balloonContent: "КасТур"}, {preset: "twirl#redDotIcon"}));};
							// ]]></script>
							<script charset="utf-8" type="text/javascript" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=zrWmQyh_2kAKK4TvjLuqEeLP2FUQ2_Pl&amp;width=349&amp;height=245"></script>
							<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
							<!--a href="">Карта в большом размере</a-->
							</div>
						</div>
					</div>
					<div class="bottom-form">
					<?php echo types_render_field("we_are_call", array());?>
					</div>
				</div>
				<?php endwhile; ?> 
			</div>
			
			
		</div><!-- end content -->
			
		<div class="clr"></div><!-- clear float -->
	</div><!-- end maincontent -->
<?php get_footer(); ?>
<?php //} else {} ?> 