		<script type="text/javascript">
		
		$(function(){
            $(".reload-table").hide();
			$(".reload-table").fadeTo(500, 1);
			$(".hidden-doc").hide();
			$(".hidden-doc").fadeTo(500, 1);

        });
		</script>

	<div class="gray-bg">
	<div class="wrap">
		<div class="hidden-table">
			<div class="reload-table" style="display: block; opacity: 1;">
				<table>
<tbody>
<tr class="tr-top">
<td class="goleft">Срочность*</td>
<td class="green">20-23 дня</td>
<td>16-18 дней</td>
<td>12-14 дней</td>
<td>9-10 дней</td>
<td>8 дней</td>
<td>7 дней</td>
<td>6 дней</td>
<td class="orange">5 дней</td>
</tr>
<tr class="tr-bottom">
<td class="goleft">Цена, руб</td>
<td class="green">15 000 р.</td>
<td>16 000 р.</td>
<td>18 000 р.</td>
<td>23 000 р.</td>
<td>28 000 р.</td>
<td>33 000 р.</td>
<td>39 000 р.</td>
<td class="orange">47 000 р.</td>
</tr>
</tbody>
</table>
			</div>
		</div>
		<div class="reload-notice">*Сроки указаны в рабочих днях</div>
	</div>
</div>
<div class="orange-call-wrap">
	<div class="orange-call">Звоните <span class="orange-call__big">+7 (495) 608-66-62</span></div>
</div>
<div class="cols">
	<div class="col-left">
		<div class="hidden-doc" style="display: block; opacity: 1;">
			<h3 class="sub-title">Необходимые документы:</h3>
			
			<ul class="content-item">
<li>Гражданский паспорт.</li>
<li>Оригинал или копия трудовой книжки(если имеется).</li>
<li>Военный билет либо справка из военкомата(для лиц мужского пола в возрасте от 18 до 27 лет).</li>
<li>Ранее выданный заграничный паспорт.</li>
<li>2 фото размером 35×45мм(матовые).</li>
</ul>
		</div>
	</div>
	<div class="col-right">
			<h3 class="sub-title">Что включено в стоимость:</h3>
			<ul class="content-item">
				<li>Госпошлина за изготовление загранпаспорта</li>
				<li>Заполнение анкеты</li>
				<li>Ксерокопия документов</li>
				<li>Сдача документов без очереди</li>
				<li>Получение документов без очереди</li>
				<li>Отслеживание готовности</li>
			</ul>
	</div>
</div>