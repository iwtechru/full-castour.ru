<div class="gray-bg">
	<div class="wrap">
		<div class="hidden-table">
			<div class="reload-table" style="display: block; opacity: 1;">
				<?php echo types_render_field($select_price, array());?>
			</div>
		</div>
		<div class="reload-notice">*Сроки указаны в рабочих днях</div>
	</div>
</div>
<div class="orange-call-wrap">
	<div class="orange-call">Звоните <span class="orange-call__big">+7 (495) 608-66-62</span></div>
</div>
<div class="cols">
	<div class="col-left">
		<div class="hidden-doc" style="display: block; opacity: 1;">
			<h3 class="sub-title">Необходимые документы:</h3>
			
			<?php echo types_render_field($select_doc, array());?>
		</div>
	</div>
	<div class="col-right">
			<h3 class="sub-title">Что включено в стоимость:</h3>
			<ul class="content-item">
				<li>Госпошлина за изготовление загранпаспорта</li>
				<li>Заполнение анкеты</li>
				<li>Ксерокопия документов</li>
				<li>Сдача документов без очереди</li>
				<li>Получение документов без очереди</li>
				<li>Отслеживание готовности</li>
			</ul>
	</div>
</div>