<?php
/**
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

?>
<div class="widget-area s4">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer4') ) : ?><?php endif; ?>
</div>