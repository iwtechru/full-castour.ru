<?php
/**
 * Template Name: РФ Паспорт
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

get_header(); ?>
	<div id="maincontent">
		<div id="content">
			<div class="breadcrumbs">
    <?php if(function_exists('bcn_display'))
    {
        bcn_display();
    }?>
</div>

			
			<?php include_once (TEMPLATEPATH . '/title.php'); ?>
			
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">
					<?php the_content(); ?>

				</div><!-- .entry-content -->
			</div><!-- #post-## -->

			<?php endwhile; ?>  
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<!--script type="text/javascript" src="<?php// echo get_template_directory_uri(); ?>/form.js"></script-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/output.js"></script> 
<script type="text/javascript">
 $(document).ready(function(){
   $('input:radio').click(function () {
    $("#output").slideUp().fadeTo(200, 0.50);                       
    $("form:first").trigger("submit"); 
    $("#output").slideDown().fadeTo(300, 1); 
   });
});  
</script>
<style type="text/css">
input:checked + label {border-bottom: 1px solid #ca0013; color:#ca0013 }
label {margin: ; cursor: pointer; padding-bottom:1px; border: none;  color: #000;}
label:hover { border-bottom: 1px solid #b7131f; color:#b7131f}
input {visibility: ; margin-bottom: 13px;}
 
.selectBoxMain { 
	background: #fdf7de;
	display:block;
	height:auto;
	padding:0px;	
	_-moz-border-radius: 10px;
	_-webkit-border-radius: 10px;
	_border:#d78841 solid 1px;
	
}
.selectBoxMain tr td {
	border-top:#d78841 solid 1px!important;
}
.selectBoxMain table {
	border-right:#d78841 solid 1px!important;
	border-left:#d78841 solid 1px!important;
	border-top:none!important;
	color:#797979;
	font-weight:800;
	font-size: 14px;
	word-wrap: normal!important;
}

.selectBoxMain h3{ 
	color: #ff8025;
	font-size:18px;
	font-weight:800;
}
.selectBoxMain h4{ 
	font-size:12px;
	font-weight:800;
	color:#000;
	height:30px
}

.selectBoxMainBottom {
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	height:20px;
	background:#fdf7de;
	widht:100%;
	top:10px;
	position:relative;
}

.selectBoxMainBlock {
	border-left:#d78841 solid 1px!important;
	border-right:#d78841 solid 1px!important;
	border-top:#d78841 solid 1px!important;
	padding:10px;
	min-height:170px; 
}
 
.selectBoxForm {
	font-size: 12px;
	margin-left: 26px;
	margin-top: -10px
}
.selectBoxFormRegister, .selectBoxFormPrichina {
	float: left;
	padding: 6px;
}
.selectBoxFormRegister p, .selectBoxFormPrichina p{
	font-size: 12px;
	line-height: 0px;
	margin-bottom: 5px!important;
}

.selectBoxFormRegister .linepadding, .selectBoxFormPrichina .linepadding {
	padding-bottom:10px;
}

.selectBoxFormRegister {
	width: 160px;
	margin-left:20px
}
.selectBoxFormPrichina {
	width: 280px;

} 

</style>
<div class="selectBoxMain">
<div  class="selectBoxMainBlock"> 
<h3>Чтобы узнать стоимость, выберите:</h3>
<form class="selectBoxForm" id="iForm" action="<?php echo get_template_directory_uri(); ?>/filter_rf.php" method="post">
  <div class="selectBoxFormPrichina"><b>Причина</b><br/><br/>
	<p><input type="radio" name="type_p" value="1" id="1" checked="checked"/><label for="1">Получение / замена паспорта</label></p>
    <p><input type="radio"  name="type_p" value="2" id="2" /><label for="2">Утрата / хищение паспорта</label></p>
  </div>
  <div class="selectBoxFormRegister"><b>Место регистрации</b><br/><br/>
   <p><input type="radio"  name="age" value="11" id="11" checked="checked"/><label for="11">Москва</label></p>
   <p><input type="radio"  name="age" value="12" id="12"/><label for="12">Московская область</label></p>
   <p><input type="radio" name="age" value="13" id="13" /><label for="13">Другие регионы РФ</label></p>
  </div>
</form>
</div> 

<div style="clear:both;position:relative;font-size:14px" id="output"> 
<?php
	require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
	$args = array( 'post_type' => 'passport', 'p' => 1304 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post();
?>
<div style="padding:0px 0px 0px 0px">
	<?php  $myExcerpt = get_the_excerpt();
  $tags = array("<p>", "</p>");
  $myExcerpt = str_replace($tags, "", $myExcerpt);
  echo $myExcerpt; ?>
</div>
<!--<div class="selectBoxMainBottom"></div>-->
<div style="width:600px;height:auto;background:#fff;padding:30px 0px 10px 0px;margin-left:0px;border-top:#d78841 solid 1px;"> 
<?
		the_content();
		endwhile;
?>
<br />
Паспорт гражданина Российской Федерации — основной документ, удостоверяющий личность гражданина РФ на территории РФ. Все граждане России в возрасте от 14 лет обязаны иметь паспорт. Выдача паспортов производится Федеральной миграционной службой по месту жительства, по месту пребывания или по месту обращения гражданина.<br /><br />
Обратившись в компанию «Кастур», вы можете быть уверены, что получение и обмен внутригражданского  паспорта будет сделан легально и максимально профессионально.  Мы оказываем помощь и в случаях, когда получение паспорта Р.Ф. нужно срочно. 

</div>  
</div>
</div>
		</div><!-- end content -->
<style>
.fixed { padding: 0; margin-top:300px; margin-right:10px;  
position: fixed; top: 0px; right: 150px; // position: absolute; top: expression( eval(document.body.scrollTop) + "px");}
</style>		
		<div id="sideright">
			<?php get_sidebar('page'); ?>
			
		</div><!-- end sideright -->
		<div class="clr"></div><!-- clear float -->
	</div><!-- end maincontent -->
<?php get_footer(); ?>