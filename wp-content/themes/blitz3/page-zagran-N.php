<?php
/**
 * Template Name: Загран паспорт N
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */

//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>
<div class="wrap">
		<div class="top-text">
			<?php include_once (TEMPLATEPATH . '/page-zagran-N-content-block.php'); ?>
		</div>
	</div>

<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-1.php'); ?>
			<div class="form-wrap">
				<div class="form-col pass">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_1.png" alt="">
						Тип загранпаспорта
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="bio" name="type_p" id="bio" checked="checked" class="styled"><label for="bio">Биометрический</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="old" name="type_p" id="old" class="styled"><label for="old">Старого образца</label>
						</div>						
					</div>
				</div>
				<div class="form-col high">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_2.png" alt="">
						Возраст получателя
					</div>
					<div class="labels">
						<div class="labels-wrap">
							<input type="radio" value="14" name="age" id="14" checked="checked" class="styled"><label for="14">до 14 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="14_17" name="age" id="14_17" class="styled"><label for="14_17">от 14 до 17 лет</label>
						</div>
						<div class="labels-wrap">
							<input type="radio" value="18" name="age" id="18" class="styled"><label for="18">18 лет и старше</label>
						</div>	
					</div>
				</div>
				<div class="form-col country">
					<div class="form__wrap-img">
						<img src="/wp-content/themes/blitz3/img-N/form_3.png" alt="">
						Место регистрации
					</div>
					<div class="labels">
					
						<div class="labels-wrap"><input type="radio" value="moscow" name="region" id="moscow" checked="checked" class="styled"><label for="moscow">Москва</label></div>
						<div class="labels-wrap"><input type="radio" value="moscow_obl" name="region" id="moscow_obl" class="styled"><label for="moscow_obl">Московская область</label></div>
						<div class="labels-wrap"><input type="radio" value="others" name="region" id="others" class="styled"><label for="others">Другие регионы РФ</label></div>
					</div>
				</div>
			</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-2.php'); ?>

		<div class="reload" id="output">		
		<?php require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
		$loop = new WP_Query('page_id=1694');
		while ( $loop->have_posts() ) : $loop->the_post();	 ?>
				
		<?php 
			
		$select_price = "bio14moscow";
		$select_doc = "bio14";		?>
		<?php include_once (TEMPLATEPATH . '/filter-variants-N.php'); ?>	
	<?php endwhile; ?>
		</div>	
<?php include_once (TEMPLATEPATH . '/page-zagran-N-part-3.php'); ?>
	<div class="big-notice big-notice_nob">
		<?php include_once (TEMPLATEPATH . '/page-zagran-N-cross-url-block.php'); ?>
	</div>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-tabs-block.php'); ?>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php include_once (TEMPLATEPATH . '/footer-N.php'); ?>