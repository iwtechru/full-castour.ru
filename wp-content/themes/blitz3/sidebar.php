<?php
/**
 * @package WordPress
 * @subpackage Vivee
 * @since Vivee 3.0
 */

?>
<div class="widget-area">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('post-sidebar') ) : ?><?php endif; ?>
</div>