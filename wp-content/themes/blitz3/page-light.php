<?php
/**
 * Template Name: Легкий шаблон
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Blitz
 * @since Blitz 3.0
 */
//get_header(); ?>
<?php include_once (TEMPLATEPATH . '/header-N.php'); ?>


<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="wrap">
		<div class="top-text">
			<?php while ( have_posts() ) : the_post(); ?>

	<h2 class="top-text__title"><?php the_title();?></h2>
	
	<?php the_content();?>
	
	
<?php endwhile; ?>
		</div>
	</div>


<?php// include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php'); ?>
<?php// include_once (TEMPLATEPATH . '/page-zagran-N-map.php'); ?>

<?php //get_footer(); ?>
<?php// include_once (TEMPLATEPATH . '/footer-N.php'); ?>
<?php include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>