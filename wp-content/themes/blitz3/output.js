// prepare the form when the DOM is ready
// http://malsup.com/jquery/form/#ajaxForm

$(document).ready(function() { 
    var options = { 
        target:        '#output',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
 
    }; 
 
    // bind form using 'ajaxForm' 
    $('#iForm').ajaxForm(options); 
}); 
 
// pre-submit callback 
function showRequest(formData, jqForm, options) { 
 
    var queryString = $.param(formData); 

    // alert('About to submit: \n\n' + queryString); 

    return true; 
} 

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 

    // alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
    //    '\n\nThe output div should have already been updated with the responseText.'); 
} 