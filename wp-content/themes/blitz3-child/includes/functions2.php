<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 22.02.15
 * Time: 13:26
 */

function ajax_login_init(){
    wp_register_script('ajax-login-script', get_template_directory_uri() . '/assets/js/ajax-login-script.js', array('jquery') );
    wp_enqueue_script('ajax-login-script');

    wp_localize_script( 'ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => '/lk/',
        'loadingmessage' => 'Проверка...'
    ));

    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
}

if (!is_user_logged_in()) {
    add_action('init', 'ajax_login_init');
}

function ajax_login(){

    check_ajax_referer( 'ajax-login-nonce', 'security' );

    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>'Неправильный договор или пароль.'));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>'Вход...'));
    }

    die();
}
/*-------------------------------------------------------------------------------------------------------------*/
/**
 * redirect Users to dogovor page
 */
//add_filter('login_redirect', 'dogovor_login_redirect', 10, 3 );

function dogovor_login_redirect( $url, $request, $user ){
    if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
        $url = home_url('/lk/');
        if( $user->has_cap( 'administrator' )) {
            $url = admin_url();
        }
        if($user->has_cap( 'editor' ) || $user->has_cap( 'manage_dogovors' )) {
            $url = admin_url('edit.php?post_type=dogovor');
        }
    }
    return $url;
}


add_filter( 'wp_insert_post_data' , 'modify_dogovor_post' , '99', 2 );

function modify_dogovor_post( $data , $postarr )
{
    $num=$postarr['wpcf']['dogovor_num'];
    if (isset($num)){
        if($data['post_type'] == 'dogovor') {
            $data['post_title'] = 'Договор № '.$num;
            $data['post_name'] = $num;
        }
        //dbg($_POST);
        $pass = check_user($num, $postarr['wpcf']['dogovor_name'], $postarr['wpcf']['dogovor_surname']);
        if ($pass){
            if (!update_post_meta($_POST['post_ID'], 'dogovor_password', $pass)) add_post_meta($_POST['post_ID'], 'dogovor_password', $pass, true);;
            $message = 'Ваше имя пользователя:'.$num."\r\nПароль:".$pass."\r\n".home_url();
            $email = $postarr['wpcf']['dogovor_e_mail'];
            wp_mail($email, 'castour Личный кабинет', $message);
            $message = "Зарегистрирован новый договор\r\nИмя пользователя:".$num."\r\nПароль:".$pass;
            global $user_email;
            get_currentuserinfo();
            wp_mail($user_email, 'castour Новый договор', $message);
        }
        if(isset($_POST['new_password'])){
            $pass = $_POST['new_password'];
            $user = get_user_by( 'login', $num );
            if ($user){
                wp_set_password( $pass, $user->id );
                if (!update_post_meta($_POST['post_ID'], 'dogovor_password', $pass)) add_post_meta($_POST['post_ID'], 'dogovor_password', $pass, true);;
                global $theme;
                $theme->dash_notice[] = 'Пароль для договора изменен.';
                $message = 'Ваше имя пользователя:'.$num."\r\nПароль:".$pass."\r\n".home_url();
                $email = $postarr['wpcf']['dogovor_e_mail'];
                wp_mail($email, 'castour Пароль для договора изменен', $message);
                $message = "Пароль для договора изменен\r\nИмя пользователя:".$num."\r\nПароль:".$pass;
                global $user_email;
                get_currentuserinfo();
                wp_mail($user_email, 'castour Пароль для договора '.$num.' изменен', $message);
            }
        }
    }
    return $data;
}

function check_user($user_name, $first_name, $sur_name){
    $password = wp_generate_password();
    $user_data = array(
        'ID' => '',
        'user_pass' => $password,
        'user_login' => $user_name,
        'user_nicename' => $user_name,
        'user_url' => '',
        'user_email' => $user_name.'@dummy.com',
        'display_name' => $user_name,
        'nickname' => $user_name,
        'first_name' => $first_name,
        'last_name' => $sur_name,
//        'user_registered' => '2010-05-15 05:55:55',
        'role' => get_option('default_role')
    );
    $user_id = wp_insert_user( $user_data );
    if(is_wp_error( $user_id )){
        return false;
    }
    return $password;
}

if (!current_user_can('administrator') && !current_user_can('editor') && !current_user_can('manage_dogovors')):
    show_admin_bar(false);
endif;
/*-------------------------------------------------------------------------*/

function get_dogovor_state($date_from, $date_to){
    date_default_timezone_set('Europe/Moscow');
    $current_date=time()-60*60;
    $date_ready=strtotime(date('d.m.Y', $date_to))+60*60*11;
    if ($current_date>$date_ready){
        return 5; //Паспорт готов!
    }
    $date_1=strtotime(date('d.m.Y', $date_from))+60*60*20;
    if ($current_date<=$date_1){
        return 0;//Подача документов
    }
    $date_2=strtotime(date('d.m.Y', $date_from))+60*60*24;
    if ($current_date<=$date_2){
        return 1;//Документы поданы
    }
    $date_x=$date_from+($date_to-$date_from)/100*80;
    $date_3=strtotime(date('d.m.Y', $date_x));
    if ($current_date<=$date_3){
        return 2;//Проверка поданных документов
    }
    $date_4=$date_x+60*60*11;
    if ($current_date<=$date_4){
        return 3;//Проверки пройдены
    }
    if ($current_date<=$date_ready){
        return 4;//Готовность паспорта
    }
    return 0;
}

add_action( 'iwto_passport_ready_event','iwto_send_passport_ready_notification', 10, 1);

function iwto_send_passport_ready_notification($email){
    if(empty($email)) return;
    $message = "Ваш паспорт готов.\n Забор паспорта – в день готовности после 15:00, в последующие дни в любое время.";
    wp_mail($email, 'Ваш паспорт готов.', $message);
}

add_action( 'admin_init', 'iwto_add_manager_role');
function iwto_add_manager_role() {
    global $theme;
    $role = get_role( 'dogovor_manager' );
    if(!isset($role)){
        $result = add_role(
            'dogovor_manager',
            'Менеджер договоров',
            array(
                'read'         => true,
                'delete_posts' => false,
                'manage_dogovors' => true,
            )
        );
        if ( null !== $result ) {
            $theme->dash_notice[] = 'Роль менеджера договоров создана!';
        }
    }
    $role = get_role('administrator');
    $role->add_cap('manage_dogovors');
    $role = get_role('editor');
    $role->add_cap('manage_dogovors');
    $dog_type = get_post_type_object( 'dogovor' );
    if(isset($dog_type)){
//        dbg($dog_type);
        //$dog_type->cap->
    }
}

add_action( 'admin_menu', 'iwto_remove_menu_items');
function iwto_remove_menu_items(){
    if(!current_user_can('administrator')){
        $menus = $GLOBALS[ 'menu' ];
        foreach($menus as $menu){
            if($menu[2]!='edit.php?post_type=dogovor'){
                remove_menu_page($menu[2]);
            }
        }
    }
}

/*------------------------------------------------------------------------*/

add_action( 'add_meta_boxes_dogovor', 'iwto_add_password_meta_box' );

function iwto_add_password_meta_box(){
    add_meta_box(
        'password_section',
        'Пароль для договора',
        'iwto_password_meta_box_callback',
        'dogovor',
        'side'
    );
}

function iwto_password_meta_box_callback($post){
    $pass = get_post_meta($post->ID, 'dogovor_password', true);
    if(!isset($pass)) return;
?>
    <p>Пароль: <?php echo $pass?></p>
    <p>
        <label for="new_password">Новый пароль</label>
        <input type="text" name="new_password" id="new_password">
    </p>
    <p><input type="submit" value="Изменить пароль"></p>

<?php
}


/*-------------------------------------------------------------------------*/
function dbg($obj){
    echo('<pre>');
    if (!isset($obj)){
        echo('Not set');
    } else {
//        var_dump($obj);
        print_r($obj);
    }
    echo('</pre>');
}

