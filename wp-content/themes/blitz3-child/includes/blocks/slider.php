<?php
$slider = $this->sanitize('Слайдер ' . $params);
$args = array('post_type' => $slider,
    posts_per_page => 999
);
$slides = get_posts($args);
?>
<div id="carousel-generic-<?php echo $slider; ?>" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php foreach ($slides as $k => $slide) { ?>
            <div class="item <?php if ($k == 0) { ?>active<?php } ?>">
                <a href="<?php echo $this->post_excerpt; ?>">
                    <img src="<?php echo $this->get_thumb_src($slide->ID); ?>" alt="<?php echo $slide->title; ?>"/>
                    <div class="carousel-caption">
                        <div class="carousel-caption-title">
                            <?php echo $slide->post_title; ?>
                        </div>
                        <div class="carousel-caption-content">
                            <?php echo $slide->post_content; ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
    <a class="left carousel-control" href="#carousel-generic-<?php echo $slider; ?>" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-generic-<?php echo $slider; ?>" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>