<?php
$prefix = $this->sanitize("Форма для ссылок");
?>
<div class="iwt-field-urls">
    <div class="iwt-field-urls-inner">
        <?php
        $vals = (($params['field_value']));
        $vals = json_decode(str_replace('&quot;', '"', $vals), true);
        foreach ($vals as $row) {
            $rows[$row['ordering']][] = $row;
        }
        ksort($rows);
        foreach ($rows as $r0) {
            foreach ($r0 as $r00) {
                $values[] = $r00;
            }
        }
        ?>
        <table>
            <thead>
                <tr>
                    <th><?php _e('Ссылка'); ?></th>
                    <th><?php _e('Описание'); ?></th>
                    <th><?php _e('Порядок отображения'); ?></th>
                    <th><?php _e('Действия'); ?></th>
                </tr>
            </thead>
            <tbody>    
                <?php
                foreach ($values as $row) {
                    if (!empty($row)) {
                        ?>
                        <tr>
                            <?php foreach ($row as $k => $v) { ?>
                                <td>
                                    <input type="text" name="<?php echo $k; ?>" value="<?php echo $v; ?>"/>
                                </td>
                            <?php } ?>
                            <td><a href="#" class="iwt-field-urls-remlink">[Удалить]</a></td>
                        </tr>
                    <?php }
                } ?>
                <tr>
                    <td><input type="text" name="link"/></td>
                    <td><input type="text" name="title"/></td>
                    <td><input type="text" name="ordering"/></td>
                    <td><a href="#" class="iwt-field-urls-remlink">[Удалить]</a></td>
                </tr>
            </tbody>            
        </table>
        <a href="#" class="button iwt-field-urls-addlink">Добавить ссылку</a>
    </div>
    <input type="hidden" name="<?php echo $params['field_name']; ?>" placeholder="<?php echo $params['value']['label']; ?>" value="<?php echo $params['field_value']; ?>" class="iwt-urls-json"/>
<?php wp_nonce_field('_' . $params['name'], '_' . $params['name']); ?>
</div>
