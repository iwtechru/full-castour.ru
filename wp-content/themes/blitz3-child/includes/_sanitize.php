<?php
if ($_POST['iwt-cpt']) {
    $pst = $_POST['iwt-cpt'];
    global $wpdb;
    foreach ($pst as $key => $value) {
        if ($this->sanitize($key) != $value) {
            $q = "UPDATE  `" . $wpdb->prefix . "posts` SET  `post_type` =  '" . $this->sanitize($value, true) . "' WHERE  `post_type` = '" . $this->sanitize($key) . "';";
            $this->update_option('sanitize', $key, $this->sanitize($value, true));
            $wpdb->query($q);
        }
    }
}
if ($q) {
    ?>
    <script type="text/javascript">
        window.location = window.location;
    </script>
    <?php
}
$san = $this->get_option('sanitize');
?>
<h1>Настройки URL для произвольных записей</h1>
<?php $cpts = ($this->registered_posts); ?>
<form name="iwt-cpt" method="post" action="">
    <table class="form-table">
        <tr>        
            <th>Название записи</th>
            <th>Текущее системное название</th>
            <th>Новый вариант названия</th>
        </tr>
        <?php foreach ($cpts as $key => $value) {
            ?>
            <tr>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo $this->sanitize($value['name']); ?></td>
                <td>
                    <input type="text" name="iwt-cpt[<?php echo ($value['name']); ?>]"
                           value="<?php echo $this->sanitize($value['name']); ?>"/>
                </td>
            </tr>
        <?php } ?>
    </table>
    <input type="submit" value="Сохранить" class="button button-primary"/>
</form>