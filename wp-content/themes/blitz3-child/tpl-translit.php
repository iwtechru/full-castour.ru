<?php
/*
  Template name: iwt Транслитерация
 */
get_header();
global $theme;
?>
<div class="row row-fluid">
    <div class="top-text ">
        

        <h1 class="top-text__title"><?php the_title(); ?></h1> 
    </div>
</div>
<?php
while (have_posts()) {
    the_post();
    ?>
    <div class="row row-fluid">
        <div class="iwt1-translit-text">
            <?php
            $content = explode('<hr />', get_the_content());
            echo $content[0];
            ?>
        </div>
        <div class="iwt1-translit-formwrapper clearfix">
            <input type="text" name="translitLastname" class="iwt1-translit-input iwt1-translit-lastname pull-left" id="iwt1-translit-lastname" placeholder="Фамилия">
            <input type="text" name="translitName" class="iwt1-translit-input iwt1-translit-name pull-left" id="iwt1-translit-name" placeholder="Имя">
            <button type="button" class="btn pull-right iwt1-translit-btn" id="js-translit">Транслитерировать</button>
        </div>
        <div class="iwt1-translit-wrapper">
            <div class="iwt1-translit-passport">
                <div class="iwt1-lastname-wrapper">
                    <div class="iwt1-lastname-rus" id="iwt1-lastname-rus">Фамилия</div>    
                    <div class="iwt1-lastname-translit" id="iwt1-lastname-translit">Familia</div>    
                </div>
                <div class="iwt1-name-wrapper">
                    <div class="iwt1-name-rus" id="iwt1-name-rus">Имя</div>    
                    <div class="iwt1-name-translit" id="iwt1-name-translit">Imia</div>    
                </div>
                <div class="iwt1-passpost-bottom" id="iwt1-passpost-bottom">р&lt;rusFamiliia&lt;&lt;Imia&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;
                    <br />XXXXXXXXXXRUSXXXXXXXXXXXXXXXXXXXXXXXX&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;15
                </div> 
            </div>
            <div class="iwt1-translit-rules hidden">
                <a href="#" class="iwt1-close-rules"></a>
            </div>
        </div>
        <button type="button" class="btn iwt1-show-rules" id="iwt1-show-rules">Новые правила транслитерации</button>
        <div class="iwt1-translit-text">
            <?php echo $content[1]; ?>
        </div>

    </div>

    </div>
    </div>
    <?php
}
get_footer();
