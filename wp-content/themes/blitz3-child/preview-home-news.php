<?php global $theme;?>
<div class="col-xs-4 iwt1-adv-col">
    <a href="<?php echo the_permalink();?>" class="iwt1-adv-link"><?php the_title();?></a>
    <div class="iwt1-adv-date"><?php echo $theme->get_the_time('%d/%m/%Y');?></div>
    <?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/', '$1...', get_the_excerpt()); ?>
</div> 