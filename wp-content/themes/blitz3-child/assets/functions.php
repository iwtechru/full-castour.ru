<?php
$url = explode('?', 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
$ID = url_to_postid($url[0]);
$files = scandir(get_stylesheet_directory());
$return = 0;
foreach ($files as $file) {
    if ($file == get_page_template_slug($ID)) {
        $return = 1;
    }
}
if ($return == 0 && $ID != 0) {
    return;
} else {
    
}

require('includes/functions_class.php');
$theme = new rht_theme('iwtech theme');
$theme->delete_option('required_plugins');
$plugins = array(
    'Attachments'
);
foreach ($plugins as $plugin) {
    $theme->update_option('required_plugins', $plugin, $plugin);
}
#$theme->disable_comments();
#$theme->update_option('menu_defaults', 'container',false);
##$theme->update_option('menu_defaults', 'container_class',false);
#$theme->update_option('menu_defaults', 'menu_class','nav nav-pills');
#$theme->delete_option('required_plugins');

function my_script1() {
    $skip[get_the_ID()] = 0;
    $skip = array(
        '1859' => 1,
        1857 => 1,
        1862 => 1,
        1694 => 1,
        1853 => 1
    );
    if ($skip[get_the_id()] == 0) {
        if (!is_admin()) {
            wp_deregister_script('jquery');
            wp_deregister_script('fancybox');
            wp_dequeue_script('jquery');
            wp_register_script('jquery', 'http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.0.min.js');
            wp_enqueue_script('jquery');
            wp_register_script('bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js');
            wp_enqueue_script('bootstrap-js');
            /* wp_register_style('bootstrap');
              wp_enqueue_style('bootsrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'); */
            //wp_register_script('jquery', get_bloginfo('template_url') . '/js/jquery.min-1.7.2.js', false, '');
            //wp_enqueue_script('jquery');
            //wp_enqueue_script('prettyphoto', get_bloginfo('template_url').'/js/jquery.prettyPhoto.js', array('jquery'), '2.5.6');
            //wp_enqueue_script('cufon-yui', get_bloginfo('template_url').'/js/cufon-yui.js', array('jquery'), '1.0.9');
            //wp_enqueue_script('trebuchet', get_bloginfo('template_url').'/js/Trebuchet_MS_400-Trebuchet_MS_700-Trebuchet_MS_italic_400.font.js', array('jquery'));
            //wp_enqueue_script('cycle', get_bloginfo('template_url').'/js/jquery.cycle.all.min.js', array('jquery'));
            //wp_enqueue_script('dropdownmenu', get_bloginfo('template_url') . '/js/dropdown.js', array('jquery'));
        }
    } else {
        global $wp_styles;
        $a = array('style1', 'style2');
        foreach ($a as $b) {
            wp_dequeue_style($b);
            wp_deregister_style($b);
        }
        //print_r($wp_styles);
    }
}

#remove_action('wp_head', 'mfbfw_init');
add_action('wp_enqueue_scripts', 'my_script1');

/* этот код убрать при слиянии */

function my_theme_load_resources() {
    $theme_uri = get_template_directory_uri();

    wp_register_style('my_theme_style', "http://localhost/castour/wp-content/themes/blitz3-child/assets/jsbootstrap.min.js", false, '0.1');
    wp_enqueue_style('my_theme_style');
    wp_register_style('my_theme_style2', "http://localhost/castour/wp-content/themes/blitz3/styles-N.css ", false, '0.1');
    wp_enqueue_style('my_theme_style2');

    /* wp_register_script('my_theme_functions', $theme_uri.'/js/my_theme_functions.js', array('jquery'), '0.1');
      wp_enqueue_script('my_theme_functions'); */
}

//add_action('wp_enqueue_scripts', 'my_theme_load_resources');

/* конец кода,который нужно убрать */

function altair_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <div <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

        <div class="comment-wrap">
            <div class="avatar-wrapper">
                <div class="altair-comment-avatar">

                </div>                
            </div>
            <div class="autor-comment">
                <?php echo get_comment_author_link() ?>
            </div>
            <div class="date-comments">
                <?php echo get_comment_date('j F Y'); ?>
            </div>
            <div class="comment-text-altair">                
                <?php comment_text() ?>
            </div>
        </div>



        <?php
    }

    function true_russian_date_forms($the_date = '') {
        if (substr_count($the_date, '---') > 0) {
            return str_replace('---', '', $the_date);
        }
        $replacements = array(
            "Январь" => "января",
            "Февраль" => "февраля",
            "Март" => "марта",
            "Апрель" => "апреля",
            "Май" => "мая",
            "Июнь" => "июня",
            "Июль" => "июля",
            "Август" => "августа",
            "Сентябрь" => "сентября",
            "Октябрь" => "октября",
            "Ноябрь" => "ноября",
            "Декабрь" => "декабря"
        );
        return strtr($the_date, $replacements);
    }

    add_filter('the_time', 'true_russian_date_forms');
    add_filter('get_the_time', 'true_russian_date_forms');
    add_filter('the_date', 'true_russian_date_forms');
    add_filter('get_the_date', 'true_russian_date_forms');
    add_filter('the_modified_time', 'true_russian_date_forms');
    add_filter('get_the_modified_date', 'true_russian_date_forms');
    add_filter('get_post_time', 'true_russian_date_forms');
    add_filter('get_comment_date', 'true_russian_date_forms');
    