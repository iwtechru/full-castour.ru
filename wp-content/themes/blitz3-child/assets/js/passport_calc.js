if (jQuery('.iwt_calc_controls').length) {
    jQuery('.iwt_calc_controls li').click(function (e) {
        e.preventDefault();
        $this = jQuery(this);
        $this.addClass('iwt-ticked');
        $this.siblings().removeClass('iwt-ticked');
        var variant;
        variant = '';
        jQuery('.iwt_calc_controls .iwt-ticked').each(function () {
            $this = jQuery(this);
            variant = variant + '_' + ($this.attr('data-variant'));
        });        
        jQuery('.iwt_variant').hide();
        jQuery('.iwt_variant' + variant).show();
        jQuery('.iwt_calc_docs_required .iwt_variant').hide();
        jQuery('.iwt_calc_docs_required  .iwt_variant' + variant).show();
        console.log('.iwt_variant'+variant);

    });
    jQuery('.iwt_calc_controls ul').each(function () {
        jQuery(this).find('li:first').click();
    });
}