jQuery(document).ready(function () {
    jQuery('.iwt_o_zpnew').find('h2').not('.top-text__title').each(function () {
        $this = jQuery(this);
        $this.css({marginBottom: 40});
    });
    jQuery("#pop").on("click", function (e) {
        jQuery('#imagepreview').attr('src', jQuery('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
        jQuery('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
        e.preventDefault();
    });
    if (jQuery('.top-text .iwt_blanki_page_price_and_terms').length === 0) {
        if (jQuery('.error404,.page-template-tpl-lk').length) {
            jQuery('.btn404').click(function () {
                window.location = '/';
            });
        } else {
            jQuery('.top-text:first').prepend(jQuery('.iwt_blanki_page_price_and_terms'));
        }
    }
    $('.iwt1-submenu').on('mouseleave', function () {
        iwt_init_active();
    });
    $('.iwt1-menu-wrapper').on('mouseleave', function () {
        iwt_init_active();
    });
    iwt_init_active();
    jQuery('.comment-form').removeAttr('novalidate');
    jQuery('.iwt-spoiler-title').click(function () {
        jQuery(this).closest('.iwt-spoiler').toggleClass('iwt-spoiler-closed');
    });
    jQuery('.iwt-inner-su-clearfix').show().slideToggle();
    jQuery('.iwt-inner-su-spoiler-title').click(function () {
        $this = jQuery(this);
        $this.closest('.iwt-inner-su-spoiler-closed').toggleClass('iwt-inner-su-spoiler-open');
        $this.siblings('.iwt-inner-su-clearfix').slideToggle();
    });
    var comp = new RegExp(location.host);

    jQuery('a').each(function () {
        if (comp.test($(this).attr('href'))) {
        }
        else {
            href = jQuery(this).attr('href');
            if (href == '#') {
                return;
            }
            if (/^(?:[a-z]+:)?\/\//i.test(href)) {
                $(this).attr('target', '_blank');
            }
        }
    });
    jQuery('.iwt-btn').click(function () {
        if ($(this).hasClass('iwt-newtab')) {
          loc=jQuery(this).find('span').attr('data-href');
          jQuery('body').append('<form id="iwtsubmitme" method="post" action="'+loc+'" target="_blank"></form>');
          jQuery('#iwtsubmitme').submit();
        } else {
            if (jQuery(this).find('a').length) {
                window.location = jQuery(this).find('a').attr('href');
            } else {
                window.location = jQuery(this).find('span').attr('data-href');
            }
        }
    });
});
function iwt_init_active() {
    jQuery('.iwt1-parent' + jQuery('.iwt1-submenu a.current').closest('div').attr('data-source')).addClass('active');
    setTimeout(function () {
        jQuery('.iwt1-parent' + jQuery('.iwt1-submenu a.current').closest('div').attr('data-source')).addClass('active');
    }, 10);
}