/**
 * Created by Oleg0s on 23.02.15.
 */

jQuery(document).ready(function($) {

    // Perform AJAX login on form submit
    $('form#auth').on('submit', function(e){
        $('form#auth p.status').show().text(ajax_login_object.loadingmessage);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#auth #username').val(),
                'password': $('form#auth #password').val(),
                'security': $('form#auth #security').val() },
            success: function(data){
                $('form#auth p.status').text(data.message);
                if (data.loggedin == true){
                    document.location.href = ajax_login_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });

});
