<?php
/*
  Template name: iwt Оформление
 */
get_header();
?>
<?php
global $theme;
while (have_posts()) {
    the_post();
    ?><div class="wrap iwt_oformlenie row-fluid">
        <h1 class="top-text__title"><?php the_title(); ?></h1>
        <?php get_template_part('block', 'extratext'); ?>
    </div>
    <div class="row-fluid iwt_blanki_block-row iwt_blanki_block iwt_noborder">
        <ul class="">
            <?php
            $filelink = $theme->field('Текст со ссылкой под заголовком');
            if (strlen($filelink) > 0) {
                $filelink_ = explode('|', $filelink);
                ?>
                <li class="iwt_blanki_block_li iwt_blanki_block_li_a iwt_blanki_block_li_<?php echo $theme->sanitize($filelink); ?>"><a href="<?php echo $filelink_[1]; ?>"><?php echo $filelink_[0]; ?></a></li>
                <?php
            } else {
                $files = $theme->attachments('Прикрепленные файлы под заголовком');
                if (is_array($files)) {
                    ?>
                    <?php foreach ($files as $file) { ?>
                        <li class="iwt_blanki_block_li iwt_blanki_block_li_<?php
                        $san = $theme->sanitize($file['fields']['title']);
                        echo $san;
                        ?> blanki_block_li_<?php
                            $sant = explode('-', $san);
                            echo preg_replace('/\d/', '', $sant[0]);
                            ?>"><a href="<?php echo $theme->get_thumb_src($file['id']); ?>" target="_blank"><?php echo $file['fields']['title']; ?></a></li>
                        <?php } ?>     
                        <?php
                    }
                }
                ?>
        </ul>
    </div>
    <div class="wrap row-fluid">
        <?php
        $content = explode('<hr />', get_the_content());
        echo $content[0];
        ?></div><div class="iwt_dopolnitelno iwt_dopolnitelno_oformlenie">
        <div class="row row-fluid">
            <?php foreach ($theme->menu_items('На странице ' . get_the_title()) as $item) { //print_r($item);
                ?>
                &nbsp;&nbsp;<a class="<?php if ($_SERVER['REQUEST_URI'] == $item->url) { ?>current-url<?php }
        if ($item->classes[1]) {
                    ?> current-url <?php }
                   ?>"
                               href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>&nbsp;&nbsp;
                               <?php
                           } if (2 == 1) {
                               $files = $theme->attachments('Документы');
                               foreach ($files as $file) {
                                   ?>
                    &nbsp;&nbsp;<a href="<?php echo $theme->get_thumb_src($file['id']); ?>" target="_blank"><?php echo $file['fields']['title']; ?></a>&nbsp;&nbsp;
                    <?php
                }
            }
            ?>
        </div>                
    </div><div class="container-fluid iwt_dopolnitelno_calc">
        <div class="iwt_dopolnitelno_calc-wrap">
            <h3 class="top-text__title">Расчет стоимости загранпаспорта</h3>
            <div class="row-fluid">
                <div class="col-xs-3 col-grey">
                    <?php
                    $controls = array(
                        'Тип загранпаспорта' => array('Биометрический', 'Старого образца'),
                        'Возраст получателя' => array('до 14 лет', 'от 14 до 17 лет', '18 лет и старше'),
                        'Место регистрации' => array('Москва', "Московская область", 'Другие регионы РФ')
                    );
                    $variants_ = iwt_cartesian($controls);
                    foreach ($variants_ as $k => $v) {
                        $string = '';
                        $class = 'iwt_variant';
                        $id = 'iwt_variant';
                        foreach ($v as $v1) {
                            $string.=$v1 . ' ';
                            $class.=' iwt_variant' . $theme->sanitize($v1);
                            $id.='_' . $theme->sanitize($v1);
                        }
                        $variant[] = array('value' => $string, 'class' => $class, 'id' => $id);
                    }
                    iwt_passport_set_variants($variant);
                    ?>
                    <div class="iwt_calc_controls">
    <?php foreach ($controls as $key => $value) { ?>
                            <h4><?php echo $key; ?></h4>
                            <ul class="iwt_calc_<?php echo $theme->sanitize($key); ?>">
                                <?php foreach ($value as $v) { ?>
                                    <li data-variant="<?php echo str_replace('obrazca', 'obraztsa', $theme->sanitize($v)); ?>"><a href=""><?php echo $v; ?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
    <?php } ?>
                    </div>
                </div>
                <div class="col-xs-8 col-xs-values">
                    <h4>Стоимость изготовления паспорта <span>(меняется в зависимости от срочности)</span></h4>
                    <div class="iwt_calc_values">
                        <?php
                        $intervals = iwt_passport_get_intervals();
                        foreach ($intervals as $key => $row) {
                            $cols = array();

                            $row_ = array();
                            $row_ = iwt_partition($row, 2);
                            // print_r($row_);
                            $countrows = 0;
                            //print_r($row_);
                            foreach ($row_ as $col => $r1) {
                                $variant_here = str_replace('obrazca', 'obraztsa', $r1[0]['variant']);
                                ?>
                                <div class="iwt_variant <?php echo $variant_here; ?> col-xs-5 <?php if ($col == 0) { ?> <?php } else { ?> col-xs-offset-2 <?php } ?>">
                                    <?php
                                    foreach ($r1 as $r) {

                                        //print_r($r);
                                        ?>
                                        <div class="row row-days row-days_<?php echo $row[0]['color']; ?> ">
                                            <div class="iwt_calc_days iwt_calc_days col-xs-9 row-days_<?php echo $r['color']; ?>">
                                                <?php echo $r['days']; ?>
                                                <?php
                                                $l = substr($r['days'], -1);
                                                $suffix = ' дней';
                                                if (!strpbrk('-', $r['days'])) {
                                                    if (in_array($l, array(2, 3, 4))) {
                                                        $suffix = 'дня';
                                                    }
                                                    if ($l == 1) {
                                                        $suffix = 'день';
                                                    }
                                                }
                                                echo $suffix;
                                                ?>
                                                <span class="iwt_dots"><?php
                                                    for ($c = 0; $c <= 255; $c++) {
                                                        ?>.<?php } ?></span>
                                            </div>
                                            <div class="iwt_calc_amount col-xs-3">
                <?php ?>
                                                <span class="iwt_variant <?php echo $variant_here; ?>"><?php echo $r['price']; ?> р.</span>
                                            </div>
                                        </div>
                                <?php } ?>
                                </div>
                                <?php if ($col == 1) { ?>
                                    <div class="clearfix"></div>
                                    <?php
                                }
                            }
                            if (2 == 1) {
                                foreach ($row as $r) {
                                    $countrows++;
                                    $variant_here = str_replace('obrazca', 'obraztsa', $r['variant']);
                                    ?>
                                    <div class="iwt_variant <?php echo $variant_here ?> col-xs-5 <?php if ($countrows == 2) { ?> col-xs-offset-2 <?php } ?>">
                                        <div class="row row-days row-days_<?php echo $row[0]['color']; ?>">
                                            <div class="iwt_calc_days iwt_calc_days col-xs-9 row-days_<?php echo $r['color']; ?>">
                                                <?php echo $r['days']; ?><span class="iwt_dots"><?php
                                                    for ($c = 0; $c <= 255; $c++) {
                                                        ?>.<?php } ?></span>
                                            </div>
                                            <div class="iwt_calc_amount col-xs-3">
                <?php ?>
                                                <span class="iwt_variant <?php echo $variant_here; ?>"><?php echo $r['price']; ?> р.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($countrows == 2) { ?><div class="clearfix"></div><?php
                                        $countrows = 0;
                                    }
                                }
                            }
                        }
                        ?><div class="clearfix"></div>
                        <?php
                        $countrows = 0;
                        ?>
                        <div class="clearfix"></div><?php ?>
                    </div>
                    <div class="iwt_calc_notes">Сроки указаны в рабочих днях. Цены актуальны на <?php echo date('d.m.Y'); ?>
    <?php //echo $theme->field('Примечание под таблицей черного цвета', array('notsingle' => false));  ?><br/><span class="iwt_redtext"><?php echo $theme->field('Примечание под таблицей красного цвета', array('notsingle' => false)); ?></span></div>
                    <div class="iwt_calc_docs_required">
                        <div class="col-xs-5"><h4>Необходимые документы:</h4>
                            <?php
                            $intervals = iwt_passport_get_intervals(get_the_id());
                            foreach ($intervals as $key => $v1) {
                                //    $docs_ = $theme->field('Необходимые документы через точку с запятой ' . $v['value'], array('notsingle' => false));
                                ?><ul style="display:none;" class="iwt_variant <?php echo str_replace('obrazca', 'obraztsa', $key); ?>"><?php
                                $countrows2 = 0;
                                foreach ($v1 as $row) {
                                    $countrows2++;
                                    if ($countrows2 == 1) {
                                        $docs_ = explode(';', $row['documents']);
                                        foreach ($docs_ as $doc) {
                                            ?><li><?php echo $doc; ?></li>
                                                <?php
                                            }
                                        }
                                    }
                                    ?></ul><?php
                                }
                                ?>
                        </div>
                        <div class="col-xs-5 col-xs-offset-2 iwt_docs_included_block"><h4>Что включено в стоимость:</h4>
                            <?php
                            $docs_ = $theme->field('Что включено - через точку с запятой', array('notsingle' => false));
                            $docs = explode(';', $docs_);
                            ?>
                            <ul class="iwt_docs_included">
                                <?php foreach ($docs as $doc) { ?>
                                    <li><?php echo $doc; ?></li>
    <?php } ?>
                            </ul>
                        </div>
                        <div class="clearfix"></div><br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="iwt_oformlenie_car">
        <div class="row">
            <?php
            $cartext = $theme->field('Текст рядом с машинкой');
            if (!strlen($cartext)) {
                ?>Если у Вас нет возможности выехать к нам в офис, то к Вам приедет наш сотрудник. Стоимость услуги 1000 рублей.<?php } ?>
        </div>
    </div>
    <div class="row row-fluid">
        <?php
        if (2 == 1) {
            $fat = 'Документы';
            $files = $theme->attachments($fat);
            if (is_array($files)) {
                require('block-blanki-ul.php');
            }
        }
        ?>
    </div>
    <div class="row row-fluid">
        <?php
        $arr = array('Обратите внимание', 'Возможные причины отказа', "Преимущества &laquo;Кастур&raquo;");
        ?>
        <div class="bs-example">
            <ul class="nav nav-tabs iwt_kontakti_map iwt_oformlenie_tabs">
                <?php foreach ($arr as $k => $v) { ?>
                    <li class="<?php if ($k == 0) { ?>active<?php } ?> tab_<?php echo $theme->sanitize($v); ?>"><a data-toggle="tab" href="#<?php echo $theme->sanitize($v); ?>"><?php echo $v; ?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content iwt_oformlenie_tab_content">
                    <?php foreach ($arr as $k => $v) { ?>
                    <div id="<?php echo $theme->sanitize($v); ?>" class="tab-pane fade <?php if ($k == 0) { ?> in active<?php } ?>">
                    <?php echo $theme->field($v, array('notsingle' => false, 'variant' => 'textarea')); ?>
                    </div>
    <?php } ?>
            </div>
        </div>
        <div class="iwt_clear"></div>
    </div>
    </div>
    <?php
}
get_footer();
