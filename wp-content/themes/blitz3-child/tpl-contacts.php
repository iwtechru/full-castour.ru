<?php
/*
  Template name: iwt Контакты
 */
get_header();
global $theme;
?>
<div class="row row-fluid">
    <div class="top-text iwt_kontakti">
        <?php while (have_posts()) : the_post(); ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1>
            <?php get_template_part('block','extratext');?>
            <div class="container-fluid iwt_kontakti_data">
                <div class="row">
                    <div class="col-xs-6 iwt_kontakti_flag"><?php echo $theme->field('Текст справа от флага', array('notsingle' => false, 'variant' => 'textarea')); ?></div>
                    <div class="col-xs-6 iwt_kontakti_phone"><?php echo $theme->field('Текст справа от трубки', array('notsingle' => false, 'variant' => 'textarea')); ?></div>
                </div>
                <div class="row">
                    <div class="col-xs-6 iwt_kontakti_clock"><?php echo $theme->field('Текст справа от часов', array('notsingle' => false, 'variant' => 'textarea')); ?></div>
                    <div class="col-xs-6 iwt_kontakti_mail"><?php echo $theme->field('Текст справа от конверта', array('notsingle' => false, 'variant' => 'textarea')); ?></div>
                </div>
            </div>
            <?php
            $content = explode('<hr />', get_the_content());
            echo $content[0];
            $arr = array('Схема', 'Панорама');
            ?>
            <div class="bs-example">
                <ul class="nav nav-tabs iwt_kontakti_map">
                    <?php foreach ($arr as $k => $v) { ?>
                        <li class="<?php if ($k == 0) { ?>active<?php } ?>"><a data-toggle="tab" href="#<?php echo $theme->sanitize($v); ?>"><?php echo $v; ?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <?php foreach ($arr as $k => $v) { ?>
                        <div id="<?php echo $theme->sanitize($v); ?>" class="tab-pane fade <?php if ($k == 0) { ?> in active<?php } ?>">
                            <?php echo $theme->field($v, array('notsingle' => false, 'variant' => 'textarea')); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="iwt_clear"></div>
            <?php echo $content[1];?>
        <?php endwhile; ?>
    </div></div>
<?php
//require_once(TEMPLATEPATH . '/footer-N-light.php');
get_footer();
