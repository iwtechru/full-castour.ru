<?php //get_header();         ?>
<?php
//include_once (TEMPLATEPATH . '/header-N.php'); 
get_header();
?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="row row-fluid">
    <div>
        <div class="top-text">
            <h1 class="top-text__title">Частые вопросы</h1>
            <?php get_template_part('block','extratext');?>
        </div>
        <?php
        global $query_string;
        query_posts($query_string . '&order=ASC&meta_key=wpcf-position-status-2&meta_value=1');
        ?><div class="iwt-spoilers">
        <?php
        $args = array('post_type' => 'faq', 'order' => 'ASC', 'orderby' => 'meta_value_num', 'posts_per_page' => 1000, 'meta_key' => 'wpcf-ordernum');
        $loop = new WP_Query($args);
        while ($loop->have_posts()) : $loop->the_post();
            ?>
            <div class="top-text iwt-spoiler-block iwt-spoiler-block-closed" >
                <div class="iwt-spoiler iwt-spoiler-style-fancy iwt-spoiler-icon-plus iwt-spoiler-closed">
                    <div class="iwt-spoiler-title">
                        <span class="iwt-spoiler-icon">
                        </span><h3 class="top-text__title"><?php the_title(); ?></h3>
                    </div>
                    <div class="iwt-spoiler-content iwt-clearfix"><?php $content = do_shortcode(get_the_content());
            echo preg_replace('/class=\"[^\"]*(su\-)/', 'class="iwt-inner-$1', $content); ?></div>
                </div>
            </div>
            <?php endwhile; ?></div>
        <div style="display:none">
        <?php //echo do_shortcode('[su_spoiler title="1" style="fancy"]2[/su_spoiler]')  ?></div>


        <?php // wp_pagenavi();  ?>


<?php wp_reset_query(); ?>
        <p><br/></p>
    </div>
</div>


<?php // include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php');  ?>
<?php //include_once (TEMPLATEPATH . '/page-zagran-N-map.php');  ?>

<?php get_footer(); ?>
<?php
//include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>