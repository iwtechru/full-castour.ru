jQuery(document).ready(function () {
    jQuery('.wp-submenu .current').closest('.wp-has-submenu').removeClass('wp-not-current-submenu').addClass('wp-has-current-submenu');
});
jQuery(document).ready(function () {
    upload_init_thumbs();
    jQuery('.iwt_image_remove').click(function () {
        $this = jQuery(this);
        $inside = $this.closest('.inside');

        $inside.find('label input[type="hidden"]').val('');
        $inside.find('label img').remove();
    });
    jQuery('.upload_image_button').click(function () {
        formfield = jQuery(this).closest('.inside').find('.upload_image').attr('name');
        jQuery('.putmediahere').removeClass('putmediahere');
        jQuery('[name="' + formfield + '"]').addClass('putmediahere');
        tb_show('', 'media-upload.php?amp;TB_iframe=true');
        return false;
    });
    iwt_urls_init();
    jQuery('.iwt-field-urls-addlink').click(function (e) {
        $this = jQuery(this);
        e.preventDefault();
        $table = $this.closest('.inside').find('table');
        $table.append($table.find('tr:last').clone());
        iwt_urls_init();
    }
    );
  /*  window.send_to_editor = function (html) {
        img = jQuery(html).find('img').eq(0);
        imgurl = img.eq(0).attr('src');
        attachmentID = img.attr('class').match(/wp-image-([0-9]+)/);//.replace(/^[^\,]*\,/,'');


        //imgurl = jQuery(html).attr('src');
        jQuery('.putmediahere').val(attachmentID[1]).before('<img src="' + imgurl + '"/>');
        tb_remove();
        upload_init_thumbs();
    }*/

});
function upload_init_thumbs() {
    jQuery('.upload_image').each(function () {
        $this = jQuery(this);
        $this.closest('.inside').find('.uploaded-img').remove();
        $this.closest('inside').prepend('<img src="' + $this.val() + '" alt="" class="uploaded-img"/>');
    });
}
function iwt_urls_init() {
    jQuery('.iwt-field-urls-remlink').unbind();
    jQuery('.iwt-field-urls-remlink').click(function (e) {
        e.preventDefault();
    });
    jQuery('.iwt-field-urls-remlink').not('table .iwt-field-urls-remlink:last').bind('click', (function (e) {
        e.preventDefault();
        jQuery(this).closest('tr').remove();
    }));
    jQuery('.iwt-field-urls-inner tr input').change(function () {
        $urls = jQuery(this).closest('.iwt-field-urls');
        countrows = 0;
        $form = {};
        jQuery($urls).find('table tbody tr').each(function () {
            $form[countrows] = {};
            $thisrow = jQuery(this);
            $thisrow.find('input').each(function () {
                $this = jQuery(this);
                $name = ($this.attr('name'));
                if ($this.val().length > 0) {
                    $form[countrows][$name] = $this.val();
                }
            });
            countrows++;
        });
        $json = JSON.stringify($form);
        $urls.find('input.iwt-urls-json').val($json);
    });
}
