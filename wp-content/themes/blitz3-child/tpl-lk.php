<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 */
/*
Template Name: lk
*/
?>
<?php
global $theme;
if ( is_user_logged_in() ) {
    $page = get_page_by_path($current_user->user_login, OBJECT, 'dogovor');
    if($page){
        wp_redirect(get_permalink($page->ID));
        exit;
    }
}
get_header();
?>

<!--    <div class="wrap wrap-nopadding"> -->
    <div class="row row-fluid">
        <div class="top-text">
            <div class="row row-fluid"></div>
            <?php
            while (have_posts()) {
                the_post();
                ?>
                <h1 class="top-text__title"><?php the_title(); ?></h1>
                <?php get_template_part('block','extratext');?>
                <?php
                if ( is_user_logged_in() ) {
                    echo 'У Вас нет договора.';
                } else {
                    ?>
                    Необходима авторизация.
                    <button type = "button" class = "btn iwt1-orange-btn pull-right" data-toggle = "modal" data-target = "#auth">Войти</button>
                    <?php
                }
                ?>
            <?php } ?>
        </div>
    </div>

<?php
get_footer();
