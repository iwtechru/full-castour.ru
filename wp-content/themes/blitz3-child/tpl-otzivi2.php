<?php
/*
  Template name: iwt Отзывы
 */
 
get_header();
global $theme;

?>
 
    <div id="primary" class="site-content">
        <div id="content" role="main">
 
            <?php comments_template('/tpl_comments.php') ?>
 
        </div><!-- #content -->
    </div><!-- #primary -->
<?php get_footer();