<?php
/*
  Template name: iwt О компании
 */
//require_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?>
<div class="row row-fluid">
    <div class="top-text iwt_o_kompanii">
        <?php while (have_posts()) : the_post(); ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1>   
            <div class="iwt_o_kompanii_redborder">
                <div class="iwt_o_kompanii_redborder_img">
                    <a href="<?php echo $theme->get_thumb_src(get_the_ID()); ?>"  class="fancybox">
                        <?php echo $theme->get_thumb(get_the_ID(), 100); ?>
                    </a>
                    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                    
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo $theme->get_thumb_src(); ?>" id="imagepreview" class="img-responsive" >
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="iwt_o_kompanii_redborder_content">
                    <?php echo $theme->field('Текст справа от сертификата', array('notsingle' => false, 'variant' => 'textarea')); ?>
                </div>
                <div class="iwt_clear"></div>
            </div>
            <?php
            $pid = get_the_ID();
            $content = explode('<hr />', do_shortcode(get_the_content()));
            ?><div class="iwt_company_content"><?php echo $content[0]; ?></div><?php
            $fat = 'Преимущества нашей компании перед конкурентами';
            $files = $theme->attachments($fat);
            ?>
        <?php endwhile; ?>
        </div>
    </div>
    <div class="iwt_o_kompanii_plus">
        <div class="iwt_wrap2">
            <h2><?php echo $fat; ?></h2>
            <?php
            $countblocks = 0;
            for ($i = 0; $i < 1; $i++) {
                foreach ($files as $key => $file) {
                    $countblocks++;
                    ?>
                    <div class="iwt_o_kompanii_plus_block iwt_o_kompanii_plus_block<?php
                    echo $countblocks;
                    if ($key >= (count($files) - 5)) {
                        ?> iwt_o_kompanii_plus_block_lastrow <?php } ?>">
                        <div class="iwt_o_kompanii_plus_block_img" style="background-image:url('<?php echo $theme->get_thumb_src($file['id']); ?>');"></div>
                        <div class="iwt_o_kompanii_plus_block_content"><?php echo $file['fields']['title']; ?></div>
                    </div>
                    <?php
                    if ($countblocks == 5) {
                        $countblocks = 0;
                    }
                }
            }
            ?>
            <div class="iwt_clear"></div>
        </div>
    </div>
    <div class="row row-fluid">
        <div class="top-text iwt_o_kompanii iwt_company_content">
            <?php echo $content[1]; ?>
            <div class="iwt_o_kompanii_redborder_nb">
                <div class="iwt_o_kompanii_redborder_nb_em">!</div>
                <div class="iwt_o_kompanii_redborder_nb_content">
                    <?php echo $theme->field('Текст справа от восклицательного знака', array('pid' => $pid, 'notsingle' => false, 'variant' => 'textarea')); ?>
                </div><div class="iwt_clear"></div></div>
                <?php echo $content[2]; ?>
        </div>
    </div>
    <?php
//require_once(TEMPLATEPATH . '/footer-N-light.php');
    get_footer();
    