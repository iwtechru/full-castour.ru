<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.  The actual display of comments is
 * handled by a callback to ts_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Blackbox
 * @since Blackbox 1.0
 */
require_once( ABSPATH . 'wp-admin/includes/image.php' );
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once( ABSPATH . 'wp-admin/includes/media.php' );
global $theme;
?>


<div class="comment-wrapper">

    <h1 class="head-comments">Отзывы клиентов</h1>
    <div class="sub-head-comments">
        <div class="count-coments">
            <?php //comments_number('нет отзывов', 'Всего отзывов: <span class="count-com">1</span>', 'Всего отзывов: <span class="count-com">%</span>'); ?>
        </div>
        <div class="button-coments">
            Оставить отзыв
        </div>
    </div>
    <div class="main-list-comments">
        <div class="commentlist" id="loop">
            <?php
            $args = array(
                "style" => "div",
                "callback" => "altair_comment",
                'reverse_top_level' =>true
                    //"per_page" => 2 
            );

            wp_list_comments($args);
            ?>


        </div>        

    </div>    
</div>

<?php if ($_GET['c'] == 'y') { ?>
    <div id="cok" class="alert alert-success" role="alert">
        <div class="row row-fluid">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only"></span>
            <?php
            $commentok = $theme->sitevar('Комментарий - сообщение о получении комментария');
            if (!strlen($commentok)) {
                ?>Комментарий принят! Он будет размещен после проверки администрацией сайта<?php
            } else {
                echo $commentok;
            }
            ?>
        </div></div>
<?php } ?>
<div class="iwt-rose-wrap">
    <div class="row row-fluid">
        <div class="add-com-form-wrapper">

            <div class="add-com-form" id="form-com">
                <div class="file-maska">Прикрепить файл</div>
                <div class="file-name"></div>
                <?php
                /* add_filter('comment_form_default_fields', 'remove_url_from_commentform');

                  function remove_url_from_commentform($arg) {
                  $arg['url'] = '';
                  return $arg;
                  } */

                $fields = array(
                    'author' => '<div class="comment-form-author">' . ( $req ? '' : '' ) .
                    '<input id="author" name="author" placeholder="Ваше имя" type="text" value="" /></div>',
                    'comment_field' => '<div class="comment-form-comment"><textarea placeholder="Отзыв" id="comment" name="comment" aria-describedby="form-allowed-tags" aria-required="true"></textarea></div>'
                );


                $comments_args = array(
                    'fields' => apply_filters('comment_form_default_fields', $fields),
                    'comment_notes_before' => '',
                    'comment_notes_after' => '',
                    'comment_field' => '<div class="comment-form-comment"><textarea placeholder="Отзыв" id="comment" name="comment" aria-describedby="form-allowed-tags" aria-required="true" required></textarea></div>',
                    'label_submit' => 'Добавить отзыв',
                    'title_reply' => 'Оставить отзыв'
                );

                comment_form($comments_args);
                ?>                
            </div>
        </div>
    </div>
</div>

