<?php
/*
  Template name: iwt О нас пишут
 */
//require_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="row row-fluid">
    <div class="top-text iwt_onaspishut_page">
        <?php while (have_posts()) : the_post(); ?>

            <h1 class="top-text__title"><?php the_title(); ?></h1>            
            <?php get_template_part('block', 'extratext'); ?>
            <div class="iwt_redblock">
                <h3><?php echo $theme->field("Заголовок рыжего блока"); ?></h3>
                <span><?php echo $theme->field('Текст рыжего блока'); ?></span>
            </div>
            <div class="row row-fluid iwt_content0">
                <?php
                $content = explode('<hr />', get_the_content());
                echo $content[0];
                ?></div>
            <div class="iwt_onaspishut">
                <div class="iwt_onaspishet_v_border"></div>
                <div class="row row-fluid">
                    <?php
                    $blocks = 1;
                    $countblocks++;
                    $files = $theme->attachments('Кто о нас пишет');
                    foreach ($files as $file) {
                        ?>
                        <div class="col-xs-6 col-sm-6 iwt_onaspishet iwt_on_<?php echo $theme->sanitize(get_the_title($file['id'])); ?> iwt_onaspishet_<?php echo $file['id']; ?> iwt_onaspishet<?php
                        $blocks = $blocks * (-1);
                        $countblocks++;
                        echo str_replace('-', '_', $blocks);
                        ?>">
                            <div class="iwt_onaspishet_img iwt_onaspishet_img<?php echo $theme->sanitize(get_the_title($file['id'])); ?>" style="background-image:url(<?php echo $theme->get_thumb_src($file['id']); ?>);"></div>
                            <p><?php echo $file['fields']['caption']; ?></p>
                            <a href="<?php echo $file['fields']['title']; ?>" target="_blank">Подробнее</a>                        
                        </div>
                        <?php if ($blocks == 1) { ?>
                        </div><div class="row row-fluid">
                        <?php } ?>
                        <?php if ($blocks == 1 && $countblocks < count($files)) { ?>
                            <div class="iwt_onaspishet_h_border"></div><?php } ?>
                    <?php } ?>
                    <div class="iwt_clear"></div>
                </div></div>
            <div class="iwt_onaspishut_2key"><?php echo $content[1]; ?></div>
        <?php endwhile; ?>
    </div>
</div>
<?php
//require_once(TEMPLATEPATH . '/footer-N-light.php');
get_footer();

