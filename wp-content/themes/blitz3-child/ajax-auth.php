<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 23.02.15
 * Time: 17:31
 */
?>
<!--окно авторизации-->
<form class="modal fade" id="auth" action="login" method="post">
    <div class="modal-dialog iwt1-auth-modal-dialog">
        <div class="iwt1-modal-header">
            <button type="button" class="close iwt1-close" data-dismiss="modal" aria-label="Close"></button>
            Авторизация
        </div>
        <p class="status"></p>
        <div class="iwt1-input-wrapper">
            <input id="username" name="username" type="text" class="iwt1-input" placeholder="Номер договора" />
        </div>
        <div class="iwt1-input-wrapper">
            <input id="password" type="password" name="password" class="iwt1-input" placeholder="Пароль" />
        </div>
        <input class="submit_button btn iwt1-orange-btn iwt1-login-btn" type="submit" value="Войти" name="submit">
        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
    </div>
</form>
<!--окно авторизации-->
