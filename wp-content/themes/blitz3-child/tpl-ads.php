<?php
/*
  Template name: iwt Объявления
 */
//require_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?>
<div class="row row-fluid">
    <div class="top-text ">
        <button class="iwt_blanki_page_price_and_terms"><a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>">ЦЕНЫ И СРОКИ</a></button>

        <h1 class="top-text__title"><?php the_title(); ?></h1> 
    </div>
</div>
<div class="wrap">

    <?php get_template_part('block', 'extratext'); ?>
    <?php query_posts('cat=31&showposts=999'); ?>
    <div class="row iwt1-ads-row">
        <?php
        $countposts = 0;
        while (have_posts()) {
            the_post();
            ?>
            <div class="col-xs-4 iwt1-ads-col">
                <a href="<?php echo post_permalink();?>" class="iwt1-ads-link"><?php the_title(); ?></a>
                <div class="iwt1-ads-date"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div>
                <div class="iwt1-ads-text"><?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/', '$1...', get_the_excerpt()); ?></div>
            </div>
            <?php
            $countposts++;
            if ($countposts == 3) {
                $countposts = 0;
                ?>
                <div class="clearfix"></div>
            <?php }
            ?>
        <?php } ?>
    </div></div>
</div>
<?php
foreach ($fattach as $fat) {
    if ($files) {
        require('block-blanki-ul.php');
    }
}
get_footer();
//require_once(TEMPLATEPATH . '/footer-N-light.php');
