<?php
/*
  Template name: iwt Загранпаспорт новый
 */
//require_once (TEMPLATEPATH . '/header-N.php');
//global $theme;
get_header();
?>
<div class = "row row-fluid">
    <div class = "top-text iwt_o_zpnew">
        <?php while (have_posts()) : the_post(); ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1>
            <?php get_template_part('block','extratext');?>
            <h3><?php echo $theme->field('Заголовок рыжего блока', array('notsingle' => true, 'variant' => 'textarea')); ?></h3>
            <div class="iwt_porofm">
                <img src="<?php echo $theme->get_thumb_src(get_the_ID(), 0, 300, true); ?>" alt=""/>
                <?php echo $theme->field('Текст рыжего блока', array('notsingle' => true, 'variant' => 'textarea')); ?>

            </div>
            <div class="iwt_o_zpnew_content"><?php the_content(); ?></div>
        <?php endwhile; ?>
    </div>
</div>
<?php
//require_once(TEMPLATEPATH . '/footer-N-light.php');
get_footer();
