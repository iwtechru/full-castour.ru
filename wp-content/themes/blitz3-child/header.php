<?php
global $theme;
//print_r($_SERVER['REQUEST_URI']);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php wp_title('|', true, 'right');
?></title><?php if (2 == 1) { ?>
            <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/152152.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/120120.png">
            <link rel="apple-touch-icon" sizes="48x48" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/4848.png">
            <link rel="apple-touch-icon" sizes="16x16" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/1616.png">
            <link rel="icon" type="image/png" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/1616.png" sizes="16x16">
            <link rel="icon" type="image/png" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/4848.png" sizes="48x48">
            <link rel="icon" type="image/png" href="<?php echo $theme->themedir; ?>/assets/css/img/icons/120120.png" sizes="120x120">
        <?php } ?>
        <link rel="Shortcut Icon" type="image/x-icon" href="<?php echo $theme->themedir; ?>/assets/img/favicon.ico" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/index.css" rel="stylesheet">-->
        <!--<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>-->
        <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
          <![endif]-->


        <!--[if gte IE 9]>
            <style type="text/css">
             *,*:hover, *:active, *:focus, *:visited{
                 filter: none;
              }
            </style>
        <![endif]-->
        <!-- wp_head() -->
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

        <?php
        if (stripos($_SERVER['REQUEST_URI'], '/lk') === 0 || stripos($_SERVER['REQUEST_URI'], '/dogovor') === 0) {
            ?>
            <div class="clearfix iwt1-lk-top">
                <div class="wrap">
                    <a href="<?php echo wp_logout_url(home_url()) ?>" class="pull-right iwt1-lk-link">Выйти из кабинета</a>
                </div>
            </div>
            <?php
        }
        ?>

        <div class="clearfix iwt1-top">
            <div class="wrap">
                <div class="clearfix row">
                    <a href="/" class="iwt1-logo pull-left"></a>
                    <span class="iwt1-span-italic  pull-left"><?php echo $theme->sitevar('Текст в шапке справа от логотипа', array('default' => 'Компания Кастур — надежная<br />помощь в сложном деле')); ?>
                    </span>
                    <div class="pull-right">
                        <button type = "button" class = "btn iwt1-orange-btn pull-right" data-toggle = "modal" data-target = "#callme">Перезвоните мне</button>
                        <?php
                        for ($i = 1; $i <= 2; $i++) {
                            $phone_ = $theme->sitevar('Телефон №' . $i . ' в шапке', array('default' => "+7 (999)999-99-99"));
                            $phone_content = explode(')', $phone_);
                            $phone_link = preg_replace("/[^0-9]/", "", $phone_);
                            ?>
                            <a href = "tel:+<?php echo $phone_link; ?>" class = "iwt1-top-phone pull-right">
                                <span class = "iwt1-phone-small pull-left"><?php echo $phone_content[0]; ?>) </span>
                                <span class = "iwt1-phone-big pull-left"><?php echo $phone_content[1]; ?></span>
                            </a>
                        <?php }
                        ?>                        
                    </div>

                </div>
            </div>
        </div>
        <!--черное меню-->
        <div class = "clearfix iwt1-dark-menu">
            <div class = "wrap">
                <div class = "row iwt1-menu-wrapper">
                    <?php $items = ($theme->menu_items('Верхнее меню (iwt)'));
                    ?>
                    <ul class = "iwt1-menu clearfix">
                        <?php
                        $children = array();
                        foreach ($items as $key => $item) {
                            if ($item->menu_item_parent == 0) {
                                ?>
                                <li class = "iwt1-parent<?php echo $item->ID; ?> <?php if (($item->classes[1])) { ?> current <?php } else { ?> current <?php } ?>iwt1-li" <?php if (is_array($item->menu_children)) {
                                    ?>data-target="iwt-menutarget<?php echo $item->ID; ?>"<?php } ?>>
                                    <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                                </li>
                                <?php
                            } else {
                                $children[$item->menu_item_parent][] = $key;
                            }
                        }
                        ?>
                    </ul>


                    <!--у меню data-target = "НАЗВАНИЕ РАЗДЕЛА"
                    а у блока iwt1-submenu добавить класс, который указан в дата атрибуте
                    элемента меню-->


                    <!--выпадающие менюшки-->
                    <?php
                    $limiter = 6;
                    foreach ($children as $k => $v) {
                        ?>
                        <div class = "iwt1-submenu iwt-menutarget<?php echo $k; ?> clearfix hidden">
                            <?php $count = 0; ?>
                            <?php
                            foreach ($v as $item_number) {
                                $count++;
                                ?>
                                <div data-source="<?php echo $k; ?>" class = "col-xs-4 iwt1-submenu-col">
                                    <a class="<?php
                                    foreach ($items[$item_number]->classes as $class) {
                                        echo $class . ' ';
                                    } if (!strlen($class) && $_SERVER["REQUEST_URI"] == $items[$item_number]->url) {
                                        ?> current <?php } ?>" href = "<?php echo $items[$item_number]->url; ?>"><?php echo $items[$item_number]->title; ?></a>
                                </div> 
                                <?php
                                if ($count == $limiter) {
                                    if ($limiter < count($v)) {
                                        ?>
                                        <a href="#" class="iwt1-submenu-icon"></a>
                                        <div class="clearfix iwt1-submenu-inner">
                                            <?php
                                        }
                                    }
                                }
                                if ($count > $limiter) {
                                    ?></div><?php } ?>
                        </div>    
                    <?php } ?>
                    <!--выпадающие менюшки-->
                </div>
            </div>
        </div>
        <!--черное меню-->
        <?php
        if (is_home() || is_front_page() || is_page('kontakty') || (get_page_template_slug() == 'tpl-main.php')) {
            
        } else {
            ?>
            <div class="wrap row-fluid">
                <div class="">
                    <div class="breadcrumbs">
                        <?php
                        if (function_exists('bcn_display')) {
#                            $bc = $theme->sitevar('Крошки ' . $_SERVER['REQUEST_URI']);
                            if (is_single) {
                                $bc = $theme->sitevar('Крошки ' . get_post_type() . '(вместо заголовка страницы используйте %%)');
                            } else {
                                $bc = $theme->sitevar('Крошки для страницы ' . (string) $_SERVER['REQUEST_URI']);
                            }
                            if (strlen($bc) > 0) {
                                echo str_replace('%%', get_the_title(), $bc);
                            } else {
                                bcn_display();
                            }
                        }
                        ?>
                    </div><br/>
                    <div class="clearfix"></div>
                </div></div>
            <?php
        } 
