<?php
/*
  Template name: iwt Главная страница
 */
get_header();
?>
<!--оранжевый блок с паспортом по центру-->
<?php
while (have_posts()) {
    the_post();
    ?>
    <div class="clearfix iwt1-orange-block">
        <div class="wrap">
            <h1 class="iwt1-title">
                Помощь в оформлении загранпаспорта
            </h1>
            <div class="clearfix">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="iwt1-list-wrapper">
                            <div class="iwt1-orange-title clearfix">
                                <span class="iwt1-orange-icon iwt1-orange-icon-1 pull-left"></span>
                                <span class="pull-left">О нас</span>
                            </div>                        
                            <ul class="iwt1-list">
                                <?php foreach ($theme->menu_items('О нас на главной') as $item) { ?>
                                    <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                                    <?php } ?>
                            </ul>          
                        </div>

                        <div class="iwt1-orange-title clearfix">
                            <span class="iwt1-orange-icon iwt1-orange-icon-2 pull-left"></span>
                            <span class="pull-left">Справочная</span>
                        </div>
                        <ul class="iwt1-list">
                            <?php foreach ($theme->menu_items('Справочная на главной') as $item) { ?>
                                <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                                <?php } ?>
                        </ul>   
                    </div>
                    <div class="col-xs-4 text-center">
                        <div class="iwt1-passport"></div>
                        <div class="iwt1-orange-title clearfix">
                            <span class="iwt1-orange-icon iwt1-orange-icon-3 pull-left"></span>
                            <?php if ( is_user_logged_in() ) {?>
                                <span class="pull-left"><a href="/lk">Проверить готовность</a></span>
                            <?php } else {?>
                                <span class="pull-left"><a href="#auth" data-toggle = "modal" data-target = "#auth">Проверить готовность</a></span>
                            <?php }?>
                        </div>
                        <div class="iwt1-clients-only">(Только для наших клиентов)</div>
                    </div>
                    <div class="col-xs-4">
                        <div class="iwt1-list-wrapper">
                            <div class="iwt1-orange-title clearfix">
                                <span class="iwt1-orange-icon iwt1-orange-icon-4 pull-left"></span>
                                <span class="pull-left">Услуги</span>
                            </div>
                            <ul class="iwt1-list">
                                <?php foreach ($theme->menu_items('Услуги на главной') as $item) { ?>
                                    <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                                    <?php } ?>
                            </ul>  
                        </div>
                        <div class="iwt1-orange-title clearfix">
                            <span class="iwt1-orange-icon iwt1-orange-icon-5 pull-left"></span>
                            <span class="pull-left">Полезное</span>
                        </div>
                        <ul class="iwt1-list">
                            <?php foreach ($theme->menu_items('Полезное') as $item) { ?>
                                <li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                                <?php } ?>
                        </ul>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--оранжевый блок с паспортом по центру-->

    <!--волнистая белая полоска-->
    <div class="clearfix white-wave"></div>
    <!--волнистая белая полоска-->

    <!--белый блок с текстом и оранжевыми кружочками-->
    <div class="wrap">
        <div class="row">
            <div class="clearfix iwt1-white-block">
                <?php
                $content = explode('<hr />', get_the_content());
                ?>
                <?php echo $content[0]; ?>
                <div class="clearfix iwt1-days-block">
                    <div class="iwt1-days-bold">
                        <?php echo $theme->field('Текст над тремя кружками', array('notsingle' => false)); ?>
                    </div>
                    <?php
                    for ($i = 1; $i <= 3; $i++) {
                        $days = explode(' ', $theme->field('Текст в кружке №' . $i, array('notsingle' => false)));
                        ?>
                        <div class="col-xs-4">
                            <div class="iwt1-day">
                                <div class="iwt1-day-number"><?php echo $days[0]; ?></div>
                                <div class="iwt1-day-text"><?php echo $days[1]; ?></div>
                            </div> 
                            <div class="iwt1-day-title"><?php echo preg_replace('/^([^\ ]*)\ /', '$1<br/>', $theme->field('Текст под кружком №' . $i, array('notsingle' => false))); ?></div>
                        </div>
                    <?php } ?>               
                </div>
                <?php echo $content[1]; ?>
            </div>
        </div>
    </div>
    <!--белый блок с текстом и оранжевыми кружочками-->


    <!--блок три характеристики-->
    <div class="clearfix iwt1-gray-block">
        <div class="wrap">
            <div class="clearfix iwt1-title-bold">                
                <?php
                echo $theme->field('Заголовок над характеристиками', array('notsingle' => false, 'default' => '<b>3 характеристики</b> <br />
                наших услуг'));
                ?>
            </div>
            <div class="row">
                <?php for ($i = 1; $i <= 3; $i++) { ?>
                    <div class="col-xs-4">                    
                        <div class="iwt1-three-icon iwt1-three-icon-<?php echo $i; ?>"></div>                        
                        <?php 
                        $field_label='Характеристика №' . $i . ' (используйте || для большого отступа)';
                        $args=array('notsingle' => false);
                        $field_value=$theme->field($field_label, $args);
                        $plus = explode('||', $field_value); ?>
                        <div class="iwt1-three-uppercase">
                            <?php echo $plus[0]; ?>
                        </div>
                        <div class="iwt1-col-text">
                            <?php echo $plus[1]; ?>
                        </div>
                    </div>
                <?php } ?>         
            </div>
        </div>
    </div>
    <!--блок три характеристики-->


    <!--блок 5 обещаний-->
    <div class="clearfix iwt1-five-block">
        <div class="wrap">
            <div class="clearfix iwt1-title-bold">
                <?php echo $theme->field('Заголовок над кругами с процентами', array('notsingle' => false)); ?>
            </div>
            <div class="row">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <div class="iwt1-col-five">
                        <div class="iwt1-five-icon iwt1-five-icon-<?php echo $i; ?>"></div>
                        <div class="iwt1-col-text">
                            <?php echo $theme->field('Текст под кругом №' . $i, array('notsingle' => false)) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--блок 5 обещаний-->

    <!--блок 7 преимуществ-->
    <div class="clearfix iwt1-seven-block">
        <div class="wrap">
            <div class="clearfix iwt1-title-bold">
                <?php echo $theme->field('Заголовок блока преимуществ', array('notsingle' => false)); ?>
            </div>
            <div class="clearfix">
                <div class="row">
                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                        <div class="col-xs-3">
                            <div class="iwt1-seven-icon iwt1-seven-icon-<?php echo $i; ?>"></div>
                            <div class="iwt1-col-text">
                                <?php echo $theme->field('Преимущество №' . $i, array('notsingle' => false)); ?>
                            </div>
                        </div>
                    <?php } ?>      
                </div>
                <div class="iwt1-small-row">
                    <?php for ($i = 5; $i <= 7; $i++) { ?>
                        <div class="col-xs-4">
                            <div class="iwt1-seven-icon iwt1-seven-icon-<?php echo $i; ?>"></div>
                            <div class="iwt1-col-text">
                                <?php echo $theme->field('Преимущество №' . $i, array('notsingle' => false)); ?>
                            </div>
                        </div>
                    <?php } ?>  
                </div>
            </div>
        </div>
    </div>
    <!--блок 7 преимуществ-->


    <!--блок с объявлениями-->
    <div class="clearfix iwt1-adv-wrapper">
        <div class="wrap">
            <div class="clearfix iwt1-title-bold">
                <?php $field=$theme->field('Текст над объявлениями',array('notsingle'=>true));
                if(strlen($field)>0){echo $field;}else{?><b>Берём на себя ваши заботы</b> <br />
                нашей компании<?php } ?>
            </div>
            <div class="iwt1-adv-text">
                Вы пребываете в зоне комфорта, пока мы сохраняем ваше время и нервную систему.
            </div>
            <div class="clearfix iwt1-title-bold">
                <b>Объявления</b>
            </div><div class="row iwt1-gray-row">
                <?php
                $news = get_posts(array('post_type' => 'post', 'posts_per_page' => 3));
                foreach ($news as $post) {
                    setup_postdata($post);
                    get_template_part('preview-home-news');
                }
                ?>
            </div>
            <button type="button" class="btn iwt-btn iwt1-gray-btn iwt-newtab"><span class="iwt-inherit-a" data-href="<?php echo get_category_link(31); ?>">Все объявления</span></button>
        </div>
    </div>
    <!--блок с объявлениями-->
<?php } ?>
<?php
get_footer();
