<?php
/*
  Template name: iwt Законодательство
 */
//include_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?>
<div class="row row-fluid iwt_zakonodatelstvo">
    <div class="top-text">
        <?php while (have_posts()) : the_post(); ?>

            <h1 class="top-text__title"><?php the_title(); ?></h1>
            <?php get_template_part('block','extratext');?>
            <div class="iwt_onaspishut">                
                <div class="iwt_onaspishet_v_border"></div>
                <div class="row row-fluid">
                    <?php
                    $blocks = 1;
                    $files = $theme->attachments('Документы');
                    foreach ($files as $file) {
                        ?>
                        <div class="col-xs-6 col-sm-6 iwt_onaspishet  iwt_onaspishet_<?php echo $file['id']; ?> iwt_onaspishet<?php
                        $blocks = $blocks * (-1);
                        $countblocks++;
                        echo str_replace('-', '_', $blocks);
                        ?> <?php if ($countblocks == count($files)) { ?> iwt_onaspishet_last iwt_onaspishet_last<?php echo $blocks; ?><?php } ?>">                       
                            <a href="<?php echo $theme->get_thumb_src($file['id']); ?>"><?php
                                echo $file['fields']['title'];
                                ?></a>
                        </div>
                        <?php if ($blocks == 1 && $countblocks < count($files)) { ?>
                        </div><div class="row row-fluid"><div class="iwt_onaspishet_h_border"></div><?php } ?>
                        <?php
                    }
                    if ($blocks == -1) {
                        ?><div class="iwt_clear"></div>
                        <div class="iwt_onaspishet iwt_onaspishet_last">&nbsp;</div><?php }
                    ?>
                    <div class="iwt_clear"></div>
                        </div></div>
                <?php //the_content();        ?>


            <?php endwhile; ?>
        </div>
    </div>


    <?php //include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php');  ?>
    <?php // include_once (TEMPLATEPATH . '/page-zagran-N-map.php');        ?>

    <?php //get_footer();  ?>
    <?php
//include_once (TEMPLATEPATH . '/footer-N-light.php');
    get_footer();
    