<?php
/*
  Template name: iwt Бланки, образцы и квитанциина загранпаспорт
 */
//require_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?><link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style-N-translite.css">
<div class="wrap">
    <div class="top-text iwt_blanki_page">
        <button class="iwt_blanki_page_price_and_terms"><a href="<?php echo $theme->sitevar('Ссылка блока цены и сроки'); ?>">ЦЕНЫ И СРОКИ</a></button>
        <?php while (have_posts()) : the_post(); ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1> 
            <?php get_template_part('block','extratext');?>
            <?php //the_content(); ?>
            <?php
            $fattach = array(
                'Загранпаспорт нового образца (биометрический) для взрослых:',
                'Загранпаспорт нового образца (биометрический) для детей:',
                'Загранпаспорт старого образца для взрослых:',
                'Загранпаспорт старого образца для детей:',
                    //    'Дополнительно'
            );
            foreach ($fattach as $fat) {
                $files = $theme->attachments($fat);
                if ($files) {
                    require('block-blanki-ul.php');
                    ?>

                    <?php
                }
            } $fattach = array('Дополнительно');
            $files=$theme->attachments($fattach[0]);
            ?>
        <?php endwhile; ?>
    </div>
</div>
<?php
foreach ($fattach as $fat) {
    if ($files) {
        require('block-blanki-ul.php');
    }
}
get_footer();
//require_once(TEMPLATEPATH . '/footer-N-light.php');
