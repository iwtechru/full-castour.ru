<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 22.02.15
 * Time: 16:07
 */

global $theme;

$email = types_render_field("dogovor_e_mail", array('output' => 'raw' ) );
$notification_shedule = wp_next_scheduled( 'iwto_passport_ready_event', $email );

if(isset($_POST['notify_email'])){
    if(isset($_POST['notify_me']) && isset($_POST['email_date'])){
        $result = wp_schedule_single_event( $_POST['email_date'], 'iwto_passport_ready_event', $email );
        if ($result===false){
            echo 'Не удалось добавить уведомление в расписание!';
        }
    } else {
        wp_unschedule_event( $notification_shedule, 'iwto_passport_ready_event', $email );
    }
    $notification_shedule = wp_next_scheduled( 'iwto_passport_ready_event', $email );
}
?>

<div class="iwt1-formtext-14 iwt1-margin-bottom-20">
Ваш номер договора: <b><?php echo types_render_field("dogovor_num", array( ) );?></b>
</div>
<form  action="" method="POST">
    <div class="iwt1-notification-form clearfix iwt1-margin-bottom-32">
        <div class="pull-left iwt1-checkbox-wrapper">
            <?php if($notification_shedule==false): ?>
                <input type="checkbox" id="iwt1-checkbox" name="notify_me" class="iwt1-checkbox" onclick="submit()">
            <?php else: ?>
                <input type="checkbox" id="iwt1-checkbox" name="notify_me" class="iwt1-checkbox" checked onclick="submit()">
            <?php endif ?>
            <label for="iwt1-checkbox" class="iwt1-label">Уведомить о готовности паспорта по почте</label>
        </div>
        <input type="text" name="notify_email" class="iwt1-input iwt1-lk-email pull-left" placeholder="e-mail" value="<?php echo types_render_field("dogovor_e_mail", array('output' => 'raw' ) );?>"/>
    </div>

<?php
$child_posts = types_child_posts('dogovor_passport');
$email_date = time() + 60*60*24*30;
foreach ($child_posts as $child_post) {
    $dogovor_state=get_dogovor_state($child_post->fields['dogovor_passport_date1'], $child_post->fields['dogovor_passport_date3']);
    if ($child_post->fields['dogovor_passport_freeze']==='1'){
        $dogovor_state=-1;
    }
    ?>
    <div class="iwt1-ready-row clearfix">
        <div class="iwt1-formtext-14 iwt1-margin-bottom-30">
            Клиент: <b><?php echo $child_post->fields['dogovor_passport_fio']?></b>
        </div>
        <?php if($dogovor_state>=0):?>
        <div class="clearfix">
            <?php if($dogovor_state<=0):?>
            <div class="pull-left iwt1-ready-orange">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text">Подача документов</div>
            <?php else: ?>
            <div class="pull-left iwt1-ready-green">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text">Документы поданы</div>
            <?php endif ?>
            </div>
            <div class="pull-left iwt1-ready-arrow"></div>
            <?php if($dogovor_state<2):?>
            <div class="pull-left iwt1-ready-gray">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">Проверка поданных документов</div>
            <?php elseif($dogovor_state==2): ?>
            <div class="pull-left iwt1-ready-orange">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">Проверка поданных документов</div>
            <?php else: ?>
            <div class="pull-left iwt1-ready-green">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">Проверки пройдены</div>
            <?php endif ?>
            </div>
            <div class="pull-left iwt1-ready-arrow"></div>
            <?php if($dogovor_state<4):?>
            <div class="pull-right iwt1-ready-gray">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">
                    Готовность паспорта
            <?php elseif($dogovor_state==4): ?>
            <div class="pull-right iwt1-ready-orange">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">
                    Готовность паспорта
            <?php else: ?>
            <div class="pull-right iwt1-ready-green">
                <div class="pull-left iwt1-ready-icon"></div>
                <div class="pull-left iwt1-ready-text iwt1-col-small">
                    Паспорт готов
            <?php endif ?>
                    <div class="pull-left iwt1-ready-date">
                        <?php
                        $ready_date = $child_post->fields['dogovor_passport_date3'];
                        echo date ('d.m.Y', $ready_date);
                        if($ready_date < $email_date) $email_date = $ready_date;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>
        <div class="iwt1-ready-text-wrapper">
            <div class="iwt1-ready-text-13">
                <?php if($dogovor_state<0):?>
                Чтобы узнать статус вашей заявки или дату готовности, просьба звонить по номеру
                <?php echo $theme->sitevar('Телефон №1 в шапке');?>
                <?php else: ?>
                    <?php if($dogovor_state==5): ?>
                        Ваш паспорт готов: <?php echo date ('d.m.Y', $child_post->fields['dogovor_passport_date3']);?><br>
                        Забор паспорта – в день готовности после 15:00, в последующие дни в любое время.<br>
                    <?php endif ?>
                <?php echo nl2br($child_post->fields['dogovor_passport_comment']);?>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php
}?>

   <input type="hidden" name="email_date" id="email_date" value="<?php echo $email_date;?>"/>
</form>


<?php get_template_part('lk','info');?>

