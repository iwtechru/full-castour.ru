#!/bin/sh
# Строка для cron
# for i in `find /var/www/html/ -name '.dbbackup.sh'`;do $i;done

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
CONFFILE="$SCRIPT_DIR/../../../wp-config.php";
SITEDIR="$(readlink -f $SCRIPT_DIR/../../../)";
SITENAME=`basename $SITEDIR`;
WPDBNAME=`cat $CONFFILE | grep DB_NAME | cut -d \' -f 4`
WPDBUSER=`cat $CONFFILE | grep DB_USER | cut -d \' -f 4`
WPDBPASS=`cat $CONFFILE | grep DB_PASSWORD | cut -d \' -f 4`
mysqldump -u $WPDBUSER -p$WPDBPASS $WPDBNAME > $SCRIPT_DIR/.sqldump.sql
cd $SITEDIR/../
tar czp --exclude='.git' -f /var/www/html/.backup/`date +%Y.%m.%d`_$SITENAME.tar.gz --strip-components=2 $SITENAME
cd /var/www/html/.backup