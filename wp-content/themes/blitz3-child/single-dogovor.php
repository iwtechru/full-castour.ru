<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 22.02.15
 * Time: 11:24
 */
?>
<?php
get_header();
?>

<?php
global $theme;
?>

    <div class="row row-fluid">
        <div class="top-text">
            <?php while (have_posts()) : the_post(); ?>
                <h2 class="top-text__title">Личный кабинет</h2>
                <?php get_template_part('block','extratext');?>
                <?php
                $dog_num = types_render_field("dogovor_num", array( ) );
                if ($current_user->allcaps['manage_options'] ||$current_user->user_login==$dog_num || $current_user->has_cap( 'editor' ) ){
                    get_template_part('dogovor','content');
                }
                else
                {
                    echo 'У Вас нет доступа к этой странице.';
                }
                ?>
            <?php endwhile; ?>
        </div>
    </div>

<?php
get_footer();
