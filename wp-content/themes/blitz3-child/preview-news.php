<?php global $theme; ?>
<div class="iwt_news_preview iwt_news_preview<?php echo $countblocks; ?>">
    <a href="<?php echo post_permalink(); ?>" class="iwt_news_preview_thumb" style="background-image:url(<?php echo $theme->get_thumb_src(get_the_ID(), array('width'=>204, 'height'=>220, 'crop'=>true)); ?>);"></a>
    <h3><a href="<?php echo post_permalink(); ?>"><?php the_title(); ?></a></h3>
    <div class="iwt_news_date"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div>
    <div class="iwt_news_excerpt"><?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/','$1...',get_the_excerpt()); ?></div>
</div>
<?php 
if ($countblocks >= 4) {
    $countblocks = 0;
    ?>
    <div class="iwt_clear"></div>
    <?php
}