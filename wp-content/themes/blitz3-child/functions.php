<?php
$url = explode('?', 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
$ID = url_to_postid($url[0]);
$pages = array('news', '/faq', '/article', get_site_url());
$files = scandir(get_stylesheet_directory());
$return = 1;
/*
  if (is_admin()) {
  $return = 1;
  }
  foreach ($files as $file) {
  if ($file == get_page_template_slug($ID)) {
  $return = 1;
  }
  }
  foreach ($pages as $page) {
  if (strpos($url[0], $page) > 0) {
  $return = 1;
  }
  } */
if ($return == 0) {

    # run_activate_plugin('/fancybox-for-wordpress/fancybox.php');    
    add_action('wp_enqueue_scripts', 'iwt_clear_style');
    return;
} else {
    
}
require('includes/functions_class.php');
$theme = new rht_theme('iwtech theme');
$theme->delete_option('required_plugins');
$plugins = array(
    'Attachments'
);
foreach ($plugins as $plugin) {
    $theme->update_option('required_plugins', $plugin, $plugin);
}
#$theme->disable_comments();
#$theme->update_option('menu_defaults', 'container',false);
##$theme->update_option('menu_defaults', 'container_class',false);
#$theme->update_option('menu_defaults', 'menu_class','nav nav-pills');
#$theme->delete_option('required_plugins');
if ($_GET['ttt'] == 12 && is_admin()) {
    $theme->delete_option('custom_fields');
}
deactivate_plugins('/fancybox-for-wordpress/fancybox.php', true);

function iwt_clear_styles() {
    $a = array('style1', 'style2');
    global $wp_styles;
    foreach ($a as $b) {
        wp_dequeue_style($b);
        wp_deregister_style($b);
    }
}

function my_script1() {
    $dqs = array('fancybox', 'wp-pagenavi');

    $skip[get_the_ID()] = 0;
    $skip = array(
        '1859' => 1,
        1857 => 1,
        1862 => 1,
        1694 => 1,
        1853 => 1
    );
    if ($skip[get_the_id()] == 0) {
        if (!is_admin()) {
            wp_deregister_script('jquery');
            foreach ($dqs as $s) {
                wp_deregister_style($s);
            }
            wp_deregister_script('fancybox');
            wp_dequeue_script('jquery');
            wp_register_script('jquery', '//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js');
            wp_enqueue_script('jquery');
            /*    wp_register_script('bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js');
              wp_enqueue_script('bootstrap-js');
              wp_register_style('bootstrap');
              wp_enqueue_style('bootsrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css');
             */  //wp_register_script('jquery', get_bloginfo('template_url') . '/js/jquery.min-1.7.2.js', false, '');
            //wp_enqueue_script('jquery');
            //wp_enqueue_script('prettyphoto', get_bloginfo('template_url').'/js/jquery.prettyPhoto.js', array('jquery'), '2.5.6');
            //wp_enqueue_script('cufon-yui', get_bloginfo('template_url').'/js/cufon-yui.js', array('jquery'), '1.0.9');
            //wp_enqueue_script('trebuchet', get_bloginfo('template_url').'/js/Trebuchet_MS_400-Trebuchet_MS_700-Trebuchet_MS_italic_400.font.js', array('jquery'));
            //wp_enqueue_script('cycle', get_bloginfo('template_url').'/js/jquery.cycle.all.min.js', array('jquery'));
            //wp_enqueue_script('dropdownmenu', get_bloginfo('template_url') . '/js/dropdown.js', array('jquery'));
        }
    } else {
        global $wp_styles;
        $a = array('style1', 'style2');
        foreach ($a as $b) {
            wp_dequeue_style($b);
            wp_deregister_style($b);
        }
        //print_r($wp_styles);
    }
}

#remove_action('wp_head', 'mfbfw_init');
add_action('wp_enqueue_scripts', 'my_script1');

function run_activate_plugin($plugin) {
    $current = get_option('active_plugins');
    $plugin = plugin_basename(trim($plugin));

    if (!in_array($plugin, $current)) {
        $current[] = $plugin;
        sort($current);
        do_action('activate_plugin', trim($plugin));
        update_option('active_plugins', $current);
        do_action('activate_' . trim($plugin));
        do_action('activated_plugin', trim($plugin));
    }

    return null;
}

function altair_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <div <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

        <div class="comment-wrap">
            <div class="avatar-wrapper">
                <div class="altair-comment-avatar">

                </div>                
            </div>
            <div class="autor-comment">
                <?php echo get_comment_author_link() ?>
            </div><!--
            <div class="date-comments">
            <?php //echo get_comment_date('j F Y'); ?>
            </div>-->
            <div class="comment-text-altair" style="">                
                <?php comment_text() ?>
            </div>
        </div>



        <?php
    }

    function true_russian_date_forms($the_date = '') {
        if (substr_count($the_date, '---') > 0) {
            return str_replace('---', '', $the_date);
        }
        $replacements = array(
            "Январь" => "января",
            "Февраль" => "февраля",
            "Март" => "марта",
            "Апрель" => "апреля",
            "Май" => "мая",
            "Июнь" => "июня",
            "Июль" => "июля",
            "Август" => "августа",
            "Сентябрь" => "сентября",
            "Октябрь" => "октября",
            "Ноябрь" => "ноября",
            "Декабрь" => "декабря"
        );
        return strtr($the_date, $replacements);
    }

    add_filter('the_time', 'true_russian_date_forms');
    add_filter('get_the_time', 'true_russian_date_forms');
    add_filter('the_date', 'true_russian_date_forms');
    add_filter('get_the_date', 'true_russian_date_forms');
    add_filter('the_modified_time', 'true_russian_date_forms');
    add_filter('get_the_modified_date', 'true_russian_date_forms');
    add_filter('get_post_time', 'true_russian_date_forms');
    add_filter('get_comment_date', 'true_russian_date_forms');

    function iwt_cartesian($input) {
        // filter out empty values
        $input = array_filter($input);

        $result = array(array());

        foreach ($input as $key => $values) {
            $append = array();

            foreach ($result as $product) {
                foreach ($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }

            $result = $append;
        }

        return $result;
    }

    if ($_GET['ttt'] == 13 && is_admin()) {
        $posts_ = get_posts(
                array(
                    'posts_per_page' => 9999,
                    'post_type' => array('news', 'article')
        ));
        foreach ($posts_ as $p) {
            $cont = $p->post_content;
            $cont1 = preg_replace('/\<a href\=\"[^<]*\<img[^>]*><\/a>/', '', $cont);
            $p->post_content = $cont1;
            if ($cont1 != $cont) {
                var_dump(wp_update_post(array(
                    'ID' => $p->ID,
                    'post_content' => $cont1
                )));
            }
        }
    }

    require('includes/functions2.php');
    add_filter('comment_post_redirect', 'redirect_after_comment');

    function redirect_after_comment($location) {
        $newurl = substr($location, 0, strpos($location, "#comment"));
        return $newurl . '?c=y';
    }

    $role_object = get_role('editor');
    $role_object->add_cap('edit_theme_options');

    function iwt_partition($list, $p) {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }
    