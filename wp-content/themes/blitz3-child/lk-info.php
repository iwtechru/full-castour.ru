<?php
/**
 * Created by PhpStorm.
 * User: Oleg0s
 * Date: 22.02.15
 * Time: 9:47
 */
?>

<div class="clearfix iwt1-margintop">
    <!--блок со списком документов-->
    <div class="col-xs-6 iwt1-docs-wrapper">
        <div class="iwt1-docs-block">
            <div class="iwt1-padding-block iwt1-docs-border">
                <div class="iwt1-docs-title">При себе иметь:</div>
                <div class="iwt1-docs-subtitle">Для взрослых</div>
                <ul class="iwt1-docs-list">
                    <li>
                        Паспорт гражданина РФ
                    </li>
                    <li>
                        Предыдущий загранпаспорт (если имеется и срок действия не истек)
                    </li>
                </ul>
            </div>

            <div class="iwt1-padding-block">
                <div class="iwt1-docs-subtitle">Для детей</div>
                <ul class="iwt1-docs-list">
                    <li>
                        Свидетельство о рождении
                    </li>
                    <li>
                        Паспорт гражданина РФ ребенка от 14-18 лет
                    </li>
                    <li>
                        Паспорт гражданина РФ одного из родителей
                    </li>
                    <li>
                        Предыдущий загранпаспорт ребенка (если имеется и срок действия не истек)
                    </li>
                </ul>
            </div>

        </div>
    </div>
    <!--блок со списком документов-->


    <!--блок с расписанием и адресом-->
    <div class="col-xs-6 iwt1-address-col">
        <div class="iwt1-docs-title">Адрес</div>
        <div class="iwt1-formtext-14 iwt1-margin-bottom-65">
            <?php echo '<ul><li>'.types_render_field( "dogovor_address", array('separator' => '</li><li>' )).'</li></ul>';?>
        </div>
        <div class="iwt1-docs-title">Режим работы</div>
        <div class="iwt1-time-block">
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Понедельник
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    9:00 - 18:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Вторник
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    10:00 - 20:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Среда
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    9:00 - 18:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Четверг
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    10:00 - 20:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Пятница
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    9:00 - 18:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Суббота
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    10:00 - 20:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
            <div class="iwt1-time-row clearfix">
                <div class="col-xs-4 iwt1-col-day">
                    Воскресенье
                </div>
                <div class="col-xs-4 iwt1-col-time">
                    9:00 - 18:00
                </div>
                <div class="col-xs-4 iwt1-col-orange">
                    с 13:00 до 13:45
                </div>
            </div>
        </div>
    </div>
    <!--блок с расписанием и адресом-->
</div>

