<?php
/*
  Template name: iwt Обычная страница
 */
get_header();
global $theme;
?>
<div class="wrap wrap-nopadding">
    <div class="top-text">
        <div class="row row-fluid"></div>
        <?php
        while (have_posts()) {
            the_post();
            ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1>
            <?php get_template_part('block','extratext');?>
            <?php the_content(); ?>
        <?php } ?>
    </div>
</div>
<?php
get_footer();
