<?php
get_header();
global $theme;
?>
<div class="row row-fluid">
    <div class="top-text">
        <h1 class="top-text__title"><?php
            $title = $theme->sitevar('404 - Заголовок');
            if (!strlen($title)) {
                ?>Страница не найдена<?php
            } else {
                echo $title;
            }
            ?></h1> 
    </div>
    <div class="row row-fluid row-content">
        <?php
        $text = $theme->sitevar('404 - Текст');
        if (!strlen($text)) {
            ?>
            Здесь мы собрали все-все-все вопросы, на которые нам приходилось отвечать. По всем нюансам оформления загранпаспорта. По подробностям получения паспорта гражданина Российской Федерации. По обращению за постоянной и временной регистрацией. По гражданству РСФСР и по виду на жительство. И по многим другим. При нажатии на крестик около заинтересовавшей вас рубрики развернется целая панорама вопросов и ответов. Посмотрите, может тут уже есть ответ и на ваш вопрос. А если нет? Пишите. Звоните. Ответим.
        <?php } ?>
    </div>
    <div class="row row-fluid">
        <!-- <a href="/" class=""><button class="button404">ГЛАВНАЯ СТРАНИЦА</button></a>-->
        <button type="button" class="btn iwt1-orange-btn btn404">ГЛАВНАЯ СТРАНИЦА</button>
    </div>
</div><?php
get_footer();
