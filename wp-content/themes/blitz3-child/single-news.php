<?php
//include_once (TEMPLATEPATH . '/header-N.php'); 
get_header();
?>

<?php global $theme; ?>

<div class="wrap iwt_single_news wrap-nopadding">
    <div class="top-text">
        <?php while (have_posts()) : the_post(); ?>
            <h1 class="top-text__title"><?php the_title(); ?></h1>
            <?php get_template_part('block', 'extratext'); ?>
            <div class=""><?php if (get_post_type() == 'news') { ?>
                    <div class="iwt_news_date"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div><?php } ?></div>
            <div class="row-content">
                <?php if (strlen($theme->get_thumb_src())) { ?>
                    <a href="<?php echo $theme->get_thumb_src(); ?>" class="fancybox">
                        <img src="<?php echo $theme->get_thumb_src(); ?>" alt="" class="iwt-featured col-xs-4"/>
                    </a>
                    <?php if (2 == 1) { ?>
                        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                    
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $theme->get_thumb_src(); ?>" id="imagepreview" class="img-responsive" >
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div>
    <?php if (get_post_type() == 'post') { ?>
        <div class="iwt_back2news iwt-btn"><a href="<?php echo get_category_link(31); ?>">ВЕРНУТЬСЯ К СПИСКУ</a></div>        
    <?php } else {
        ?>
        <div class="iwt_back2news iwt-btn"><a href="<?php echo get_post_type_archive_link(get_post_type()); ?>">ВЕРНУТЬСЯ К СПИСКУ <?php
                switch (get_post_type()) {
                    case 'article':
                        echo 'СТАТЕЙ';
                        break;
                    case 'news':
                        echo 'НОВОСТЕЙ';
                        break;
                }
                ?></a></div><?php } ?>
</div>


<?php // include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php');   ?>
<?php // include_once (TEMPLATEPATH . '/page-zagran-N-map.php');    ?>

<?php //get_footer();  ?>
<?php
//include_once (TEMPLATEPATH . '/footer-N-light.php');
get_footer();
