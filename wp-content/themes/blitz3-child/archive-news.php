<?php //get_header();                                    ?>
<?php
//include_once (TEMPLATEPATH . '/header-N.php');
get_header();
global $theme;
?>
<div class="wrap row-fluid">
    <div class="top-text">
        <h1 class="top-text__title"><?php echo $theme->sitevar('Заголовок страницы ' . get_post_type(), array('default' => 'Полезные новости')); ?></h1>
        <?php get_template_part('block', 'extratext'); ?>
    </div>  
</div>
<div class="row row-fluid">
    <?php
    $countblocks = 0;
    while (have_posts()) {

        the_post();
        ?>
        <div class="col-xs-4 col-sm-4 col-news col-news-<?php echo $countblocks; ?>">
            <a href="<?php echo post_permalink(); ?>" class="iwt_news_preview_thumb1" style="background-image:url(<?php echo $theme->get_thumb_src(get_the_ID(), array('width' => 204, 'height' => 220, 'crop' => true)); ?>);"></a>
            <h3><a href="<?php echo post_permalink(); ?>"><?php the_title(); ?></a></h3>                
            <?php if (get_post_type() == 'news') { ?><div class="iwt_news_date"><?php echo $theme->get_the_time('%d %FS %Y'); ?></div><?php } ?>
            <div class="iwt_news_excerpt1"><?php echo preg_replace('/^([^$]{300}[^\ ]*)\ [^$]*$/', '$1...', get_the_excerpt()); ?></div>
        </div>

        <?php
        $countblocks++;
        if ($countblocks == 3) {
            $countblocks = 0;
            ?>
        </div><div class="row row-fluid">
            <?php
        }
    }
    ?>        </div>
</div>


<?php // include_once (TEMPLATEPATH . '/page-zagran-N-tel-2.php');   ?>
<?php //include_once (TEMPLATEPATH . '/page-zagran-N-map.php');      ?>

<?php get_footer(); ?>
<?php
//include_once (TEMPLATEPATH . '/footer-N-light.php'); ?>