<?php
global $theme;
if (is_single() || is_page() ) {    
        
    $text = $theme->field('Текст под заголовком');
} else {    
    $text = $theme->sitevar('Текст под заголовком для страницы ' . str_replace('/', (' '), (string) $_SERVER['REQUEST_URI']));
}
if (strlen($text)) {
    ?>
    <div class="iwt-extratext"><?php echo $text; ?></div>
<?php }