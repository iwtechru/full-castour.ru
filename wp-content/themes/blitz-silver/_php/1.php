<b>Выбрано: Старого образца, ребенок до 14 лет, Москва</b><br><br>
<TABLE>
<TR>
<TD>Срок оформления, раб.дней </TH>
<TD >12-14</TD> <TD>9-10</TD> <TD>7-8</TD> <TD>5-6</TD><TD>3-4</TD> 
</TR>
<TR> 
<TD>Цена, руб</TH>    
<TD>7 000</TD> <TD>8 000</TD> <TD>9 000</TD> <TD>10 000 </TD> <TD>11 000</TD>   
</TR>
</TABLE>
<i>Госпошлина – 300 руб.</i> 
<br><br>

<h4>В cтоимость включено</h4>

<ul>
<li>Заполнение анкеты</li>
<li>Ксерокопия документов</li>
<li>Сдача документов без очереди</li>
<li>Получение документов без очереди</li>
<li>Отслеживание готовности</li>
<li>Выезд сотрудника (при срочном оформлении)</li>
</ul>

<h4>Что потребуется</h4>
<ul>
<li>Свидетельство о рождении с отметкой  о принадлежности к гражданству РФ</li>
<li>Гражданский паспорт одного из родителей</li>
<li>Ранее выданный заграничный паспорт (если имеется)
<li>3 фото размером 35x45 мм (матовые в овале) </li>
</ul>
<h4>Следует знать</h4>
<ul>
<li>На сегодняшний день ребенок до 14 лет может выезжать как по своему загранпаспорту, так и по загранпаспорту родителя, если он вписан в паспорт родителя (старого образца)</li>
<li>Если паспорт родителя  биометрический, то для выезда ребенка за границу оформление паспорта ребенку обязательно. </li>
<li>Загранпаспорт нового образца на ребенка оформляется с первых дней жизни и в загранпаспорт родителей не вписывается.</li>
<li>Заявление на выдачу загранпаспорта подается  от имени одного из  родителей либо законного представителя.</li>
</ul>